package com.example.hir_normal.notification;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.hir_normal.R;
import com.example.hir_normal.activity.SplashScreenActivity;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class FireBaseMesaaging extends FirebaseMessagingService {
    String title = "", body = "", page = "", page_id = "", image = "";
    String notification_id = "";
    String type = "";
    String order_id = "";
    public static String token = "";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        token = s;
        Function.setPrefData(Preferences.DEVICE_TOKEN, s, getApplicationContext());
        Log.d("state", s);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("nf", "temppp==" + remoteMessage.getData());
        Log.d("nf", "temppp==" + remoteMessage.getNotification());

        boolean isBackGround = isAppIsInBackground(FireBaseMesaaging.this);
        Log.d("aaa", "onMessageReceived: " + isBackGround);
        String msg = "";
        try {
            sendNotification(msg, remoteMessage, SplashScreenActivity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getApplicationContext().getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = null;
            if (componentInfo.getPackageName().equals(context.getApplicationContext().getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void sendNotification(String msg, RemoteMessage remoteMessage, Class<?> cls) throws UnsupportedEncodingException {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;
        NotificationManager notificationManager;

            String title;
            String message;
            String type;
            title = getString(R.string.app_name);

            message = "Default";
        Map<String, String> data = null;

        try {
            data = remoteMessage.getData();

            Log.d("notification", "sendNotification: " + data);

            notification_id = remoteMessage.getData().get("notification_id");
//            type = remoteMessage.getData().get("type");
//            order_id = remoteMessage.getData().get("order_id");

            body = remoteMessage.getNotification().getBody();//object.getString("body");
            title = remoteMessage.getNotification().getTitle(); //object.getString("title");
//            image = "http://fashionwholesalehub.com/resources/assets/images/notification_images/1570869909.Koala.jpg";
//            if (!remoteMessage.getData().get("image_url").equalsIgnoreCase("")) {
//                image = remoteMessage.getData().get("image_url");
//            }
//            page = object.getString("page");
//            page_id = object.getString("page_id");

           /* if (!image.equals("")) {
                try {
                    URL url = new URL(image);
                    image1 = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (IOException e) {
                    System.out.println(e);
                }
            }*/
        } catch (Exception e) {
            Log.d("e", "onMessageReceived: ");
        }

        Intent intent;
       /* PendingIntent pendingIntent = null;
        if (Function.getBooleanPrefData(Preferences.IS_LOGIN, FireBaseMesaaging.this)) {
            Log.d("notification", "sendNotification: type :" + type);
            Log.d("notification", "sendNotification: data :" + data);
            intent = new Intent(this, NotificationDetailActivity.class)
                    .putExtra("notification_id", notification_id)
                    .putExtra("message", remoteMessage.getNotification().getBody())
                    .putExtra("from", "notification");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);



            if (!notification_id.equalsIgnoreCase("")) {
                if (type != null && !type.equalsIgnoreCase("") && type.equalsIgnoreCase("order")) {
                    intent = new Intent(this, NotificationDetailActivity.class)
                            .putExtra("notification_id", notification_id)
                            .putExtra("from", "notification");
                } else if (type != null && !type.equalsIgnoreCase("") && type.equalsIgnoreCase(Constants.INBOX_SCREEN)) {
                    intent = new Intent(this, InboxActivity.class)
                            .putExtra(Constants.FROM, Constants.NOTIFICATION);
                }else {
                    intent = new Intent(this, InquiryDetailActivity.class)
                            .putExtra("id", order_id)
                            .putExtra("from", "notification");
                }

            }  else if (Functions.getPrefData(Preferences.USER_ROLE, this).equalsIgnoreCase(Constants.PROVIDER)) {
                intent = new Intent(this, ProviderHomeActivity.class);
            } else {
                intent = new Intent(this, CustomerHomeActivity.class);
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

        } else {
            intent = new Intent(this, LoginActivity.class)
                    .putExtra("notification_id", notification_id)
                    .putExtra("from", "notification");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }
*/

       /* if (page.equalsIgnoreCase("home")) {
            Functions.setBooleanPref(Constants.ISLOGIN, false, MyFirebaseMesssage.this);
            intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        } else if (page.equalsIgnoreCase("catalogtype")) {
            intent = new Intent(this, BannerActivity.class);
            Functions.setPrefData("catalog_type_id", page_id, MyFirebaseMesssage.this);
            Functions.setPrefData("from", page, MyFirebaseMesssage.this);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        } else if (page.equalsIgnoreCase("manufacturer")) {
            intent = new Intent(this, BannerActivity.class);
            Functions.setPrefData("manufacture_id", page_id, MyFirebaseMesssage.this);
            Functions.setPrefData("from", page, MyFirebaseMesssage.this);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        } else if (page.equalsIgnoreCase("category")) {
            intent = new Intent(this, BannerActivity.class);
            Functions.setPrefData("catagory_id", page_id, MyFirebaseMesssage.this);
            Functions.setPrefData("from", page, MyFirebaseMesssage.this);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        } else if (page.equalsIgnoreCase("productdetail")) {
            intent = new Intent(this, ProductDetailActivity.class);
            intent.putExtra("catalog_id", page_id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        } else if (page.equalsIgnoreCase("vieworder")) {
            if (Functions.getBooleanpref(Constants.ISLOGIN, MyFirebaseMesssage.this)) {
                intent = new Intent(this, OrderInformationActivity.class)
                        .putExtra("id", page_id);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                        PendingIntent.FLAG_ONE_SHOT);
            } else {
                intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                        PendingIntent.FLAG_ONE_SHOT);
            }
        } else {
            intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }*/

        notificationBuilder = new NotificationCompat.Builder(this, getResources().getString(R.string.app_name));
        long when = System.currentTimeMillis();
        int iconL = R.drawable.ic_youtube;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this, getResources().getString(R.string.app_name))
                    .setSmallIcon(iconL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setWhen(when)
                    .setSmallIcon(R.drawable.ic_youtube)
                    .setColor(getResources().getColor(R.color.colorAccent))
                    .setSound(defaultSoundUri);
//                    .setStyle(new NotificationCompat.BigPictureStyle()
//                            .bigPicture(image1).setSummaryText(body))
//                    .setColor(getResources()
//                            .getColor(R.color.colorPrimary))
//                    .setLargeIcon(image1);
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // notificationManager.notify(0, notificationBuilder.build());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                NotificationChannel mChannel = null;
                int importance = NotificationManager.IMPORTANCE_HIGH;
                // The id of the channel.
                String id = getResources().getString(R.string.app_name);
                // The user-visible name of the channel.
                CharSequence name = getString(R.string.app_name);
                mChannel = new NotificationChannel(id, name, importance);
                // Configure the notification channel.
                mChannel.enableLights(true);
                // Sets the notification light color for notifications posted to this
                // channel, if the device supports this feature.
                mChannel.setLightColor(getResources().getColor(R.color.colorAccent));
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                        .build();

                mChannel.setSound(defaultSoundUri, audioAttributes);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert notificationManager != null;
                notificationManager.createNotificationChannel(mChannel);
                notificationBuilder.setChannelId(getResources().getString(R.string.app_name));

            }

            notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());

        } else {
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(title).setContentText(body)
                    .setAutoCancel(true).setWhen(when)
                    .setSound(defaultSoundUri);
//                    .setStyle(new NotificationCompat.BigPictureStyle()
//                            .bigPicture(image1).setSummaryText(body))
//                    .setColor(getResources().getColor(R.color.colorPrimary))
//                    .setLargeIcon(image1)
//                    .setContentIntent(pendingIntent);

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());
        }

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_youtube : R.drawable.ic_youtube;
    }
}
