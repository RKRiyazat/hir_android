package com.example.hir_normal.interfaces;

import com.example.hir_normal.model.CartData;

public interface OnCartItemClick {

    void onCartItemClick(int pos, CartData productData);

}
