package com.example.hir_normal.interfaces;

import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.ProductData;

public interface OnGiftItemClick {

    void onGiftItemClick(int pos, GiftData giftData);

}
