package com.example.hir_normal.interfaces;

import com.example.hir_normal.apiCallBack.AvailableEarningCallBack;
import com.example.hir_normal.apiCallBack.CartListCallBack;
import com.example.hir_normal.apiCallBack.CatalogListCallBack;
import com.example.hir_normal.apiCallBack.CityListCallBack;
import com.example.hir_normal.apiCallBack.ContactListCallBack;
import com.example.hir_normal.apiCallBack.CountryListCallBack;
import com.example.hir_normal.apiCallBack.DesignListCallBack;
import com.example.hir_normal.apiCallBack.EventListCallBack;
import com.example.hir_normal.apiCallBack.GiftCartListCallBack;
import com.example.hir_normal.apiCallBack.GiftListCallBack;
import com.example.hir_normal.apiCallBack.GiftOrderHistoryCallBack;
import com.example.hir_normal.apiCallBack.HIRTalkCallBack;
import com.example.hir_normal.apiCallBack.NotificationCallBack;
import com.example.hir_normal.apiCallBack.OfferListCallBack;
import com.example.hir_normal.apiCallBack.OrderHistoryCallBack;
import com.example.hir_normal.apiCallBack.ProductListCallBack;
import com.example.hir_normal.apiCallBack.RedeemEarningCallBack;
import com.example.hir_normal.apiCallBack.RequirementsCallBack;
import com.example.hir_normal.apiCallBack.RequirementsDeleteCallBack;
import com.example.hir_normal.apiCallBack.StateListCallBack;
import com.example.hir_normal.apiCallBack.UserTotalPointCallBack;
import com.example.hir_normal.apiCallBack.VideoListCallBack;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface API {
    @Multipart
    @POST("register")
    Call<JsonObject> registerApplicator(@PartMap HashMap<String, RequestBody> data,
                                        @Part MultipartBody.Part gst_no,
                                        @Part MultipartBody.Part pan_card,
                                        @Part MultipartBody.Part image_1,
                                        @Part MultipartBody.Part image_2);

    @Multipart
    @POST("update-profile")
    Call<LoginResponseModel> updateProfile(@PartMap HashMap<String, RequestBody> data,
                                           @Part MultipartBody.Part profile_image,
                                           @Part MultipartBody.Part gst_no,
                                           @Part MultipartBody.Part pan_card,
                                           @Part MultipartBody.Part image_1,
                                           @Part MultipartBody.Part image_2);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponseModel> loginApi(@FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST("category-list")
    Call<ProductListCallBack> getCategory(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("get-cateloges")
    Call<CatalogListCallBack> getCatalog(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("get-HIR-Talks")
    Call<HIRTalkCallBack> getHirTalk(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("product-list")
    Call<ProductListCallBack> getProduct(@Field("category_id") String category_id,
                                         @Field("user_id") String user_id,
                                         @Field("class_id") String class_id);


    @FormUrlEncoded
    @POST("change-password")
    Call<JsonObject> changePassword(@Field("user_id") String user_id,
                                    @Field("current_password") String current_password,
                                    @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("get-gift-list")
    Call<GiftListCallBack> getGift(@Field("user_id") String user_id);

    @POST("country-list")
    Call<CountryListCallBack> getCountry();

    @FormUrlEncoded
    @POST("state-list")
    Call<StateListCallBack> getStates(@Field("country_id") String country_id);

    @FormUrlEncoded
    @POST("city-list")
    Call<CityListCallBack> getCities(@Field("state_id") String state_id);

    @FormUrlEncoded
    @POST("add-to-cart")
    Call<JsonObject> addToCart(@Field("user_id") String user_id,
                               @Field("product_id") String product_id,
                               @Field("price") String price,
                               @Field("quantity") String quantity,
                               @Field("is_remove") String is_remove);

    @FormUrlEncoded
    @POST("app-add-to-cart")
    Call<JsonObject> addGiftToCart(@Field("user_id") String user_id,
                                   @Field("gift_id") String gift_id,
                                   @Field("price") String price,
                                   @Field("quantity") String quantity,
                                   @Field("is_remove") String is_remove);

    @FormUrlEncoded
    @POST("list-cart")
    Call<CartListCallBack> getCartList(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("get_app_carts")
    Call<GiftCartListCallBack> getGiftCartList(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("get-earning-history")
    Call<AvailableEarningCallBack> getAvailableEarning(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get-redumption-history")
    Call<RedeemEarningCallBack> getRedeemEarning(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get-order-history")
    Call<OrderHistoryCallBack> getOrderHistory(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get-gift-order-history")
    Call<GiftOrderHistoryCallBack> getGiftOrderHistory(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("store-applicator-earning")
    Call<JsonObject> callEarningApi(@Field("user_id") String user_id,
                                    @Field("product_id") String product_id,
                                    @Field("city_id") String city_id,
                                    @Field("id") String qrcode_id,
                                    @Field("hirCode") String hirCode,
                                    @Field("points") String points);

    @FormUrlEncoded
    @POST("store-applicator-redumption")
    Call<JsonObject> submitPointData(@Field("mobile") String toString,
                                     @Field("gift_id") String gift_id,
                                     @Field("points") String points);

    @FormUrlEncoded
    @POST("get-video-list")
    Call<VideoListCallBack> getVideoList(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("add-order")
    Call<JsonObject> addOrder(@Field("user_id") String user_id,
                              @Field("type") String type,
                              @Field("total") String total,
                              @Field("cheque_no") String cheque_no);

    @FormUrlEncoded
    @POST("gift-add-to-cart")
    Call<JsonObject> addGiftCartOrder(@Field("user_id") String user_id,
                                      @Field("points") String points);

    @FormUrlEncoded
    @POST("cancel-order")
    Call<JsonObject> cancelOrder(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("change-order-status")
    Call<JsonObject> changeOrderStatus(@Field("order_id") String order_id,
                                       @Field("status") String status);

    @POST("get-contact-list")
    Call<ContactListCallBack> getContacts();

    @FormUrlEncoded
    @POST("get-offer-list")
    Call<OfferListCallBack> getOffer(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("design-list")
    Call<DesignListCallBack> getDesigns(@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("get-user-total-points")
    Call<UserTotalPointCallBack> getUserTotalPoints(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("store-applicator-redumption")
    Call<JsonObject> submitGiftPointData(@Field("mobile") String toString,
                                         @Field("gift_id") String gift_id,
                                         @Field("points") String points,
                                         @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("notification-list")
    Call<NotificationCallBack> getNotifications(@Field("class_id") String class_id);

    @Multipart
    @POST("add-requirement")
    Call<JsonObject> addRequirement(@Part("id") RequestBody id,
                                    @Part("distributor") RequestBody distributor,
                                    @Part("title") RequestBody title,
                                    @Part("description") RequestBody description,
                                    @Part("address") RequestBody address,
                                    @Part("qty") RequestBody qty,
                                    @Part("receive_date") RequestBody receive_date,
                                    @Part MultipartBody.Part image,
                                    @Part MultipartBody.Part video);


    @FormUrlEncoded
    @POST("list-requirement")
    Call<RequirementsCallBack> getRequirements(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("delete-requirement")
    Call<RequirementsDeleteCallBack> deleteRequirements(@Field("id") String id);

    @FormUrlEncoded
    @POST("event")
    Call<EventListCallBack> getEvent(@Field("id") String id);

    @FormUrlEncoded
    @POST("update-token")
    Call<LoginResponseModel> updateToken(@Field("user_id") String user_id,
                                         @Field("device_token") String device_token);
}