package com.example.hir_normal.interfaces;

import com.example.hir_normal.model.CartData;
import com.example.hir_normal.model.GiftCartData;

public interface OnGiftCartItemClick {

    void onGiftCartItemClick(int pos, GiftCartData giftCartData);

}
