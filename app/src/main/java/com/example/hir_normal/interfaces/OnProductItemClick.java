package com.example.hir_normal.interfaces;

import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.ProductData;

public interface OnProductItemClick {

    void onProductItemClick(int pos, ProductData productData);

}
