package com.example.hir_normal.function;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.hir_normal.MainActivity;
import com.example.hir_normal.R;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Function {
    public static void openDatePicker(Context mContext, TextView textView) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        DatePickerDialog picker = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "";
                        String dayOfMonthString = "";
                        if (dayOfMonth < 10) {
                            dayOfMonthString = ("0" + dayOfMonth);
                        }else{
                            dayOfMonthString = String.valueOf(dayOfMonth);
                        }
                        if ((monthOfYear + 1) < 10) {
                            date = dayOfMonthString + "/" + "0" + (monthOfYear + 1) + "/" + year;
                        } else {
                            date = dayOfMonthString + "/" + (monthOfYear + 1) + "/" + year;
                        }
                        textView.setText(date);

                    }
                }, year, month, day);
        picker.show();

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean checkBlankEditText(String value) {
        boolean result = false;
        if (value.equals("")) {
            result = true;
        }
        return result;
    }

    public static String getPrefData(String prefName, Context context) {
        SharedPreferences pref;
        String prefValue;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        prefValue = pref.getString(prefName, "");
        return prefValue;
    }

    public static void setPrefData(String prefName, String value, Context context) {
        SharedPreferences pref;
        SharedPreferences.Editor editor;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(prefName, value);
        editor.commit();
    }

    public static Integer getPrefIntData(String prefName, Context context) {
        SharedPreferences pref;
        Integer prefValue;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        prefValue = pref.getInt(prefName, 0);
        return prefValue;
    }

    public static void setPrefIntData(String prefName, Integer value, Context context) {
        SharedPreferences pref;
        SharedPreferences.Editor editor;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putInt(prefName, value);
        editor.commit();
    }

    public static void setBooleanPrefData(String prefName, boolean value, Context context) {
        SharedPreferences pref;
        SharedPreferences.Editor editor;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putBoolean(prefName, value);
        editor.commit();
    }

    public static boolean getBooleanPrefData(String prefName, Context context) {
        SharedPreferences pref;
        boolean prefValue;
        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        prefValue = pref.getBoolean(prefName, false);
        return prefValue;
    }

    public static boolean checkConnection(Context mContext) {
        boolean check = internetConnectionAvailable(1000);
        if (!check) {
            showDialogue(mContext);
            return false;
        } else {
            return true;
        }
    }

    public static void showDialogue(Context mContext) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet);
        Window window = dialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Button btnok = dialog.findViewById(R.id.btnOk);
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               /* Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setClassName("com.example.hir_normal", "com.android.phone.NetworkSetting");
                mContext.startActivity(intent);*/
                mContext.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
            }
        });
        dialog.show();
    }


    public static boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }


    public static String dateFormatChange(String inputDateStr) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        return outputDateStr;
    }

    public static String dateFormatWithoutChange(String inputDateStr) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = "";
        if (date != null) {
            outputDateStr = outputFormat.format(date);
        }
        return outputDateStr;
    }

    public static String dateFormatWithoutChangeShort(String inputDateStr) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = "";
        if (date != null) {
            outputDateStr = outputFormat.format(date);
        }
        return outputDateStr;
    }

    public static String updateDateFormatWithoutChange(String inputDateStr) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = "";
        if (date != null) {
            outputDateStr = outputFormat.format(date);
        }
        return outputDateStr;
    }

}
