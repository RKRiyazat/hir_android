package com.example.hir_normal.function;

public class Preferences {
    public static final String IS_LOGIN = "is_login";
    public static final String USER_ROLE = "user_role";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_IMAGE = "user_image";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_ADHAR = "user_adhar";
    public static final String USER_DOB = "user_dob";
    public static final String USER_ANNIVERSERY = "user_anniversey";
    public static final String  DISTRIBUTORE_NAME= "distributor_name";
    public static final String  STATE_ID= "state_id";
    public static final String  CITY_ID= "city_id";
    public static final String  USER_FIRM_TYPE= "user_firm_type";
    public static final String  USER_FIRM_NAME= "user_firm_name";
    public static final String  USER_PAN_CARD_IMAGE= "user_pan_card_image";
    public static final String  USER_PAN_CARD_NUMBER= "user_pan_card_number";
    public static final String  USER_GST_IMAGE= "user_gst_image";
    public static final String  USER_GST_NUMBER= "user_gst_number";
    public static final String  USER_CONCERN_PERSON= "user_concern_person";
    public static final String  USER_ORDER_PERSON= "user_order_person";
    public static final String  USER_BANK_NAME= "user_bank_name";
    public static final String  USER_BANK_ACCOUNT_NUMBER= "user_bank_account_number";
    public static final String  USER_BANK_IFSC_CODE= "user_bank_ifsc_code";
    public static final String  USER_BANK_BRANCH= "user_bank_branch";
    public static final String  USER_BANK_HOLDER_NAME= "user_bank_holder_name";
    public static final String  USER_BANK_PASS_BOOK_1= "user_bank_passbook_1";
    public static final String  USER_BANK_PASS_BOOK_2= "user_bank_passbook_2";
    public static final String  DISTRIBUTOR_CODE= "distributor_code";
    public static final String  CLASS_ID= "class_id";
    public static final String  DEVICE_TOKEN= "device_token";

}
