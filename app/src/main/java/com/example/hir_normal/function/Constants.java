package com.example.hir_normal.function;

public class Constants {
    final public static String CATEGORY_ID = "category_id";
    final public static String REQUIREMENT_ID = "requirement_id";
    final public static String REQUIREMENT_TITLE = "requirement_title";
    final public static String REQUIREMENT_DESCRIPTION = "requirement_description";
    final public static String REQUIREMENT_QTY = "requirement_qty";
    final public static String REQUIREMENT_ADDRESS = "requirement_address";
    final public static String REQUIREMENT_RECEIVE_DATE = "requirement_receive_date";
    final public static String REQUIREMENT_IMAGE = "requirement_image";
    final public static String REQUIREMENT_VIDEO = "requirement_video";
    final public static String PAYMENT_HISTORY_LIST = "payment_history_list";
    final public static String ORDER_GIFT_LIST = "order_gift_list";
    final public static String ORDER_PRODUCT_LIST = "order_product_list";

    final public static String IMAGE_BASE_URL = "http://www.hirgroup.in/admin/public/uploads/";
    public static final String VIDEO_LINK = "video_link";
    public static final String VIDEO_TITLE = "video_title";
    public static final String ADD = "add";
    public static final String UPDATE = "update";
    public static final String PDF_URL = "pdf_url";
    public static final String FROM = "from";
    public static final String DESIGN = "design";
    public static final String VIDEO = "video";
    public static final String CATALOG = "catalog";
    public static final String HIR_TALK = "hir_talk";


    public static final String MENU_PROFILE = "menu_profile";
    public static final String MENU_REWARD_HISTORY = "menu_reward_history";
    public static final String MENU_CATALOGUE = "menu_catalogue";
    public static final String MENU_TALK = "menu_talk";
    public static final String MENU_PASSWORD = "menu_password";
    public static final String MENU_LOGOUT = "menu_logout";
    public static final String MENU_ADD_REQUIREMENT = "menu_add_requirement";
    public static final String MENU_REQUIREMENTS = "menu_requirements";
    public static final String MENU_SCAN = "menu_scan";
    public static final String MENU_GIFT = "menu_gift";
    public static final String MENU_POINT_HISTORY = "menu_point_history";

    public static final String SUCCESS = "success";
    public static final String DESCRIPTION = "description";

    public static final int REQUIREMENT_UPDATE_CODE = 123;

    public static final String VIDEO_URL = "video_url";
    public static final String VIDEO_TYPE = "video_type";
}
