package com.example.hir_normal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("adhar_no")
    @Expose
    private Object adharNo;
    @SerializedName("pan_card")
    @Expose
    private Object panCard;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firm_name")
    @Expose
    private String firmName;
    @SerializedName("firm_type")
    @Expose
    private String firmType;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("concern_person")
    @Expose
    private Object concernPerson;
    @SerializedName("order_person")
    @Expose
    private Object orderPerson;
    @SerializedName("aniversary_date")
    @Expose
    private String aniversaryDate;
    @SerializedName("area")
    @Expose
    private Object area;
    @SerializedName("distributor_name")
    @Expose
    private String distributorName;
    @SerializedName("gst_no")
    @Expose
    private Object gstNo;
    @SerializedName("gst_no_text")
    @Expose
    private Object gstNoText;
    @SerializedName("pan_card_text")
    @Expose
    private Object panCardText;
    @SerializedName("device_token")
    @Expose
    private Object deviceToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(Object adharNo) {
        this.adharNo = adharNo;
    }

    public Object getPanCard() {
        return panCard;
    }

    public void setPanCard(Object panCard) {
        this.panCard = panCard;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getFirmType() {
        return firmType;
    }

    public void setFirmType(String firmType) {
        this.firmType = firmType;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public Object getConcernPerson() {
        return concernPerson;
    }

    public void setConcernPerson(Object concernPerson) {
        this.concernPerson = concernPerson;
    }

    public Object getOrderPerson() {
        return orderPerson;
    }

    public void setOrderPerson(Object orderPerson) {
        this.orderPerson = orderPerson;
    }

    public String getAniversaryDate() {
        return aniversaryDate;
    }

    public void setAniversaryDate(String aniversaryDate) {
        this.aniversaryDate = aniversaryDate;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public Object getGstNo() {
        return gstNo;
    }

    public void setGstNo(Object gstNo) {
        this.gstNo = gstNo;
    }

    public Object getGstNoText() {
        return gstNoText;
    }

    public void setGstNoText(Object gstNoText) {
        this.gstNoText = gstNoText;
    }

    public Object getPanCardText() {
        return panCardText;
    }

    public void setPanCardText(Object panCardText) {
        this.panCardText = panCardText;
    }

    public Object getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(Object deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
