package com.example.hir_normal.model.OrderHistory;

import com.example.hir_normal.model.OrderHistory.Order;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderData implements Serializable {
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;
    @SerializedName("total_purchase_amount")
    @Expose
    private Integer totalPurchaseAmount;
    @SerializedName("total_pending_amount")
    @Expose
    private Integer totalPendingAmount;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Integer getTotalPurchaseAmount() {
        return totalPurchaseAmount;
    }

    public void setTotalPurchaseAmount(Integer totalPurchaseAmount) {
        this.totalPurchaseAmount = totalPurchaseAmount;
    }

    public Integer getTotalPendingAmount() {
        return totalPendingAmount;
    }

    public void setTotalPendingAmount(Integer totalPendingAmount) {
        this.totalPendingAmount = totalPendingAmount;
    }
}
