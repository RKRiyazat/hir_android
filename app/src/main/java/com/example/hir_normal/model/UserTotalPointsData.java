package com.example.hir_normal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserTotalPointsData implements Serializable {
    @SerializedName("total_earning")
    @Expose
    private Integer totalEarning;
    @SerializedName("total_redeemed")
    @Expose
    private Integer totalRedeemed;
    @SerializedName("total_points")
    @Expose
    private String totalPoints;

    public Integer getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(Integer totalEarning) {
        this.totalEarning = totalEarning;
    }

    public Integer getTotalRedeemed() {
        return totalRedeemed;
    }

    public void setTotalRedeemed(Integer totalRedeemed) {
        this.totalRedeemed = totalRedeemed;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }
}
