package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.example.hir_normal.R;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;

import rx.functions.Functions;

public class SplashScreenActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mContext = SplashScreenActivity.this;
        init();
    }

    private void init() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Function.getBooleanPrefData(Preferences.IS_LOGIN, mContext)) {
                    if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
                        startActivity(new Intent(mContext, ApplicatorDashBoardActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {
                        startActivity(new Intent(mContext, DistributorDashBoardActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                } else {
                    startActivity(new Intent(mContext, LoginSelectionActivity.class));
                }
                finish();
            }
        }, 2000);
    }
}
