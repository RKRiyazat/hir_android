package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.OrderHistoryAdapter;
import com.example.hir_normal.apiCallBack.OrderHistoryCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.OrderHistory.Order;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_total_order_amount_value)
    TextView tv_total_order_amount_value;
    @BindView(R.id.tv_total_pending_amount_value)
    TextView tv_total_pending_amount_value;
    @BindView(R.id.rvOrderHistory)
    RecyclerView rvOrderHistory;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String userId = "";
    private OrderHistoryAdapter orderHistoryAdapter;
    private ArrayList<Order> orderArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        mContext = OrderHistoryActivity.this;
        ButterKnife.bind(this);
//        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tv_title.setText(R.string.account_ledger);
        userId = Function.getPrefData(Preferences.USER_ID, mContext);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        Log.d("userId", "init: " + userId);

        if (Function.isNetworkAvailable(mContext)) {
            getOrderHistoryData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    private void getOrderHistoryData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<OrderHistoryCallBack> orderHistoryCallBackCall = api.getOrderHistory(userId);
        orderHistoryCallBackCall.enqueue(new Callback<OrderHistoryCallBack>() {
            @Override
            public void onResponse(@NonNull Call<OrderHistoryCallBack> call, @NonNull Response<OrderHistoryCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equalsIgnoreCase("200")) {

                    tv_total_order_amount_value.setText(response.body().getData().getTotalPurchaseAmount().toString());
                    tv_total_pending_amount_value.setText(response.body().getData().getTotalPendingAmount().toString());

                    orderArrayList = new ArrayList<>();
                    orderArrayList = (ArrayList<Order>) response.body().getData().getOrders();
                    if (orderArrayList != null && orderArrayList.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvOrderHistory.setVisibility(View.VISIBLE);
                        orderHistoryAdapter = new OrderHistoryAdapter(mContext, orderArrayList);
                        rvOrderHistory.setLayoutManager(new LinearLayoutManager(mContext));
                        rvOrderHistory.setAdapter(orderHistoryAdapter);
                    } else {
                        rvOrderHistory.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderHistoryCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick({R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
