package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.CategoryAdapter;
import com.example.hir_normal.adapter.RequirementsAdapter;
import com.example.hir_normal.apiCallBack.ProductListCallBack;
import com.example.hir_normal.apiCallBack.RequirementsCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.ProductData;
import com.example.hir_normal.model.RequirementsData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequirementListActivity extends AppCompatActivity {

    @BindView(R.id.rv_requirement)
    RecyclerView rv_requirement;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;

    private Context mContext;
    private RequirementsAdapter requirementsAdapter;
    private ArrayList<RequirementsData> data;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirement_list);
        mContext = RequirementListActivity.this;
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        userId = Function.getPrefData(Preferences.USER_ID, mContext);
        tv_title.setText(getString(R.string.requirement_list));

        if (Function.isNetworkAvailable(mContext)) {
            getRequirementsData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getRequirementsData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<RequirementsCallBack> requirementsCallBackCall = api.getRequirements(userId);
        requirementsCallBackCall.enqueue(new Callback<RequirementsCallBack>() {
            @Override
            public void onResponse(@NonNull Call<RequirementsCallBack> call, @NonNull Response<RequirementsCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<RequirementsData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_requirement.setVisibility(View.VISIBLE);

                        requirementsAdapter = new RequirementsAdapter(mContext, data);
                        rv_requirement.setLayoutManager(new LinearLayoutManager(mContext));
                        rv_requirement.setAdapter(requirementsAdapter);
                    } else {
                        rv_requirement.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RequirementsCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Constants.REQUIREMENT_UPDATE_CODE) {
            if (Function.isNetworkAvailable(mContext)) {
                getRequirementsData();
            } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                Function.showDialogue(mContext);
            }
        }
    }
}
