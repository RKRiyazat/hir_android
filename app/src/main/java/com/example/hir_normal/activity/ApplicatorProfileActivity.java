package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.CityListCallBack;

import com.example.hir_normal.apiCallBack.StateListCallBack;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.imagecrop.BitmapUtil;
import com.example.hir_normal.imagecrop.CropHandler;
import com.example.hir_normal.imagecrop.CropHelper;
import com.example.hir_normal.imagecrop.CropParams;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.City;
import com.example.hir_normal.model.States;
import com.example.hir_normal.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicatorProfileActivity extends AppCompatActivity implements CropHandler {


    private String imagePath = "";

    @BindView(R.id.tv_birth_date)
    TextView tv_birth_date;
    @BindView(R.id.tv_marriage_anniversary_date)
    TextView tv_marriage_anniversary_date;
/*    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;*/
    @BindView(R.id.et_name_as_per_aadhar)
    EditText et_name_as_per_aadhar;
    @BindView(R.id.et_aadhar_number)
    EditText et_aadhar_number;
    @BindView(R.id.et_name_and_code_of_distributor)
    EditText et_name_and_code_of_distributor;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.sp_state)
    Spinner sp_state;
    @BindView(R.id.sp_city)
    Spinner sp_city;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_user_profile)
    ImageView iv_user_profile;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private String selectedCityId = "";
    private String selectedStateId = "";
    private String selectedCountryId = "101";
    ArrayList<City> cityArrayList;
    ArrayList<States> stateArrayList;

    private Context mContext;

    private Dialog dialogProfilePicture;
    File profileImageFile = null;
    private CropParams mCropParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicator_profile);
        mContext = ApplicatorProfileActivity.this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.img_bg));
        tv_title.setText(R.string.profile);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        cityArrayList = new ArrayList<>();
        City city = new City();
        city.setName("Select City");
        cityArrayList.add(city);

        stateArrayList = new ArrayList<>();
        States states = new States();
        states.setName("Select State");
        stateArrayList.add(states);

        ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_state.setAdapter(dataAdapter);

        ArrayAdapter<City> dataAdapterCity = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
        dataAdapterCity.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_city.setAdapter(dataAdapterCity);

        if (Function.isNetworkAvailable(mContext)) {
            getStateApiCall();
        } else {
            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
        }

        setData();
    }

    private void setData() {

        et_email.setText(Function.getPrefData(Preferences.USER_EMAIL, mContext));
        et_name_as_per_aadhar.setText(Function.getPrefData(Preferences.USER_NAME, mContext));
        et_mobile_number.setText(Function.getPrefData(Preferences.USER_MOBILE, mContext));
        et_aadhar_number.setText(Function.getPrefData(Preferences.USER_ADHAR, mContext));
//        tv_birth_date.setText(Function.getPrefData(Preferences.USER_DOB, mContext));
//        tv_marriage_anniversary_date.setText(Function.getPrefData(Preferences.USER_ANNIVERSERY, mContext));
        et_name_and_code_of_distributor.setText(Function.getPrefData(Preferences.DISTRIBUTOR_CODE, mContext));

        String birthDate = Function.dateFormatWithoutChange(Function.getPrefData(Preferences.USER_DOB, mContext));
        if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
            tv_birth_date.setText(birthDate);
        }

        String mADate = Function.dateFormatWithoutChange(Function.getPrefData(Preferences.USER_ANNIVERSERY, mContext));
        if (mADate != null && !mADate.equalsIgnoreCase("")) {
            tv_marriage_anniversary_date.setText(mADate);
        }

        selectedStateId = Function.getPrefData(Preferences.STATE_ID, mContext);
        selectedCityId = Function.getPrefData(Preferences.CITY_ID, mContext);


        String userImage = "";

        userImage = Function.getPrefData(Preferences.USER_IMAGE, mContext);

        if (userImage != null &&
                !userImage.equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + userImage)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(iv_user_profile);
        } else {
            iv_user_profile.setImageResource(R.drawable.ic_image_placeholder);
        }
    }


    private void getStateApiCall() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<StateListCallBack> stateListCallBackCall = api.getStates(selectedCountryId);
        stateListCallBackCall.enqueue(new Callback<StateListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<StateListCallBack> call, @NonNull Response<StateListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        stateArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_state.setAdapter(dataAdapter);
                    sp_state.setSelection(0);

                    if (selectedStateId != null && !selectedStateId.equalsIgnoreCase("")) {
                        for (int j = 0; j < stateArrayList.size(); j++) {
                            if (stateArrayList.get(j).getId() != null && stateArrayList.get(j).getId() == Integer.parseInt(selectedStateId)) {
                                sp_state.setSelection(j);
                            }
                        }
                    } else {
                        sp_state.setSelection(0);
                    }

                    sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedStateId = "";
                                cityArrayList = new ArrayList<>();
                                City city = new City();
                                city.setName("Select City");

                                cityArrayList.add(city);
                                ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                                sp_city.setAdapter(dataAdapter);
                                sp_city.setSelection(0);

                            } else {
                                selectedStateId = stateArrayList.get(i).getId().toString();
                                getCityApiCall(selectedStateId);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<StateListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCityApiCall(String stateId) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CityListCallBack> cityListCallBackCall = api.getCities(stateId);
        cityListCallBackCall.enqueue(new Callback<CityListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CityListCallBack> call, @NonNull Response<CityListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        cityArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_city.setAdapter(dataAdapter);
                    sp_city.setSelection(0);

                    if (selectedCityId != null && !selectedCityId.equalsIgnoreCase("")) {
                        for (int j = 0; j < cityArrayList.size(); j++) {
                            if (cityArrayList.get(j).getId() != null && cityArrayList.get(j).getId() == Integer.parseInt(selectedCityId)) {
                                sp_city.setSelection(j);
                            }
                        }
                    } else {
                        sp_city.setSelection(0);
                    }

                    sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedCityId = "";
                            } else {
                                selectedCityId = cityArrayList.get(i).getId().toString();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CityListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @OnClick({R.id.tv_birth_date, R.id.tv_marriage_anniversary_date, R.id.tv_sign_up, R.id.iv_user_profile, R.id.iv_back, R.id.tvViewCart})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_user_profile:
                chekPermition();
                break;
            case R.id.tvViewCart:
                startActivity(new Intent(mContext, PointHistoryActivity.class));
                break;

            case R.id.tv_birth_date:
                Function.openDatePicker(ApplicatorProfileActivity.this, tv_birth_date);
                break;
            case R.id.tv_marriage_anniversary_date:
                Function.openDatePicker(ApplicatorProfileActivity.this, tv_marriage_anniversary_date);
                break;
            case R.id.tv_sign_up:

                String email = et_email.getText().toString();
//                String password = et_password.getText().toString();
//                String cPassword = et_confirm_password.getText().toString();
                String name = et_name_as_per_aadhar.getText().toString();
                String deviceToken = "adafdsfasdfasdfsadfsadf";

                String birthDate = Function.updateDateFormatWithoutChange(tv_birth_date.getText().toString());
                String marriageDate = Function.updateDateFormatWithoutChange(tv_marriage_anniversary_date.getText().toString());
                String anniversaryDate = "";
                String dateOfBirth = "";

                if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
                    dateOfBirth = birthDate;
                }

                if (marriageDate != null && !marriageDate.equalsIgnoreCase("")) {
                    anniversaryDate = marriageDate;
                }

                String stateId = selectedStateId;
                String cityId = selectedCityId;
                String distributorName = et_name_and_code_of_distributor.getText().toString();
                String mobileNo = et_mobile_number.getText().toString();
                String aadharNumber = et_aadhar_number.getText().toString();

                String checkValidation = checkValidation(email, name, deviceToken, anniversaryDate, dateOfBirth,
                        stateId, cityId, distributorName, mobileNo, aadharNumber);
                if (checkValidation.equals("")) {
                    if (Function.isNetworkAvailable(mContext)) {
                        callApi(email, name, deviceToken, anniversaryDate, dateOfBirth,
                                stateId, cityId, distributorName, mobileNo, aadharNumber);
                    } else {
                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    private void callApi(String email, String name, String deviceToken, String anniversary, String dateOfBirth,
                         String stateId, String cityId, String distributorName, String mobileNo, String aadharNumber) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(ApplicatorProfileActivity.this, "", "please wait...");
        dialog.show();
        HashMap<String, RequestBody> data = new HashMap<>();

        RequestBody req_user_id = RequestBody.create(MediaType.parse("text/plain"), Function.getPrefData(Preferences.USER_ID, mContext));
        RequestBody req_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody req_name = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody req_device_token = RequestBody.create(MediaType.parse("text/plain"), deviceToken);
        RequestBody req_aniversary_date = RequestBody.create(MediaType.parse("text/plain"), anniversary);
        RequestBody req_distributor_name = RequestBody.create(MediaType.parse("text/plain"), distributorName);
        RequestBody req_state_id = RequestBody.create(MediaType.parse("text/plain"), stateId);
        RequestBody req_city_id = RequestBody.create(MediaType.parse("text/plain"), cityId);
        RequestBody req_role = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody req_dob = RequestBody.create(MediaType.parse("text/plain"), dateOfBirth);
        RequestBody req_mobile = RequestBody.create(MediaType.parse("text/plain"), mobileNo);
        RequestBody req_adhar_no = RequestBody.create(MediaType.parse("text/plain"), aadharNumber);
        RequestBody req_gst_no_text = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_pan_card_text = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_firm_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_concern_person = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_is_bank_details = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody req_bank_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_account_no = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_ifsc_code = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_branch = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_holder_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_gst_no = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyGst = MultipartBody.Part.createFormData("gst_no", "", req_gst_no);
        RequestBody req_pan_card = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyPanCard = MultipartBody.Part.createFormData("pan_card", "", req_pan_card);
        RequestBody req_image_1 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage1 = MultipartBody.Part.createFormData("image_1", "", req_image_1);
        RequestBody req_image_2 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage2 = MultipartBody.Part.createFormData("image_2", "", req_image_2);


        MultipartBody.Part profileImage = null;

        if (profileImageFile != null) {
            RequestBody reqImgFileProfile = RequestBody.create(MediaType.parse("multipart/form-data"), profileImageFile);
            profileImage = MultipartBody.Part.createFormData("avatar", profileImageFile.getName(), reqImgFileProfile);
        } else {
            RequestBody reqImgFileProfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            profileImage = MultipartBody.Part.createFormData("avatar", "", reqImgFileProfile);
        }

        data.put("email", req_email);
        data.put("user_id", req_user_id);
        data.put("name", req_name);
        data.put("aniversary_date", req_aniversary_date);
        data.put("state_id", req_state_id);
        data.put("city_id", req_city_id);
        data.put("role", req_role);
        data.put("dob", req_dob);
        data.put("mobile", req_mobile);
        data.put("adhar_no", req_adhar_no);

        Call<LoginResponseModel> jsonObjectCall = api.updateProfile(data, profileImage, multipartBodyGst, multipartBodyPanCard, multipartBodyImage1, multipartBodyImage2);
        jsonObjectCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Function.setPrefData(Preferences.USER_IMAGE, response.body().getLoginData().getAvatar(), mContext);
                        Function.setPrefData(Preferences.USER_EMAIL, response.body().getLoginData().getEmail(), mContext);
                        Function.setPrefData(Preferences.USER_ID, response.body().getLoginData().getId().toString(), mContext);
                        Function.setPrefData(Preferences.USER_NAME, response.body().getLoginData().getName(), mContext);
                        Function.setPrefData(Preferences.USER_MOBILE, response.body().getLoginData().getMobile(), mContext);
                        Function.setPrefData(Preferences.USER_ADHAR, response.body().getLoginData().getAdharNo(), mContext);
                        Function.setPrefData(Preferences.USER_DOB, response.body().getLoginData().getDob(), mContext);
                        Function.setPrefData(Preferences.USER_ANNIVERSERY, response.body().getLoginData().getAniversaryDate(), mContext);
                        Function.setPrefData(Preferences.STATE_ID, response.body().getLoginData().getStateId(), mContext);
                        Function.setPrefData(Preferences.CITY_ID, response.body().getLoginData().getCityId(), mContext);

                        Toast.makeText(ApplicatorProfileActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ApplicatorProfileActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ApplicatorProfileActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String checkValidation(String email, String name, String deviceToken, String anniversary, String dateOfBirth,
                                  String stateId, String cityId, String distributorName, String mobileNo, String aadharNumber) {
        if (TextUtils.isEmpty(name)) {
            return getString(R.string.enter_name);
        } else if (TextUtils.isEmpty(email)) {
            return getString(R.string.enter_email);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return getString(R.string.valid_email);
        } else if (TextUtils.isEmpty(mobileNo)) {
            return getString(R.string.enter_mobile);
        } else if (!Patterns.PHONE.matcher(mobileNo).matches()) {
            return getString(R.string.valid_mobile);
        } else if (TextUtils.isEmpty(anniversary)) {
            return getString(R.string.please_select_anniversary_date);
        } else if (TextUtils.isEmpty(dateOfBirth)) {
            return getString(R.string.please_select_date_of_birth);
        } else if (TextUtils.isEmpty(stateId)) {
            return getString(R.string.please_select_state);
        } else if (TextUtils.isEmpty(cityId)) {
            return getString(R.string.please_select_city);
        } else if (TextUtils.isEmpty(aadharNumber)) {
            return getString(R.string.enter_aadhar_number);
        } else {
            return "";
        }
    }


    private void chekPermition() {
        if (Build.VERSION.SDK_INT < 23) {
            showMenuDialogToSetProfilePic();
        } else {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showMenuDialogToSetProfilePic();
            } else {
                Utils.checkPermitionCameraGaller(ApplicatorProfileActivity.this);
            }
        }
    }

    private void showMenuDialogToSetProfilePic() {
        mCropParams = new CropParams(mContext);
        final TextView tvClickByCamera;
        final TextView tvPickFromGallery;
        final TextView tvCancel;

        dialogProfilePicture = new Dialog(mContext, R.style.picture_dialog_style);
        dialogProfilePicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProfilePicture.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogProfilePicture.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogProfilePicture.getWindow().setAttributes(wlmp);
        dialogProfilePicture.show();

        tvClickByCamera = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickFromGallery = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCancel);

        tvPickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
                openGallery();
            }
        });
        tvClickByCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
                openCamera();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
            }
        });
    }

    public void openGallery() {
        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;
        Intent intent = CropHelper.buildGalleryIntent(mCropParams, ApplicatorProfileActivity.this);
        startActivityForResult(intent, CropHelper.REQUEST_CROP);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropHelper.REQUEST_CROP) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == CropHelper.REQUEST_CAMERA) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        }

    }

    private void openCamera() {

        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;

        Intent intent = CropHelper.buildCameraIntent(mCropParams, ApplicatorProfileActivity.this);
        startActivityForResult(intent, CropHelper.REQUEST_CAMERA);

    }


    @Override
    public void onPhotoCropped(Uri uri) {

        if (!mCropParams.compress) {
            imagePath = uri.getPath();
            Log.d("onPhotoCropped", "imagePath==" + imagePath);
            iv_user_profile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(ApplicatorProfileActivity.this, uri));
            //ivbgImage.setImageBitmap(BitmapUtil.decodeUriAsBitmap(homeActivity, uri));

            //updateProfilePic();
            profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
        }

    }

    @Override
    public void onCompressed(Uri uri) {
        imagePath = uri.getPath();
        Log.d("--- image path", "--" + imagePath);

        Picasso.get().load(uri).into(iv_user_profile);
        profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
        //fragmentProfileBinding.ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(homeActivity, uri));
        //ivbgImage.setImageBitmap(BitmapUtil.decodeUriAsBitmap(activity, uri));
//        updateProfilePic();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }
}
