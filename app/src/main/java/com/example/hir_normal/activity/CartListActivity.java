package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.CartListAdapter;
import com.example.hir_normal.apiCallBack.CartListCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnCartItemClick;
import com.example.hir_normal.model.CartData;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartListActivity extends AppCompatActivity  implements OnCartItemClick {

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rbCash)
    RadioButton rbCash;
    @BindView(R.id.rbCheque)
    RadioButton rbCheque;
    @BindView(R.id.tvTotalValue)
    TextView tvTotal;
    @BindView(R.id.rgPayment)
    RadioGroup rgPayment;
    @BindView(R.id.etCheque)
    EditText etCheque;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.etChequeIssueDate)
    TextView etChequeIssuedate;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;
    @BindView(R.id.btnPay)
    Button btnPay;

    String userID = "";

    private Context mContext;
    private CartListAdapter cartListAdapter;
    private ArrayList<CartData> data;

    private Dialog qtydialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        mContext = CartListActivity.this;
        ButterKnife.bind(this);
//        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tv_title.setText(R.string.cart_list);
        userID = Function.getPrefData(Preferences.USER_ID, mContext);

        iv_back.setOnClickListener(view -> finish());
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        etChequeIssuedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Function.openDatePicker(CartListActivity.this, etChequeIssuedate);
            }
        });

        if (Function.isNetworkAvailable(mContext)) {
            getCartData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

        rgPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbCheque) {
                    etCheque.setVisibility(View.VISIBLE);
                    etChequeIssuedate.setVisibility(View.VISIBLE);
                } else {
                    etCheque.setVisibility(View.GONE);
                    etChequeIssuedate.setVisibility(View.GONE);
                }

            }
        });
    }

    private void getCartData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CartListCallBack> productListCallBackCall = api.getCartList(userID);
        productListCallBackCall.enqueue(new Callback<CartListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CartListCallBack> call, @NonNull Response<CartListCallBack> response) {
                dialog.dismiss();

                try {

                    if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                        data = new ArrayList<>();
                        data = (ArrayList<CartData>) response.body().getData();
                        if (data != null && response.body().getData().size() > 0) {
                            tv_not_available.setVisibility(View.GONE);
                            rvProduct.setVisibility(View.VISIBLE);
                            btnPay.setEnabled(true);
                            Double price = 0.0;
                            if (data != null && data.size() > 0) {
                                for (int i = 0; i < data.size(); i++) {
                                    price = price + (Double.parseDouble(data.get(i).getPrice()) * Double.parseDouble(data.get(i).getQuantity()));
                                }
                            }

                            tvTotal.setText(String.valueOf(price));
                            cartListAdapter = new CartListAdapter(CartListActivity.this, data, tvTotal);
                            rvProduct.setLayoutManager(new LinearLayoutManager(CartListActivity.this));
                            rvProduct.setAdapter(cartListAdapter);
                        } else {
//                            Toast.makeText(mContext, R.string.no_cart_item_available, Toast.LENGTH_SHORT).show();
                            tv_not_available.setVisibility(View.VISIBLE);
                            rvProduct.setVisibility(View.GONE);
                            btnPay.setEnabled(false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CartListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick(R.id.tvViewCart)
    public void onClick() {
        startActivity(new Intent(mContext, PointHistoryActivity.class));
    }

    @OnClick(R.id.btnPay)
    public void click() {
        if (rbCheque.isChecked()) {
            String cheque_no = etCheque.getText().toString();
            if (cheque_no.equals("")) {
                Toast.makeText(mContext, "please enter cheque no", Toast.LENGTH_SHORT).show();
            } else {
                if (Function.isNetworkAvailable(mContext)) {
                    callAddOrder();
                } else {
//                    Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    Function.showDialogue(mContext);
                }
            }
        } else {
            if (Function.isNetworkAvailable(mContext)) {
                callAddOrder();
            } else {
//                Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                Function.showDialogue(mContext);
            }
        }
    }

    private void callAddOrder() {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();

        String type = "";
        String chequeNo = "";
        String total = "";
        String userId = "";

        userId = Function.getPrefData(Preferences.USER_ID, mContext);

        if (rbCash.isChecked()) {
            type = "1";
            chequeNo = "";
        } else {
            type = "2";
            chequeNo = etCheque.getText().toString();
        }
        total = tvTotal.getText().toString();


        Log.d("addOrder", "callAddOrder: " + userId);
        Log.d("addOrder", "callAddOrder: " + type);
        Log.d("addOrder", "callAddOrder: " + total);
        Log.d("addOrder", "callAddOrder: " + chequeNo);

        API api = MyApp.retrofit.create(API.class);
        Call<JsonObject> jsonObjectCall = api.addOrder(userId, type, total, chequeNo);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                        finish();
                    }
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCartItemClick(int pos, CartData productData) {

        quentityUpdateDialog(productData,pos);
    }

    private void quentityUpdateDialog(CartData productData, int pos) {


        qtydialog= new Dialog(CartListActivity.this);
        qtydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtydialog.setContentView(R.layout.dialog_quenty_chamge);
        qtydialog.setCancelable(false);
        Objects.requireNonNull(qtydialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        qtydialog.getWindow().setGravity(Gravity.CENTER);
        ImageView ivProduct = qtydialog.findViewById(R.id.ivProduct);
        TextView tvProduuctName = qtydialog.findViewById(R.id.tvProduct);
        TextView tvok = qtydialog.findViewById(R.id.tv_ok);
        TextView tvCancle = qtydialog.findViewById(R.id.tv_cancel);
        EditText edtQty = qtydialog.findViewById(R.id.tv_qty_value);



        tvProduuctName.setText(productData.getTitle());

        edtQty.setText(productData.getQuantity() + "");

        if (productData.getImage() != null &&
                                productData.getImage() != null &&
                !productData.getImage().equalsIgnoreCase("")) {

            Picasso.get().load(Constants.IMAGE_BASE_URL + productData.getImage()).
                    placeholder(R.drawable.ic_image_placeholder).
                    error(R.drawable.ic_image_placeholder).
                    into(ivProduct);
        }


        tvok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = edtQty.getText().toString();

                if (Function.isNetworkAvailable(mContext)) {
                    callAddToCartApi(productData, Integer.parseInt(qty), Constants.UPDATE,pos);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qtydialog.dismiss();
            }
        });


        qtydialog.show();

    }

    private void callAddToCartApi(CartData productData, int value, String type, int pos) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);
        Double price = Double.parseDouble(productData.getPrice()) * value;
//            Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();

        Log.d("parameter", "callAddToCartApi: " + Function.getPrefData(Preferences.USER_ID, mContext));
        Log.d("parameter", "callAddToCartApi: " + productData.getId());
        Log.d("parameter", "callAddToCartApi: " + price);
        Log.d("parameter", "callAddToCartApi: " + value);
        Call<JsonObject> jsonObjectCall = api.addToCart(Function.getPrefData(Preferences.USER_ID, mContext),
                String.valueOf(productData.getProductId()), String.valueOf(price), String.valueOf(value), "0");

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        data.get(pos).setCartQuantity(String.valueOf(value));

                        cartListAdapter.notifyDataSetChanged();
                        qtydialog.dismiss();
                        //notifyItemChanged(position);
//                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
