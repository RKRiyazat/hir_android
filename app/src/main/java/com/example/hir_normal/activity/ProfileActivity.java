package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hir_normal.R;
import com.example.hir_normal.imagecrop.CropHandler;
import com.example.hir_normal.imagecrop.CropHelper;
import com.example.hir_normal.imagecrop.CropParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity implements CropHandler, View.OnClickListener {

    private CropParams mCropParams;

    private TextView tvClickByCamera;
    private TextView tvPickFromGallery;
    private TextView tvCancel;
    private Context mContext;
    private Dialog dialogProfilePicture;
    private String imagePath = "";
    @BindView(R.id.ivProfile)
    ImageView ivProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mContext = ProfileActivity.this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
    }

    public void openGallery() {
        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = false;
        Intent intent = CropHelper.buildGalleryIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CROP);

    }



    private void openCamera() {

        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = false;

        Intent intent = CropHelper.buildCameraIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CAMERA);

    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }

    @Override
    public void onPhotoCropped(Uri uri) {

        if (!mCropParams.compress) {
            ivProfile.setImageURI(uri);
            imagePath = uri.getPath();
            Log.d("onPhotoCropped", "imagePath==" + imagePath);
            //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
            //                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                ivProfile.setImageBitmap(bitmap);

            /*Image image = new Image();
            image.setImage(imagePath);
            imagesAdapter.imageArrayList.add(image);
            imagesAdapter.notifyDataSetChanged();*/



           /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                    circularBitmapDrawable.setCircular(false);
                    iv_donate_item.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        }
    }

    @Override
    public void onCompressed(Uri uri) {
        imagePath = uri.getPath();
        //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
       /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(false);
                iv_donate_item.setImageDrawable(circularBitmapDrawable);
            }
        });*/
    }


    @Override
    public void onCancel() {
        //Toast.makeText(this, "Crop canceled!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed(String message) {
        //Toast.makeText(this, "Crop failed: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CropHelper.REQUEST_CROP) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == CropHelper.REQUEST_CAMERA) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        }

    }


    @OnClick(R.id.tvEditPhoto)
    public void click() {
        if (Build.VERSION.SDK_INT < 23) {
            showMenuDialogToSetProfilePic();
        } else {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showMenuDialogToSetProfilePic();
            } else {
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
            }
        }
    }


    private void showMenuDialogToSetProfilePic() {
        mCropParams = new CropParams(mContext);
        dialogProfilePicture = new Dialog(mContext, R.style.picture_dialog_style);
        dialogProfilePicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProfilePicture.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogProfilePicture.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogProfilePicture.getWindow().setAttributes(wlmp);
        dialogProfilePicture.show();

        tvClickByCamera = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickFromGallery = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCancel);

        tvPickFromGallery.setOnClickListener(this);
        tvClickByCamera.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == tvClickByCamera) {
            dialogProfilePicture.dismiss();
            openCamera();
        } else if (v == tvPickFromGallery) {
            // Dismiss the  Dialog
            dialogProfilePicture.dismiss();
            openGallery();
        } else if (v == tvCancel) {
            // Dismiss the  Dialog
            dialogProfilePicture.dismiss();
        }
    }


}
