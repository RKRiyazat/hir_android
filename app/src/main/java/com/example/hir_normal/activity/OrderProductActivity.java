package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.OrderProductAdapter;
import com.example.hir_normal.adapter.OrderStatusAdapter;
import com.example.hir_normal.apiCallBack.OrderHistoryCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.OrderHistory.Order;
import com.example.hir_normal.model.OrderHistory.OrderProduct;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderProductActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rvOrderProduct)
    RecyclerView rvOrderProduct;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String userId = "";
    private OrderProductAdapter orderProductAdapter;
    private ArrayList<OrderProduct> orderProductArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_product);
        mContext = OrderProductActivity.this;
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tv_title.setText(R.string.str_ordered_product);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        orderProductArrayList = new ArrayList<>();
        orderProductArrayList = (ArrayList<OrderProduct>) getIntent().getSerializableExtra(Constants.ORDER_PRODUCT_LIST);

        if (orderProductArrayList != null && orderProductArrayList.size() > 0) {
            tv_not_available.setVisibility(View.GONE);
            rvOrderProduct.setVisibility(View.VISIBLE);
            orderProductAdapter = new OrderProductAdapter(mContext, orderProductArrayList);
            rvOrderProduct.setLayoutManager(new LinearLayoutManager(mContext));
            rvOrderProduct.setAdapter(orderProductAdapter);
        } else {
            rvOrderProduct.setVisibility(View.GONE);
            tv_not_available.setVisibility(View.VISIBLE);
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });

    }



    @OnClick({R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
