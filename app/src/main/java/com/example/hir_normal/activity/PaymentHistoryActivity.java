package com.example.hir_normal.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.R;
import com.example.hir_normal.adapter.OrderHistoryAdapter;
import com.example.hir_normal.adapter.PaymentHistoryAdapter;
import com.example.hir_normal.adapter.RedeemListAdapter;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.model.EarningData;
import com.example.hir_normal.model.PaymentHistory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentHistoryActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rvPaymentHistory)
    RecyclerView rvPaymentHistory;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String userId = "";

    private PaymentHistoryAdapter paymentHistoryAdapter;
    private ArrayList<PaymentHistory> paymentHistoryArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);
        ButterKnife.bind(this);
        mContext = PaymentHistoryActivity.this;
        init();
    }

    private void init() {
        tv_title.setText(R.string.payment_history);
        userId = Function.getPrefData(Preferences.USER_ID, mContext);

        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        paymentHistoryArrayList = new ArrayList<>();
        paymentHistoryArrayList = (ArrayList<PaymentHistory>) getIntent().getSerializableExtra(Constants.PAYMENT_HISTORY_LIST);

        if (paymentHistoryArrayList != null && paymentHistoryArrayList.size() > 0) {
            paymentHistoryAdapter = new PaymentHistoryAdapter(mContext, paymentHistoryArrayList);
            rvPaymentHistory.setLayoutManager(new LinearLayoutManager(mContext));
            rvPaymentHistory.setAdapter(paymentHistoryAdapter);
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    @OnClick(R.id.iv_back)
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
