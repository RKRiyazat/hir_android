package com.example.hir_normal.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hir_normal.R;
import com.example.hir_normal.databinding.ActivityLoginSelectionBinding;

import java.util.Objects;


public class LoginSelectionActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityLoginSelectionBinding activityLoginSelectionBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityLoginSelectionBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_selection);

        //setContentView(R.layout.activity_login_selection);

        init();
    }

    private void init() {


        activityLoginSelectionBinding.tvSignIn.setOnClickListener(this);
        activityLoginSelectionBinding.tvSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tv_sign_in:
                startActivity(new Intent(LoginSelectionActivity.this, LoginActivity.class));
                break;
            case R.id.tv_sign_up:
                showDialogue();

                break;
        }

    }

    private void showDialogue() {
        Dialog dialog = new Dialog(LoginSelectionActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_select_user);
        Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        TextView tv_provider = dialog.findViewById(R.id.tv_provider);
        TextView tv_distributor = dialog.findViewById(R.id.tv_distributor);

        tv_provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(LoginSelectionActivity.this, ApplicatorRegisterActivity.class));
            }
        });

        tv_distributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(LoginSelectionActivity.this, DistributorRegisterActivity.class));
            }
        });
        dialog.show();
    }
}
