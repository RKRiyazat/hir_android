package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.CategoryAdapter;
import com.example.hir_normal.adapter.ProductListAdapter;
import com.example.hir_normal.adapter.VideoAdapter;
import com.example.hir_normal.apiCallBack.ProductListCallBack;
import com.example.hir_normal.apiCallBack.VideoListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.VideoData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoListActivity extends AppCompatActivity {

    @BindView(R.id.rvVideo)
    RecyclerView rvVideo;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private ArrayList<VideoData> videoDataArrayList;
    private VideoAdapter videoAdapter;
    private String classId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        mContext = VideoListActivity.this;
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();
    }

    private void init() {
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);
        tv_title.setText(R.string.video);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });

        if (Function.isNetworkAvailable(mContext)) {
            getVideoData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }

    private void getVideoData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<VideoListCallBack> videoListCallBackCall = api.getVideoList(classId);
        videoListCallBackCall.enqueue(new Callback<VideoListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<VideoListCallBack> call, @NonNull Response<VideoListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    videoDataArrayList = new ArrayList<>();
                    videoDataArrayList = (ArrayList<VideoData>) response.body().getData();
                    if (videoDataArrayList != null && videoDataArrayList.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvVideo.setVisibility(View.VISIBLE);

                        videoAdapter = new VideoAdapter(mContext, videoDataArrayList);
                        rvVideo.setLayoutManager(new LinearLayoutManager(mContext));
                        rvVideo.setAdapter(videoAdapter);
                    } else {
                        rvVideo.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<VideoListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
