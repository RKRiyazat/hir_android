package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.R;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;

public class PdfViewerActivity extends AppCompatActivity{
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.iv_share)
    ImageView iv_share;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        mContext = PdfViewerActivity.this;
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        tv_title.setText(getString(R.string.pdf_view));
        iv_share.setVisibility(View.VISIBLE);
        url = Constants.IMAGE_BASE_URL + getIntent().getStringExtra(Constants.PDF_URL);

        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });

        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody1 = url;
                Intent sharingIntent1 = new Intent(Intent.ACTION_SEND);
                sharingIntent1.setType("text/plain");
                sharingIntent1.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.share));
                sharingIntent1.putExtra(Intent.EXTRA_TEXT, shareBody1);
                mContext.startActivity(Intent.createChooser(sharingIntent1, mContext.getResources().getString(R.string.share_using)));
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
    }

}
