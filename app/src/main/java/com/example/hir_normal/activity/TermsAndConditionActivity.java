package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hir_normal.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermsAndConditionActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_terms_and_conditions)
    TextView tv_terms_and_conditions;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        ButterKnife.bind(this);
        mContext = TermsAndConditionActivity.this;
        init();
    }

    public void init() {
        tv_title.setText(R.string.str_terms_and_conditions);

        tv_terms_and_conditions.setText(getString(R.string.terms_and_conditions));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
