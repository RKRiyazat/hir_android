package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.databinding.ActivityChangePasswordBinding;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityChangePasswordBinding activityChangePasswordBinding;

    private String oldPassword = "";
    private String newPassword = "";
    private String confirmPassword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityChangePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);


        init();


    }

    private void init() {

        if (Function.getPrefData(Preferences.USER_ROLE, this).equalsIgnoreCase("1")) {
            activityChangePasswordBinding.headerLayoutView.tvViewCart.setVisibility(View.VISIBLE);
            activityChangePasswordBinding.headerLayoutView.tvViewCart.setText(getString(R.string.rewards));
        }

        activityChangePasswordBinding.headerLayoutView.tvTitle.setText(R.string.str_change_password);

        activityChangePasswordBinding.headerLayoutView.ivBack.setOnClickListener(this);
        activityChangePasswordBinding.headerLayoutView.tvViewCart.setOnClickListener(this);

        activityChangePasswordBinding.tvChangePassword.setOnClickListener(this);
    }


    private void callApiForForgotPassword(String oldPassword, String newPassword) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(ChangePasswordActivity.this, "", "please wait...");
        dialog.show();


        Call<JsonObject> jsonObjectCall = api.changePassword(Function.getPrefData(Preferences.USER_ID, ChangePasswordActivity.this), oldPassword, newPassword);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(ChangePasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ChangePasswordActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:

                finish();
                break;

            case R.id.tvViewCart:

            startActivity(new Intent(ChangePasswordActivity.this, PointHistoryActivity.class));
            break;

            case R.id.tv_change_password:

                oldPassword = activityChangePasswordBinding.etOldPassword.getText().toString();
                newPassword = activityChangePasswordBinding.etNewPassword.getText().toString();
                confirmPassword = activityChangePasswordBinding.etConfirmPassword.getText().toString();

                if (checkValidation()) {

                    if (Function.isNetworkAvailable(ChangePasswordActivity.this)) {
                        //getStateApiCall();

                        callApiForForgotPassword(oldPassword, newPassword);

                    } else {
                        Toast.makeText(ChangePasswordActivity.this, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }

                }

                break;
        }
    }

    private boolean checkValidation() {
        boolean isValidField = false;
        if (oldPassword.isEmpty()) {
            Toast.makeText(ChangePasswordActivity.this, R.string.str_please_enter_old_password, Toast.LENGTH_LONG).show();
            isValidField = false;
        } else if (newPassword.isEmpty()) {
            Toast.makeText(ChangePasswordActivity.this, R.string.str_please_enter_old_password, Toast.LENGTH_LONG).show();
            isValidField = false;
        } else if (confirmPassword.isEmpty()) {
            Toast.makeText(ChangePasswordActivity.this, R.string.str_please_enter_confirm_password, Toast.LENGTH_LONG).show();
            isValidField = false;
        } else if (!newPassword.equals(confirmPassword)) {
            Toast.makeText(ChangePasswordActivity.this, R.string.str_new_password_confirm_password_not_match, Toast.LENGTH_LONG).show();
            isValidField = false;
        } else {
            isValidField = true;
        }
        return isValidField;
    }
}
