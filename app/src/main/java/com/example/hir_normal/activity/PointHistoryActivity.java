package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.EarningListAdapter;
import com.example.hir_normal.adapter.OrderGiftStatusAdapter;
import com.example.hir_normal.adapter.RedeemListAdapter;
import com.example.hir_normal.apiCallBack.AvailableEarningCallBack;
import com.example.hir_normal.apiCallBack.GiftOrderHistoryCallBack;
import com.example.hir_normal.apiCallBack.RedeemEarningCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.EarningData;
import com.example.hir_normal.model.GiftOrderHistoryData;
import com.example.hir_normal.model.RedeemData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointHistoryActivity extends AppCompatActivity {


    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tvAvailable)
    TextView tvAvailable;
    @BindView(R.id.tvRedeem)
    TextView tvRedeem;
    @BindView(R.id.clText)
    ConstraintLayout clText;
    @BindView(R.id.rvEarning)
    RecyclerView rvEarning;
    @BindView(R.id.rvRedeem)
    RecyclerView rvRedeem;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvTotalRedeem)
    TextView tvTotalRedeem;

    private Context mContext;
    private String userId = "";

    private EarningListAdapter earningListAdapter;
    private RedeemListAdapter redeemListAdapter;
    private ArrayList<EarningData> earningDataArrayList;
    private ArrayList<RedeemData> redeemDataArrayList;
    private OrderGiftStatusAdapter orderGiftStatusAdapter;
    private ArrayList<GiftOrderHistoryData> giftOrderHistoryDataArrayList;

    private boolean idApiCalled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_history);
        mContext = PointHistoryActivity.this;
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();
    }

    private void init() {
        tv_title.setText(R.string.point_history);
        userId = Function.getPrefData(Preferences.USER_ID, mContext);

        Log.d("userId", "init: " + userId);

        if (Function.isNetworkAvailable(mContext)) {
            getAvailableData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }


    @OnClick({R.id.tvAvailable, R.id.tvRedeem, R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvAvailable:
                tvAvailable.setBackground(ContextCompat.getDrawable(mContext, R.drawable.orange_fill_bg));
                tvAvailable.setTextColor(getResources().getColor(R.color.colorWhite));
                tvRedeem.setBackgroundResource(0);
                tvRedeem.setTextColor(getResources().getColor(R.color.colorBoxSix));
                rvRedeem.setVisibility(View.GONE);
                tv_not_available.setVisibility(View.GONE);
                tvTotalRedeem.setVisibility(View.GONE);
                tvTotal.setVisibility(View.VISIBLE);
                if (earningDataArrayList != null && earningDataArrayList.size() > 0) {
                    rvEarning.setVisibility(View.VISIBLE);
                } else {
                    tv_not_available.setText(getString(R.string.no_earning_available));
                    tv_not_available.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tvRedeem:
                tvRedeem.setBackground(ContextCompat.getDrawable(mContext, R.drawable.orange_fill_bg));
                tvRedeem.setTextColor(getResources().getColor(R.color.colorWhite));
                tvAvailable.setBackgroundResource(0);
                tvAvailable.setTextColor(getResources().getColor(R.color.colorBoxSix));
                rvEarning.setVisibility(View.GONE);
                tv_not_available.setVisibility(View.GONE);
                tvTotal.setVisibility(View.GONE);
                tvTotalRedeem.setVisibility(View.VISIBLE);
                if (!idApiCalled) {
                    if (Function.isNetworkAvailable(mContext)) {
//                        getRedeemData();
                        getGiftOrderHistoryData();
                    } else {
//                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        Function.showDialogue(mContext);
                    }
                } else {
                    if (giftOrderHistoryDataArrayList != null && giftOrderHistoryDataArrayList.size() > 0) {
                        rvRedeem.setVisibility(View.VISIBLE);
                    } else {
                        tv_not_available.setText(getString(R.string.no_redeem_available));
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void getAvailableData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<AvailableEarningCallBack> availableEarningCallBackCall = api.getAvailableEarning(userId);
        availableEarningCallBackCall.enqueue(new Callback<AvailableEarningCallBack>() {
            @Override
            public void onResponse(@NonNull Call<AvailableEarningCallBack> call, @NonNull Response<AvailableEarningCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equalsIgnoreCase("200")) {
                    earningDataArrayList = new ArrayList<>();
                    earningDataArrayList = (ArrayList<EarningData>) response.body().getData();
                    if (earningDataArrayList != null && earningDataArrayList.size() > 0) {
                        rvEarning.setVisibility(View.VISIBLE);
                        tv_not_available.setVisibility(View.GONE);

                        earningListAdapter = new EarningListAdapter(mContext, earningDataArrayList);
                        rvEarning.setLayoutManager(new LinearLayoutManager(mContext));
                        rvEarning.setAdapter(earningListAdapter);

                        int total = 0;
                        for (int i = 0; i < earningDataArrayList.size(); i++) {

                            total = total + Integer.parseInt(earningDataArrayList.get(i).getPoints());
                        }
                        tvTotal.setVisibility(View.VISIBLE);
                        tvTotal.setText(getString(R.string.str_total_earning_points) + total + "");
                    } else {
                        tv_not_available.setText(getString(R.string.no_earning_available));
                        tv_not_available.setVisibility(View.VISIBLE);
                        rvEarning.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<AvailableEarningCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getRedeemData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<RedeemEarningCallBack> redeemEarningCallBackCall = api.getRedeemEarning(userId);
        redeemEarningCallBackCall.enqueue(new Callback<RedeemEarningCallBack>() {
            @Override
            public void onResponse(@NonNull Call<RedeemEarningCallBack> call, @NonNull Response<RedeemEarningCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equalsIgnoreCase("200")) {
                    redeemDataArrayList = new ArrayList<>();
                    redeemDataArrayList = (ArrayList<RedeemData>) response.body().getData();
                    if (redeemDataArrayList != null && redeemDataArrayList.size() > 0) {
                        rvRedeem.setVisibility(View.VISIBLE);
                        tv_not_available.setVisibility(View.GONE);

                        redeemListAdapter = new RedeemListAdapter(mContext, response.body().getData());
                        rvRedeem.setLayoutManager(new LinearLayoutManager(mContext));
                        rvRedeem.setAdapter(redeemListAdapter);

                        int total = 0;
                        for (int i = 0; i < redeemDataArrayList.size(); i++) {

                            total = total + Integer.parseInt(redeemDataArrayList.get(i).getPoints());
                        }
                        tvTotalRedeem.setVisibility(View.VISIBLE);
                        tvTotalRedeem.setText(getString(R.string.str_total_redemed_points) + total + "");
                    } else {
                        tv_not_available.setText(getString(R.string.no_redeem_available));
                        tv_not_available.setVisibility(View.VISIBLE);
                        rvRedeem.setVisibility(View.GONE);
                    }
                    idApiCalled = true;
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RedeemEarningCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getGiftOrderHistoryData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<GiftOrderHistoryCallBack> giftOrderHistoryCallBackCall = api.getGiftOrderHistory(userId);
        giftOrderHistoryCallBackCall.enqueue(new Callback<GiftOrderHistoryCallBack>() {
            @Override
            public void onResponse(@NonNull Call<GiftOrderHistoryCallBack> call, @NonNull Response<GiftOrderHistoryCallBack> response) {
                dialog.dismiss();
                if (response.code() == 200) {

                   /* tv_total_order_point_value.setText(response.body().getData().getTotalPurchaseAmount().toString());
                    tv_total_pending_point_value.setText(response.body().getData().getTotalPendingAmount().toString());*/

                    rvRedeem.setVisibility(View.VISIBLE);
                    tv_not_available.setVisibility(View.GONE);

                    giftOrderHistoryDataArrayList = new ArrayList<>();
                    giftOrderHistoryDataArrayList = (ArrayList<GiftOrderHistoryData>) response.body().getData();
                    if (giftOrderHistoryDataArrayList != null && giftOrderHistoryDataArrayList.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvRedeem.setVisibility(View.VISIBLE);
                        orderGiftStatusAdapter = new OrderGiftStatusAdapter(mContext, giftOrderHistoryDataArrayList);
                        rvRedeem.setLayoutManager(new LinearLayoutManager(mContext));
                        rvRedeem.setAdapter(orderGiftStatusAdapter);

                        Double total = 0.0;
                        for (int i = 0; i < giftOrderHistoryDataArrayList.size(); i++) {
                            total = total + Double.valueOf(giftOrderHistoryDataArrayList.get(i).getTotal());
                        }
                        tvTotalRedeem.setText(getString(R.string.str_total_redemed_points) + total + "");
                    } else {
                        rvRedeem.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                } else {
                    tv_not_available.setText(getString(R.string.no_redeem_available));
                    tv_not_available.setVisibility(View.VISIBLE);
                    rvRedeem.setVisibility(View.GONE);
                }
                idApiCalled = true;
            }

            @Override
            public void onFailure(@NonNull Call<GiftOrderHistoryCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
