package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.CityListCallBack;
import com.example.hir_normal.apiCallBack.CountryListCallBack;
import com.example.hir_normal.apiCallBack.StateListCallBack;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.imagecrop.BitmapUtil;
import com.example.hir_normal.imagecrop.CropHandler;
import com.example.hir_normal.imagecrop.CropHelper;
import com.example.hir_normal.imagecrop.CropParams;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.City;
import com.example.hir_normal.model.States;
import com.example.hir_normal.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistributorProfileActivity extends AppCompatActivity implements CropHandler {

    private Context mContext;
    ArrayList<City> cityArrayList;
    ArrayList<States> stateArrayList;
    private String selectedCityId = "";
    private String selectedStateId = "";
    private String selectedCountryId = "101";
    private CropParams mCropParams;
    private TextView tvClickByCamera;
    private TextView tvPickFromGallery;
    private TextView tvCancel;
    private Dialog dialogProfilePicture;
    private String imagePath = "";
    private String passBookImage1 = "";
    private String passBookImage2 = "";


    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_code_value)
    TextView tv_code_value;
    @BindView(R.id.sp_state)
    Spinner sp_state;
    @BindView(R.id.sp_city)
    Spinner sp_city;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.tv_birth_date)
    TextView tv_birth_date;
    @BindView(R.id.tv_marriage_anniversary_date)
    TextView tv_marriage_anniversary_date;
    @BindView(R.id.et_name_and_code_of_distributor)
    EditText et_name_and_code_of_distributor;
    @BindView(R.id.btnGst)
    Button btnGst;
    @BindView(R.id.ivGSTImage)
    ImageView ivGSTImage;
    @BindView(R.id.et_gst_number)
    EditText et_gst_number;
    @BindView(R.id.et_pancard_number)
    EditText et_pancard_number;
    @BindView(R.id.btnPanCard)
    Button btnPanCard;
    @BindView(R.id.ivPanCardImage)
    ImageView ivPanCardImage;
    @BindView(R.id.et_firm_name)
    EditText et_firm_name;
    @BindView(R.id.rbPartnership)
    RadioButton rbPartnership;
    @BindView(R.id.rbProperitory)
    RadioButton rbProperitory;
    @BindView(R.id.et_concern_person)
    EditText et_concern_person;
    @BindView(R.id.et_order_person)
    EditText et_order_person;
    @BindView(R.id.et_bank_name)
    EditText et_bank_name;
    @BindView(R.id.et_account_number)
    EditText et_account_number;
    @BindView(R.id.et_ifsc_code)
    EditText et_ifsc_code;
    @BindView(R.id.et_branch)
    EditText et_branch;
    @BindView(R.id.et_holder_name)
    EditText et_holder_name;
    @BindView(R.id.btnPassbook1)
    Button btnPassbook1;
    @BindView(R.id.ivPassbookImage1)
    ImageView ivPassbookImage1;
    @BindView(R.id.btnPassbook2)
    Button btnPassbook2;
    @BindView(R.id.ivPassbookImage2)
    ImageView ivPassbookImage2;
    @BindView(R.id.iv_user_profile)
    ImageView iv_user_profile;

    private String selectedImage = "";
    MultipartBody.Part profileImage = null;
    MultipartBody.Part gstImage = null;
    MultipartBody.Part panCardImage = null;
    MultipartBody.Part passbookImage1 = null;
    MultipartBody.Part passbookImage2 = null;
    File profileImageFile = null, gstImageFile = null, panCardImageFile = null, passbookFile1 = null, passbookFile2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_profile);
        mContext = DistributorProfileActivity.this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.img_bg));
        tv_title.setText(R.string.profile);

        cityArrayList = new ArrayList<>();
        City city = new City();
        city.setName("Select City");
        cityArrayList.add(city);

        stateArrayList = new ArrayList<>();
        States states = new States();
        states.setName("Select State");
        stateArrayList.add(states);

        ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_state.setAdapter(dataAdapter);

        ArrayAdapter<City> dataAdapterCity = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
        dataAdapterCity.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_city.setAdapter(dataAdapterCity);

        if (Function.isNetworkAvailable(mContext)) {
            getStateApiCall();
        } else {
            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
        }

        setData();
    }


    private void setData() {
        et_email.setText(Function.getPrefData(Preferences.USER_EMAIL, mContext));
        et_name.setText(Function.getPrefData(Preferences.USER_NAME, mContext));
        et_mobile_number.setText(Function.getPrefData(Preferences.USER_MOBILE, mContext));
//        tv_birth_date.setText(Function.getPrefData(Preferences.USER_DOB, mContext));
//        tv_marriage_anniversary_date.setText(Function.getPrefData(Preferences.USER_ANNIVERSERY, mContext));
        et_gst_number.setText(Function.getPrefData(Preferences.USER_GST_NUMBER, mContext));
        et_pancard_number.setText(Function.getPrefData(Preferences.USER_PAN_CARD_NUMBER, mContext));
        et_concern_person.setText(Function.getPrefData(Preferences.USER_CONCERN_PERSON, mContext));
        et_order_person.setText(Function.getPrefData(Preferences.USER_ORDER_PERSON, mContext));
        et_name_and_code_of_distributor.setText(Function.getPrefData(Preferences.DISTRIBUTORE_NAME, mContext));
        et_firm_name.setText(Function.getPrefData(Preferences.USER_FIRM_NAME, mContext));
        et_bank_name.setText(Function.getPrefData(Preferences.USER_BANK_NAME, mContext));
        et_account_number.setText(Function.getPrefData(Preferences.USER_BANK_ACCOUNT_NUMBER, mContext));
        et_ifsc_code.setText(Function.getPrefData(Preferences.USER_BANK_IFSC_CODE, mContext));
        et_branch.setText(Function.getPrefData(Preferences.USER_BANK_BRANCH, mContext));
        et_holder_name.setText(Function.getPrefData(Preferences.USER_BANK_HOLDER_NAME, mContext));
        et_name_and_code_of_distributor.setText(Function.getPrefData(Preferences.DISTRIBUTOR_CODE, mContext));

        String birthDate = Function.dateFormatWithoutChange(Function.getPrefData(Preferences.USER_DOB, mContext));
        if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
            tv_birth_date.setText(birthDate);
        }

        String mADate = Function.dateFormatWithoutChange(Function.getPrefData(Preferences.USER_ANNIVERSERY, mContext));
        if (mADate != null && !mADate.equalsIgnoreCase("")) {
            tv_marriage_anniversary_date.setText(mADate);
        }

        String gstImage = "", panCardImage = "", userImage = "";

        gstImage = Function.getPrefData(Preferences.USER_GST_IMAGE, mContext);
        panCardImage = Function.getPrefData(Preferences.USER_PAN_CARD_IMAGE, mContext);
        userImage = Function.getPrefData(Preferences.USER_IMAGE, mContext);

        passBookImage1 = Function.getPrefData(Preferences.USER_BANK_PASS_BOOK_1, mContext);
        passBookImage2 = Function.getPrefData(Preferences.USER_BANK_PASS_BOOK_2, mContext);


        if (passBookImage1 != null && !passBookImage1.equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + passBookImage1)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPassbookImage1);
        }

        if (passBookImage2 != null && !passBookImage2.equalsIgnoreCase("")) {

            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + passBookImage2)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPassbookImage2);
        }


        ivGSTImage.setVisibility(View.VISIBLE);
        ivPanCardImage.setVisibility(View.VISIBLE);
        if (userImage != null &&
                !userImage.equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + userImage)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(iv_user_profile);
        } else {
            iv_user_profile.setImageResource(R.drawable.ic_image_placeholder);
        }

        if (gstImage != null &&
                !gstImage.equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + gstImage)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivGSTImage);
        } else {
            ivGSTImage.setImageResource(R.drawable.ic_image_placeholder);
        }

        if (panCardImage != null &&
                !panCardImage.equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + panCardImage)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPanCardImage);
        } else {
            ivPanCardImage.setImageResource(R.drawable.ic_image_placeholder);
        }

        String firmType = "";
        firmType = Function.getPrefData(Preferences.USER_FIRM_TYPE, mContext);
        if (firmType != null && firmType.equalsIgnoreCase("2")) {
            rbProperitory.setChecked(true);
        } else {
            rbPartnership.setChecked(true);
        }

        selectedStateId = Function.getPrefData(Preferences.STATE_ID, mContext);
        selectedCityId = Function.getPrefData(Preferences.CITY_ID, mContext);
    }

    private void getStateApiCall() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<StateListCallBack> stateListCallBackCall = api.getStates(selectedCountryId);
        stateListCallBackCall.enqueue(new Callback<StateListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<StateListCallBack> call, @NonNull Response<StateListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        stateArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_state.setAdapter(dataAdapter);
                    sp_state.setSelection(0);

                    if (selectedStateId != null && !selectedStateId.equalsIgnoreCase("")) {
                        for (int j = 0; j < stateArrayList.size(); j++) {
                            if (stateArrayList.get(j).getId() != null && stateArrayList.get(j).getId() == Integer.parseInt(selectedStateId)) {
                                sp_state.setSelection(j);
                            }
                        }
                    } else {
                        sp_state.setSelection(0);
                    }

                    sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedStateId = "";
                                cityArrayList = new ArrayList<>();
                                City city = new City();
                                city.setName("Select City");

                                cityArrayList.add(city);
                                ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                                sp_city.setAdapter(dataAdapter);
                                sp_city.setSelection(0);
                            } else {
                                selectedStateId = stateArrayList.get(i).getId().toString();
                                getCityApiCall(selectedStateId);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<StateListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCityApiCall(String stateId) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CityListCallBack> cityListCallBackCall = api.getCities(stateId);
        cityListCallBackCall.enqueue(new Callback<CityListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CityListCallBack> call, @NonNull Response<CityListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        cityArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_city.setAdapter(dataAdapter);
                    sp_city.setSelection(0);

                    if (selectedCityId != null && !selectedCityId.equalsIgnoreCase("")) {
                        for (int j = 0; j < cityArrayList.size(); j++) {
                            if (cityArrayList.get(j).getId() != null && cityArrayList.get(j).getId() == Integer.parseInt(selectedCityId)) {
                                sp_city.setSelection(j);
                            }
                        }
                    } else {
                        sp_city.setSelection(0);
                    }

                    sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedCityId = "";
                            } else {
                                selectedCityId = cityArrayList.get(i).getId().toString();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CityListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.iv_user_profile, R.id.tv_birth_date, R.id.tv_marriage_anniversary_date, R.id.tv_sign_up, R.id.btnGst, R.id.btnPassbook1, R.id.btnPassbook2, R.id.btnPanCard, R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_birth_date:
                Function.openDatePicker(mContext, tv_birth_date);
                break;
            case R.id.tv_marriage_anniversary_date:
                Function.openDatePicker(mContext, tv_marriage_anniversary_date);
                break;
            case R.id.tv_sign_up:
                String email = et_email.getText().toString();
                String name = et_name.getText().toString();
                String deviceToken = "";
//                String anniversaryDate = tv_marriage_anniversary_date.getText().toString();
//                String dateOfBirth = tv_birth_date.getText().toString();

                String birthDate = Function.updateDateFormatWithoutChange(tv_birth_date.getText().toString());
                String marriageDate = Function.updateDateFormatWithoutChange(tv_marriage_anniversary_date.getText().toString());
                String anniversaryDate = "";
                String dateOfBirth = "";

                if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
                    dateOfBirth = birthDate;
                }

                if (marriageDate != null && !marriageDate.equalsIgnoreCase("")) {
                    anniversaryDate = marriageDate;
                }

                String stateId = selectedStateId;
                String cityId = selectedCityId;
                String distributorName = et_name_and_code_of_distributor.getText().toString();
                String mobileNo = et_mobile_number.getText().toString();
                String gstNumber = et_gst_number.getText().toString();
                String panCardNumber = et_pancard_number.getText().toString();
                String firmName = et_firm_name.getText().toString();
                String concernPerson = et_concern_person.getText().toString();
                String orderPerson = et_order_person.getText().toString();
                String bankName = et_bank_name.getText().toString();
                String accountNumber = et_account_number.getText().toString();
                String ifscCode = et_ifsc_code.getText().toString();
                String branchName = et_branch.getText().toString();
                String holderName = et_holder_name.getText().toString();

                String checkValidation = checkValidation(email, name, deviceToken, anniversaryDate, dateOfBirth,
                        stateId, cityId, distributorName, mobileNo, gstNumber, panCardNumber,
                        firmName, concernPerson, orderPerson, bankName, accountNumber, ifscCode, branchName, holderName);
                if (checkValidation.equals("")) {
                    if (Function.isNetworkAvailable(mContext)) {
                        callApi(email, name, deviceToken, anniversaryDate, dateOfBirth,
                                stateId, cityId, distributorName, mobileNo, gstNumber, panCardNumber,
                                firmName, concernPerson, orderPerson, bankName, accountNumber, ifscCode, branchName, holderName);
                    } else {
                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_user_profile:

                selectedImage = "profile";
                checkPermition();

                break;
            case R.id.btnGst:

                selectedImage = "gst";
                checkPermition();

                break;
            case R.id.btnPassbook1:

                selectedImage = "passbook1";
                checkPermition();

                break;
            case R.id.btnPassbook2:

                selectedImage = "passbook2";
                checkPermition();

                break;
            case R.id.btnPanCard:

                selectedImage = "pancard";
                checkPermition();

                break;
        }
    }


    public void openGallery() {
        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;
        Intent intent = CropHelper.buildGalleryIntent(mCropParams, DistributorProfileActivity.this);
        startActivityForResult(intent, CropHelper.REQUEST_CROP);

    }


    private void openCamera() {

        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;

        Intent intent = CropHelper.buildCameraIntent(mCropParams, DistributorProfileActivity.this);
        startActivityForResult(intent, CropHelper.REQUEST_CAMERA);

    }


    private void callApi(String email, String name, String deviceToken, String anniversary, String dateOfBirth,
                         String stateId, String cityId, String distributorName, String mobileNo,
                         String gstNo, String panCardNo, String firmName, String concernPerson,
                         String orderPerson, String bankName, String accountNumber, String ifscCode,
                         String branchName, String holderName) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        HashMap<String, RequestBody> data = new HashMap<>();

        RequestBody req_user_id = RequestBody.create(MediaType.parse("text/plain"), Function.getPrefData(Preferences.USER_ID, mContext));
        RequestBody req_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody req_password = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_name = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody req_device_token = RequestBody.create(MediaType.parse("text/plain"), deviceToken);
        RequestBody req_aniversary_date = RequestBody.create(MediaType.parse("text/plain"), anniversary);
        RequestBody req_distributor_name = RequestBody.create(MediaType.parse("text/plain"), distributorName);
        RequestBody req_state_id = RequestBody.create(MediaType.parse("text/plain"), stateId);
        RequestBody req_city_id = RequestBody.create(MediaType.parse("text/plain"), cityId);
        RequestBody req_role = RequestBody.create(MediaType.parse("text/plain"), "2");
        RequestBody req_dob = RequestBody.create(MediaType.parse("text/plain"), dateOfBirth);
        RequestBody req_mobile = RequestBody.create(MediaType.parse("text/plain"), mobileNo);
        RequestBody req_adhar_no = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_gst_no_text = RequestBody.create(MediaType.parse("text/plain"), gstNo);
        RequestBody req_pan_card_text = RequestBody.create(MediaType.parse("text/plain"), panCardNo);
        RequestBody req_firm_name = RequestBody.create(MediaType.parse("text/plain"), firmName);
        RequestBody req_firm_type;
        if (rbPartnership.isChecked()) {
            req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "1");
        } else {
            req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "2");
        }
        RequestBody req_concern_person = RequestBody.create(MediaType.parse("text/plain"), concernPerson);
        RequestBody req_order_person = RequestBody.create(MediaType.parse("text/plain"), orderPerson);
        RequestBody req_is_bank_details = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody req_bank_name = RequestBody.create(MediaType.parse("text/plain"), bankName);
        RequestBody req_account_no = RequestBody.create(MediaType.parse("text/plain"), accountNumber);
        RequestBody req_ifsc_code = RequestBody.create(MediaType.parse("text/plain"), ifscCode);
        RequestBody req_branch = RequestBody.create(MediaType.parse("text/plain"), branchName);
        RequestBody req_holder_name = RequestBody.create(MediaType.parse("text/plain"), holderName);

        if (profileImageFile != null) {
            RequestBody reqImgFileProfile = RequestBody.create(MediaType.parse("multipart/form-data"), profileImageFile);
            profileImage = MultipartBody.Part.createFormData("avatar", profileImageFile.getName(), reqImgFileProfile);
        } else {
            RequestBody reqImgFileProfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            profileImage = MultipartBody.Part.createFormData("avatar", "", reqImgFileProfile);
        }

        if (gstImageFile != null) {
            RequestBody reqImgFileGst = RequestBody.create(MediaType.parse("multipart/form-data"), gstImageFile);
            gstImage = MultipartBody.Part.createFormData("gst_no", gstImageFile.getName(), reqImgFileGst);
        } else {
            RequestBody reqImgFileGst = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            gstImage = MultipartBody.Part.createFormData("gst_no", "", reqImgFileGst);
        }

        if (panCardImageFile != null) {
            RequestBody reqImgFilePanCard = RequestBody.create(MediaType.parse("multipart/form-data"), panCardImageFile);
            panCardImage = MultipartBody.Part.createFormData("pan_card", panCardImageFile.getName(), reqImgFilePanCard);
        } else {
            RequestBody reqImgFilePanCard = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            panCardImage = MultipartBody.Part.createFormData("pan_card", "", reqImgFilePanCard);
        }

       /* if (gstImage == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            gstImage = MultipartBody.Part.createFormData("gst_no", "", reqimgfile);
        }
        if (panCardImage == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            panCardImage = MultipartBody.Part.createFormData("pan_card", "", reqimgfile);
        }
        */

        if (passbookImage1 == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            passbookImage1 = MultipartBody.Part.createFormData("image_1", "", reqimgfile);
        }
        if (passbookImage2 == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            passbookImage2 = MultipartBody.Part.createFormData("image_1", "", reqimgfile);
        }


        /*RequestBody req_gst_no = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyGst = MultipartBody.Part.createFormData("gst_no", "", req_gst_no);
        RequestBody req_pan_card = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyPanCard = MultipartBody.Part.createFormData("pan_card", "", req_pan_card);
        RequestBody req_image_1 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage1 = MultipartBody.Part.createFormData("image_1", "", req_image_1);
        RequestBody req_image_2 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage2 = MultipartBody.Part.createFormData("image_2", "", req_image_2);*/

        data.put("user_id", req_user_id);
        data.put("email", req_email);
        data.put("password", req_password);
        data.put("name", req_name);
        data.put("device_token", req_device_token);
        data.put("aniversary_date", req_aniversary_date);
        data.put("distributor_name", req_distributor_name);
        data.put("state_id", req_state_id);
        data.put("city_id", req_city_id);
        data.put("role", req_role);
        data.put("dob", req_dob);
        data.put("mobile", req_mobile);
        data.put("adhar_no", req_adhar_no);
        data.put("gst_no_text", req_gst_no_text);
        data.put("pan_card_text", req_pan_card_text);
        data.put("firm_name", req_firm_name);
        data.put("firm_type", req_firm_type);
        data.put("concern_person", req_concern_person);
        data.put("order_person", req_order_person);
        data.put("is_bank_details", req_is_bank_details);
        data.put("bank_name", req_bank_name);
        data.put("account_no", req_account_no);
        data.put("ifsc_code", req_ifsc_code);
        data.put("branch", req_branch);
        data.put("holder_name", req_holder_name);

        Call<LoginResponseModel> jsonObjectCall = api.updateProfile(data, profileImage, gstImage, panCardImage, passbookImage1, passbookImage2);
        jsonObjectCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Function.setPrefData(Preferences.USER_IMAGE, response.body().getLoginData().getAvatar(), mContext);
                        Function.setPrefData(Preferences.USER_EMAIL, response.body().getLoginData().getEmail(), mContext);
                        Function.setPrefData(Preferences.USER_ID, response.body().getLoginData().getId().toString(), mContext);
                        Function.setPrefData(Preferences.USER_NAME, response.body().getLoginData().getName(), mContext);
                        Function.setPrefData(Preferences.USER_MOBILE, response.body().getLoginData().getMobile(), mContext);
                        Function.setPrefData(Preferences.USER_ADHAR, response.body().getLoginData().getAdharNo(), mContext);
                        Function.setPrefData(Preferences.USER_DOB, response.body().getLoginData().getDob(), mContext);
                        Function.setPrefData(Preferences.USER_ANNIVERSERY, response.body().getLoginData().getAniversaryDate(), mContext);
                        Function.setPrefData(Preferences.STATE_ID, response.body().getLoginData().getStateId(), mContext);
                        Function.setPrefData(Preferences.CITY_ID, response.body().getLoginData().getCityId(), mContext);
                        Function.setPrefData(Preferences.USER_FIRM_TYPE, response.body().getLoginData().getFirmType(), mContext);
                        Function.setPrefData(Preferences.USER_FIRM_NAME, response.body().getLoginData().getFirmName(), mContext);
                        Function.setPrefData(Preferences.USER_PAN_CARD_IMAGE, response.body().getLoginData().getPanCard(), mContext);
                        Function.setPrefData(Preferences.USER_PAN_CARD_NUMBER, response.body().getLoginData().getPanCardText(), mContext);
                        Function.setPrefData(Preferences.USER_GST_IMAGE, response.body().getLoginData().getGstNo(), mContext);
                        Function.setPrefData(Preferences.USER_GST_NUMBER, response.body().getLoginData().getGstNoText(), mContext);
                        Function.setPrefData(Preferences.DISTRIBUTORE_NAME, response.body().getLoginData().getDistributorName(), mContext);
                        Function.setPrefData(Preferences.USER_CONCERN_PERSON, response.body().getLoginData().getConcernPerson(), mContext);
                        Function.setPrefData(Preferences.USER_ORDER_PERSON, response.body().getLoginData().getOrderPerson(), mContext);


                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }

    @Override
    public void onPhotoCropped(Uri uri) {

        if (!mCropParams.compress) {
//            ivProfile.setImageURI(uri);
            imagePath = uri.getPath();
            if (selectedImage.equals("profile")) {
//                iv_user_profile.setImageURI(uri);
                iv_user_profile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(mContext, uri));
                profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
                Glide.with(mContext)
                        .load(profileImageFile)
                        .placeholder(R.drawable.ic_image_placeholder)
                        .error(R.drawable.ic_image_placeholder)
                        .into(ivGSTImage);
            } else if (selectedImage.equals("gst")) {
                ivGSTImage.setVisibility(View.VISIBLE);
                ivGSTImage.setImageURI(uri);
                gstImageFile = new File(Objects.requireNonNull(uri.getPath()));
            } else if (selectedImage.equals("passbook1")) {
                ivPassbookImage1.setVisibility(View.VISIBLE);
                ivPassbookImage1.setImageURI(uri);
                passbookFile1 = new File(Objects.requireNonNull(uri.getPath()));
            } else if (selectedImage.equals("passbook2")) {
                ivPassbookImage2.setVisibility(View.VISIBLE);
                ivPassbookImage2.setImageURI(uri);
                passbookFile2 = new File(Objects.requireNonNull(uri.getPath()));
            } else {
                ivPanCardImage.setVisibility(View.VISIBLE);
                ivPanCardImage.setImageURI(uri);
                panCardImageFile = new File(Objects.requireNonNull(uri.getPath()));
            }
            Log.d("onPhotoCropped", "imagePath==" + imagePath);
            //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
            //                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                ivProfile.setImageBitmap(bitmap);

            /*Image image = new Image();
            image.setImage(imagePath);
            imagesAdapter.imageArrayList.add(image);
            imagesAdapter.notifyDataSetChanged();*/



           /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                    circularBitmapDrawable.setCircular(false);
                    iv_donate_item.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        }
    }

    @Override
    public void onCompressed(Uri uri) {
        imagePath = uri.getPath();
        if (selectedImage.equals("profile")) {
            profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
            Glide.with(mContext)
                    .load(profileImageFile)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(iv_user_profile);
            Log.d("imagepicture", "onPhotoCropped: " + profileImageFile.length());
        } else if (selectedImage.equals("gst")) {
            ivGSTImage.setVisibility(View.VISIBLE);
//            ivGSTImage.setImageURI(uri);
            gstImageFile = new File(Objects.requireNonNull(uri.getPath()));
            Glide.with(mContext)
                    .load(gstImageFile)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivGSTImage);
        } else if (selectedImage.equals("passbook1")) {
            ivPassbookImage1.setVisibility(View.VISIBLE);
//            ivPassbookImage1.setImageURI(uri);
            passbookFile1 = new File(Objects.requireNonNull(uri.getPath()));
            Glide.with(mContext)
                    .load(passbookFile1)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPassbookImage1);
        } else if (selectedImage.equals("passbook2")) {
            ivPassbookImage2.setVisibility(View.VISIBLE);
//            ivPassbookImage2.setImageURI(uri);
            passbookFile2 = new File(Objects.requireNonNull(uri.getPath()));
            Glide.with(mContext)
                    .load(passbookFile2)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPassbookImage2);
        } else {
            ivPanCardImage.setVisibility(View.VISIBLE);
//            ivPanCardImage.setImageURI(uri);
            panCardImageFile = new File(Objects.requireNonNull(uri.getPath()));
            Glide.with(mContext)
                    .load(panCardImageFile)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(ivPanCardImage);
        }
        //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
       /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(false);
                iv_donate_item.setImageDrawable(circularBitmapDrawable);
            }
        });*/
    }


    @Override
    public void onCancel() {
        //Toast.makeText(this, "Crop canceled!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed(String message) {
        //Toast.makeText(this, "Crop failed: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropHelper.REQUEST_CROP) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == CropHelper.REQUEST_CAMERA) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        }

    }

    private void checkPermition() {
        if (Build.VERSION.SDK_INT < 23) {
            showMenuDialogToSetProfilePic();
        } else {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showMenuDialogToSetProfilePic();
            } else {
                Utils.checkPermitionCameraGaller(DistributorProfileActivity.this);
            }
        }
    }

    private void showMenuDialogToSetProfilePic() {
        mCropParams = new CropParams(mContext);
        dialogProfilePicture = new Dialog(mContext, R.style.picture_dialog_style);
        dialogProfilePicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProfilePicture.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogProfilePicture.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogProfilePicture.getWindow().setAttributes(wlmp);
        dialogProfilePicture.show();

        tvClickByCamera = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickFromGallery = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCancel);

        tvPickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
                openGallery();
            }
        });
        tvClickByCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
                openCamera();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
            }
        });
    }

    public String checkValidation(String email, String name, String deviceToken, String anniversary, String dateOfBirth,
                                  String stateId, String cityId, String distributorName, String mobileNo,
                                  String gstNo, String panCardNo, String firmName, String concernPerson,
                                  String orderPerson, String bankName, String accountNumber, String ifscCode,
                                  String branchName, String holderName) {
        if (TextUtils.isEmpty(name)) {
            return getString(R.string.enter_name);
        } else if (TextUtils.isEmpty(email)) {
            return getString(R.string.enter_email);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return getString(R.string.valid_email);
        } else if (TextUtils.isEmpty(mobileNo)) {
            return getString(R.string.enter_mobile);
        } else if (!Patterns.PHONE.matcher(mobileNo).matches()) {
            return getString(R.string.valid_mobile);
        } else if (TextUtils.isEmpty(anniversary)) {
            return getString(R.string.please_select_anniversary_date);
        } else if (TextUtils.isEmpty(dateOfBirth)) {
            return getString(R.string.please_select_date_of_birth);
        } else if (TextUtils.isEmpty(stateId)) {
            return getString(R.string.please_select_state);
        } else if (TextUtils.isEmpty(cityId)) {
            return getString(R.string.please_select_city);
        } else if (TextUtils.isEmpty(distributorName)) {
            return getString(R.string.enter_distributor_or_code);
        } else if (TextUtils.isEmpty(gstNo)) {
            return getString(R.string.enter_gst_no);
        } else if (TextUtils.isEmpty(panCardNo)) {
            return getString(R.string.enter_pan_card_no);
        } else if (TextUtils.isEmpty(firmName)) {
            return getString(R.string.enter_firm_name);
        } else if (TextUtils.isEmpty(concernPerson)) {
            return getString(R.string.enter_concern_person);
        } else if (TextUtils.isEmpty(orderPerson)) {
            return getString(R.string.enter_order_person);
        } else if (TextUtils.isEmpty(bankName)) {
            return getString(R.string.enter_bank_name);
        } else if (TextUtils.isEmpty(accountNumber)) {
            return getString(R.string.enter_account_number);
        } else if (TextUtils.isEmpty(ifscCode)) {
            return getString(R.string.enter_ifsc_code);
        } else if (TextUtils.isEmpty(branchName)) {
            return getString(R.string.enter_branch_name);
        } else if (TextUtils.isEmpty(holderName)) {
            return getString(R.string.enter_holder_name);
        } else {
            return "";
        }
    }
}
