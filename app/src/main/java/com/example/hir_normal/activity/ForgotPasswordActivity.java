package com.example.hir_normal.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    private ImageView iv_back;
    private TextView tv_title;
    private EditText et_email;
    private TextView tv_submit;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mContext = ForgotPasswordActivity.this;
        init();
    }

    public void init(){
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        et_email = findViewById(R.id.et_email);
        tv_submit = findViewById(R.id.tv_submit);

        tv_title.setText(getString(R.string.forgot_password_title));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkValidation = checkvalidation(et_email.getText().toString());
                if (checkValidation.equalsIgnoreCase("")) {
//                    apiCallForgotPassword(et_email.getText().toString());
                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String checkvalidation(String email) {
        if (TextUtils.isEmpty(email)) {
            return getString(R.string.enter_email);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return getString(R.string.valid_email);
        } else {
            return "";
        }
    }

   /* public void apiCallForgotPassword(String email) {

        if (Function.isNetworkAvailable(mContext)) {
            cl_progress.setVisibility(View.VISIBLE);
            API api = MyApp.retrofit.create(API.class);
            Call<LoginCallBack> typeInfoCallBackCall = api.forgotPassword(email);

            typeInfoCallBackCall.enqueue(new Callback<LoginCallBack>() {
                @Override
                public void onResponse(Call<LoginCallBack> call, Response<LoginCallBack> response) {
                    cl_progress.setVisibility(View.GONE);
                    if (response.code() == 500) {
                        Toast.makeText(mContext, "" + getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == 200 && response.body().getResult() == 1) {
//                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setMessage(response.body().getMessage());
                        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                        builder.show();
                        builder.setCancelable(false);
//                        finish();
                    } else {
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginCallBack> call, Throwable t) {
                    cl_progress.setVisibility(View.GONE);
                    Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }*/
}
