package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.GiftAdapter;
import com.example.hir_normal.apiCallBack.GiftListCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnGiftItemClick;
import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.ProductData;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftActivity extends AppCompatActivity implements OnGiftItemClick {

    @BindView(R.id.rvGift)
    RecyclerView rvGift;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private GiftAdapter giftAdapter;
    private ArrayList<GiftData> data;
    private Dialog qtyDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift);
        mContext = GiftActivity.this;
        ButterKnife.bind(this);
//        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tvViewCart.setVisibility(View.VISIBLE);
        if (Function.isNetworkAvailable(mContext)) {
            getGiftData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
        tv_title.setText(getString(R.string.reward_list));

        Log.d("userId", "init: " + Function.getPrefData(Preferences.USER_ID, mContext));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getGiftData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<GiftListCallBack> productListCallBackCall = api.getGift(Function.getPrefData(Preferences.USER_ID, mContext));
        productListCallBackCall.enqueue(new Callback<GiftListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<GiftListCallBack> call, @NonNull Response<GiftListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<GiftData>) response.body().getData();
                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvGift.setVisibility(View.VISIBLE);

                        giftAdapter = new GiftAdapter(mContext, data);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(GiftActivity.this);

                        //rvGift.setLayoutManager(new GridLayoutManager(mContext, 2));
                        rvGift.setLayoutManager(linearLayoutManager);
                        rvGift.setAdapter(giftAdapter);
                    } else {
                        rvGift.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<GiftListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onGiftItemClick(int pos, GiftData giftData) {
        quantityUpdateDialog(giftData, pos);
    }

    private void quantityUpdateDialog(GiftData giftData, int pos) {

        qtyDialog = new Dialog(GiftActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.dialog_quenty_chamge);
        qtyDialog.setCancelable(false);
        Objects.requireNonNull(qtyDialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        qtyDialog.getWindow().setGravity(Gravity.CENTER);
        ImageView ivProduct = qtyDialog.findViewById(R.id.ivProduct);
        TextView tvProduuctName = qtyDialog.findViewById(R.id.tvProduct);
        TextView tvok = qtyDialog.findViewById(R.id.tv_ok);
        TextView tvCancle = qtyDialog.findViewById(R.id.tv_cancel);
        EditText edtQty = qtyDialog.findViewById(R.id.tv_qty_value);

        tvProduuctName.setText(giftData.getName());

        edtQty.setText(giftData.getCartQuantity() + "");

        if (giftData.getImage() != null &&
                !giftData.getImage().equalsIgnoreCase("")) {
            Picasso.get().load(Constants.IMAGE_BASE_URL + giftData.getImage()).
                    placeholder(R.drawable.ic_image_placeholder).
                    error(R.drawable.ic_image_placeholder).
                    into(ivProduct);
        }


        tvok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = edtQty.getText().toString();

                if (Function.isNetworkAvailable(mContext)) {
                    callAddToCartApi(giftData, Integer.parseInt(qty), Constants.UPDATE, pos);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qtyDialog.dismiss();
            }
        });

        qtyDialog.show();

    }

    private void callAddToCartApi(GiftData giftData, int value, String type, int pos) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);
        Double price = Double.parseDouble(giftData.getPoints()) * value;
//            Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();

        Log.d("parameter", "callAddToCartApi: " + Function.getPrefData(Preferences.USER_ID, mContext));
        Log.d("parameter", "callAddToCartApi: " + giftData.getId());
        Log.d("parameter", "callAddToCartApi: " + price);
        Log.d("parameter", "callAddToCartApi: " + value);
        Call<JsonObject> jsonObjectCall = api.addGiftToCart(Function.getPrefData(Preferences.USER_ID, mContext),
                String.valueOf(giftData.getId()), String.valueOf(price), String.valueOf(value), "0");

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        data.get(pos).setCartQuantity(value);

                        giftAdapter.notifyDataSetChanged();
                        qtyDialog.dismiss();
                        //notifyItemChanged(position);
//                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.tvViewCart)
    public void click() {
        startActivity(new Intent(mContext, GiftCartListActivity.class));
    }
}
