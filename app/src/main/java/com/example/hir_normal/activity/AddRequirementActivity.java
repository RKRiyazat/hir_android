package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MainActivity;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.imagecrop.BitmapUtil;
import com.example.hir_normal.imagecrop.CropHandler;
import com.example.hir_normal.imagecrop.CropHelper;
import com.example.hir_normal.imagecrop.CropParams;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;
import retrofit2.http.Part;

public class AddRequirementActivity extends AppCompatActivity implements CropHandler {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.et_title)
    EditText et_title;
    @BindView(R.id.et_description)
    EditText et_description;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_qty)
    EditText et_qty;
    @BindView(R.id.tv_receive)
    TextView tv_receive;
    @BindView(R.id.tv_submit)
    TextView tv_submit;
    @BindView(R.id.btn_upload_image)
    Button btn_upload_image;
    @BindView(R.id.iv_image)
    ImageView iv_image;
    @BindView(R.id.btn_upload_video)
    Button btn_upload_video;
    @BindView(R.id.iv_video)
    ImageView iv_video;

    private Context mContext;
    private String requirementId = "";

    private String from = "";

    private Dialog dialogProfilePicture;
    private Dialog dialogVideo;
    private File profileImageFile = null;
    private File videoFile = null;
    private CropParams mCropParams;
    private String imagePath = "";
    private String videoPath = "";
    private static final int REQUEST_VIDEO = 1596;

    private static final int BUFFER_SIZE = 1024 * 2;
    private static final String IMAGE_DIRECTORY = "/HIR";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_requirement);
        ButterKnife.bind(this);
        mContext = AddRequirementActivity.this;
        init();
    }

    public void init() {
        tv_title.setText(getString(R.string.add_requirement));
        from = getIntent().getStringExtra(Constants.FROM);

        if (from.equalsIgnoreCase(Constants.UPDATE)) {
            requirementId = getIntent().getStringExtra(Constants.REQUIREMENT_ID);
            et_title.setText(getIntent().getStringExtra(Constants.REQUIREMENT_TITLE));
            et_description.setText(getIntent().getStringExtra(Constants.REQUIREMENT_DESCRIPTION));
            et_address.setText(getIntent().getStringExtra(Constants.REQUIREMENT_ADDRESS));
            et_qty.setText(getIntent().getStringExtra(Constants.REQUIREMENT_QTY));

            String image = getIntent().getStringExtra(Constants.REQUIREMENT_IMAGE);
            String video = getIntent().getStringExtra(Constants.REQUIREMENT_VIDEO);

            if (image != null && !image.equalsIgnoreCase("")) {
                iv_image.setVisibility(View.VISIBLE);
                Picasso.get().load(Constants.IMAGE_BASE_URL + image).into(iv_image);
            }

            if (video != null && !video.equalsIgnoreCase("")) {
                iv_video.setVisibility(View.VISIBLE);
                new DownloadImage(iv_video).execute(Constants.IMAGE_BASE_URL + video);
            }

            tv_submit.setText(getString(R.string.update));

            String receiveDateTextView = Function.dateFormatWithoutChangeShort(getIntent().getStringExtra(Constants.REQUIREMENT_RECEIVE_DATE));

            if (receiveDateTextView != null && !receiveDateTextView.equalsIgnoreCase("")) {
                tv_receive.setText(receiveDateTextView);
            }

        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_upload_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                };
                if (!hasPermissions(mContext, PERMISSIONS)) {
                    ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, 2);
                } else {
                    showMenuDialogForSelectVideo();
                }
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = et_title.getText().toString();
                String description = et_description.getText().toString();
                String address = et_address.getText().toString();
                String qty = et_qty.getText().toString();

                String receiveDateTextView = Function.updateDateFormatWithoutChange(tv_receive.getText().toString());
                String receiveDate = "";

                if (receiveDateTextView != null && !receiveDateTextView.equalsIgnoreCase("")) {
                    receiveDate = receiveDateTextView;
                }

                String checkValidation = checkValidation(title, description, address, qty, receiveDate);
                if (checkValidation.equals("")) {
                    if (Function.isNetworkAvailable(mContext)) {
                        callAddRequirementApi(title, description, address, qty, receiveDate);
                    } else {
                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Function.openDatePicker(mContext, tv_receive);
            }
        });

        btn_upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission();
            }
        });

    }

    public class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = (ImageView) bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            Bitmap myBitmap = null;
            MediaMetadataRetriever mMRetriever = null;
            try {
                mMRetriever = new MediaMetadataRetriever();
                mMRetriever.setDataSource(urls[0], new HashMap<String, String>());
                myBitmap = mMRetriever.getFrameAtTime();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (mMRetriever != null) {
                    mMRetriever.release();
                }
            }
            return myBitmap;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case 2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showMenuDialogForSelectVideo();
                }
            }
        }
    }

    private void callAddRequirementApi(String title, String description, String address, String qty, String receiveDate) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();

        String user_id = Function.getPrefData(Preferences.USER_ID, mContext);

        RequestBody req_requirementId = RequestBody.create(MediaType.parse("text/plain"), requirementId);
        RequestBody req_user_id = RequestBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody req_title = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody req_description = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody req_address = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody req_qty = RequestBody.create(MediaType.parse("text/plain"), qty);
        RequestBody req_receiveDate = RequestBody.create(MediaType.parse("text/plain"), receiveDate);
        RequestBody req_image = null, req_video = null;
        MultipartBody.Part multipartBodyImage = null,multipartBodyVideo = null;
        if (profileImageFile != null) {
            req_image = RequestBody.create(MediaType.parse("image/*"), profileImageFile);
            multipartBodyImage = MultipartBody.Part.createFormData("image", profileImageFile.getName(), req_image);
        } else {
            req_image = RequestBody.create(MediaType.parse("image/*"), "");
            multipartBodyImage = MultipartBody.Part.createFormData("image", "", req_image);
        }

        if (videoFile != null) {
            req_video = RequestBody.create(MediaType.parse("video/*"), videoFile);
            multipartBodyVideo = MultipartBody.Part.createFormData("video", videoFile.getName(), req_video);
        } else {
            req_video = RequestBody.create(MediaType.parse("video/*"), "");
            multipartBodyVideo = MultipartBody.Part.createFormData("video", "", req_video);
        }

        Call<JsonObject> addRequirementCallBack = api.addRequirement(req_requirementId, req_user_id, req_title, req_description, req_address, req_qty,
                req_receiveDate, multipartBodyImage, multipartBodyVideo);
        addRequirementCallBack.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {

                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (from.equalsIgnoreCase(Constants.UPDATE)) {
                            setResult(Constants.REQUIREMENT_UPDATE_CODE);

                            finish();
                        } else {
                            et_title.setText("");
                            et_description.setText("");
                            et_address.setText("");
                            et_qty.setText("");
                            tv_receive.setText("");

                            imagePath = "";
                            profileImageFile = null;
                            videoPath = "";
                            videoFile = null;

                            iv_image.setVisibility(View.GONE);
                            iv_video.setVisibility(View.GONE);

                            et_title.requestFocus();
                        }
//                        finish();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public String checkValidation(String title, String description, String address, String qty, String receive_date) {
        if (TextUtils.isEmpty(title)) {
            return getString(R.string.please_enter_title);
        } else if (TextUtils.isEmpty(description)) {
            return getString(R.string.please_enter_description);
        } else if (TextUtils.isEmpty(address)) {
            return getString(R.string.please_enter_address);
        } else if (TextUtils.isEmpty(qty)) {
            return getString(R.string.please_enter_quantity);
        } else if (TextUtils.isEmpty(receive_date)) {
            return getString(R.string.please_select_receive_date);
        } else {
            return "";
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT < 23) {
            showMenuDialogToSetProfilePic();
        } else {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showMenuDialogToSetProfilePic();
            } else {
                Utils.checkPermitionCameraGaller((AppCompatActivity) mContext);
            }
        }
    }

    private void showMenuDialogToSetProfilePic() {
        mCropParams = new CropParams(mContext);
        final TextView tvClickByCamera;
        final TextView tvPickFromGallery;
        final TextView tvCancel;

        dialogProfilePicture = new Dialog(mContext, R.style.picture_dialog_style);
        dialogProfilePicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProfilePicture.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogProfilePicture.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogProfilePicture.getWindow().setAttributes(wlmp);
        dialogProfilePicture.show();

        tvClickByCamera = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickFromGallery = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCancel);

        tvPickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
                openGallery();
            }
        });
        tvClickByCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
                openCamera();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProfilePicture.dismiss();
            }
        });
    }

    private void showMenuDialogForSelectVideo() {
        dialogVideo = new Dialog(mContext, R.style.picture_dialog_style);
        dialogVideo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogVideo.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogVideo.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView tvClickVideo;
        TextView tvPickImage;
        TextView tvCancel;

        dialogVideo.getWindow().setAttributes(wlmp);
        dialogVideo.show();

        tvClickVideo = dialogVideo.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickImage = dialogVideo.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogVideo.findViewById(R.id.dialog_choose_image_tvCancel);

        tvClickVideo.setText(getString(R.string.camera));
        tvPickImage.setText(getString(R.string.gallery));

        tvClickVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogVideo.dismiss();
                Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(videoCapture, REQUEST_VIDEO);
            }
        });
        tvPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogVideo.dismiss();
                Intent intent = new Intent();
                intent.setTypeAndNormalize("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_VIDEO);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogVideo.dismiss();
            }
        });
    }

    public void openGallery() {
        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;
        Intent intent = CropHelper.buildGalleryIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CROP);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropHelper.REQUEST_CROP) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == CropHelper.REQUEST_CAMERA) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == REQUEST_VIDEO) {
            final Uri selectedUri = data.getData();

            Log.d("uri", "onActivityResult: " + selectedUri);
            if (selectedUri != null) {
                videoPath = getFilePathFromURI(mContext, selectedUri);
                Log.d("video", "onActivityResult: " + videoPath);
                videoFile = new File(videoPath);
                Log.d("video", "onActivityResult: " + videoFile.length());

//                videoFile = new File(String.valueOf(selectedUri));
                Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                iv_video.setVisibility(View.VISIBLE);
                iv_video.setImageBitmap(bmThumbnail);
            } else {
                Toast.makeText(mContext, "Video Error", Toast.LENGTH_SHORT).show();
            }

        }

    }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path

        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        File copyFile = new File(wallpaperDirectory + File.separator + Calendar.getInstance()
                .getTimeInMillis() + ".mp4");
        // create folder if not exists

        copy(context, contentUri, copyFile);
        Log.d("vPath--->", copyFile.getAbsolutePath());

        return copyFile.getAbsolutePath();

    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    private void openCamera() {

        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = true;

        Intent intent = CropHelper.buildCameraIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CAMERA);

    }


    @Override
    public void onPhotoCropped(Uri uri) {

        if (!mCropParams.compress) {
            iv_image.setVisibility(View.VISIBLE);
            imagePath = uri.getPath();
            Log.d("onPhotoCropped", "imagePath==" + imagePath);
            iv_image.setImageBitmap(BitmapUtil.decodeUriAsBitmap(mContext, uri));
            //ivbgImage.setImageBitmap(BitmapUtil.decodeUriAsBitmap(homeActivity, uri));

            //updateProfilePic();
            profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
        }

    }

    @Override
    public void onCompressed(Uri uri) {
        imagePath = uri.getPath();
        Log.d("--- image path", "--" + imagePath);
        iv_image.setVisibility(View.VISIBLE);
        Picasso.get().load(uri).into(iv_image);
        profileImageFile = new File(Objects.requireNonNull(uri.getPath()));
        //fragmentProfileBinding.ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(homeActivity, uri));
        //ivbgImage.setImageBitmap(BitmapUtil.decodeUriAsBitmap(activity, uri));
//        updateProfilePic();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }


    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureVideoOutputUri().getPath();
        else return getPathFromURI(data.getData());
    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private Uri getCaptureVideoOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), timeStamp + "hir.mp4"));
        }
        return outputFileUri;
    }
}