package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.DesignsAdapter;
import com.example.hir_normal.adapter.GiftAdapter;
import com.example.hir_normal.apiCallBack.DesignListCallBack;
import com.example.hir_normal.apiCallBack.GiftListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.DesignData;
import com.example.hir_normal.model.GiftData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DesignsActivity extends AppCompatActivity {

    @BindView(R.id.rv_designs)
    RecyclerView rv_designs;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private DesignsAdapter designsAdapter;
    private ArrayList<DesignData> data;
    private String classId = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designs);
        mContext = DesignsActivity.this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);


        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        if (Function.isNetworkAvailable(mContext)) {
            getDesignData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
        tv_title.setText(getString(R.string.design_list));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    private void getDesignData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<DesignListCallBack> designListCallBackCall = api.getDesigns(classId);
        designListCallBackCall.enqueue(new Callback<DesignListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<DesignListCallBack> call, @NonNull Response<DesignListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<DesignData>) response.body().getData();
                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_designs.setVisibility(View.VISIBLE);

                        designsAdapter = new DesignsAdapter(mContext, data);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);

                        //rvGift.setLayoutManager(new GridLayoutManager(mContext, 2));
                        rv_designs.setLayoutManager(linearLayoutManager);
                        rv_designs.setAdapter(designsAdapter);
                    } else {
                        rv_designs.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DesignListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
