package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hir_normal.R;
import com.example.hir_normal.function.AESHelper;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoPlayerActivity extends YouTubeBaseActivity {

    private YouTubePlayerView youtube_player_view;
    private ImageView ivBack;
    private ImageView iv_share;
    private TextView tv_title;
    private TextView tv_video_title;
    private TextView tvViewCart;

    private YouTubePlayer.OnInitializedListener onInitializedListener;

    private Context mContext;
    private String url = "", videoTitle = "", from = "", description = "", pdf = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        mContext = VideoPlayerActivity.this;
        init();
    }

    public void init() {
        ivBack = findViewById(R.id.iv_back);
        iv_share = findViewById(R.id.iv_share);
        tv_title = findViewById(R.id.tv_title);
        youtube_player_view = findViewById(R.id.youtube_player_view);
        tv_video_title = findViewById(R.id.tv_video_title);
        tvViewCart = findViewById(R.id.tvViewCart);

        iv_share.setVisibility(View.VISIBLE);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        url = getIntent().getStringExtra(Constants.VIDEO_LINK);
        videoTitle = getIntent().getStringExtra(Constants.VIDEO_TITLE);
        from = getIntent().getStringExtra(Constants.FROM);

        if (from.equalsIgnoreCase(Constants.DESIGN)) {
            description = getIntent().getStringExtra(Constants.DESCRIPTION);
            pdf = getIntent().getStringExtra(Constants.PDF_URL);
        }
        String id = "";
        if (url != null && !url.equalsIgnoreCase("")) {
            id = url.split("v=")[1];
        }

        tv_title.setText(getString(R.string.video_player));
        if (videoTitle != null) {
            tv_video_title.setText(videoTitle);
        }
        String finalId = id;
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer.loadVideo(finalId);
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        youtube_player_view.initialize("AIzaSyA5wgo2_t1N4EwRIGvEDqwH2SDMF_Gig0c", onInitializedListener);
        encryption();
        decryption("DD2E0B87E3146490F133097C8145502689A5C6DEB5048C3E101A4A7CD4A97E08717EE2B2915C3F25DBE611B1173E5A3D");

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody1 = url;
                Intent sharingIntent1 = new Intent(Intent.ACTION_SEND);
                sharingIntent1.setType("text/plain");
                sharingIntent1.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.share));
                sharingIntent1.putExtra(Intent.EXTRA_TEXT, shareBody1);
                mContext.startActivity(Intent.createChooser(sharingIntent1, mContext.getResources().getString(R.string.share_using)));
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    public void encryption() {
        String encrypted = "";
        String sourceStr = getString(R.string.gcp_key);
        try {
            encrypted = AESHelper.encrypt(sourceStr);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decryption(String strEncryptedText) {
        String encrypted = strEncryptedText;
        String decrypted = "";
        try {
            decrypted = AESHelper.decrypt(encrypted);
            youtube_player_view.initialize(decrypted, onInitializedListener);
            Log.d("TEST", "decrypted:" + decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
