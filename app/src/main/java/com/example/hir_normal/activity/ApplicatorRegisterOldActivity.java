package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.CityListCallBack;
import com.example.hir_normal.apiCallBack.StateListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.City;
import com.example.hir_normal.model.States;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicatorRegisterOldActivity extends AppCompatActivity {
    @BindView(R.id.tv_birth_date)
    TextView tv_birth_date;
    @BindView(R.id.tv_marriage_anniversary_date)
    TextView tv_marriage_anniversary_date;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;
    @BindView(R.id.et_name_as_per_aadhar)
    EditText et_name_as_per_aadhar;
    @BindView(R.id.et_aadhar_number)
    EditText et_aadhar_number;
    @BindView(R.id.et_name_and_code_of_distributor)
    EditText et_name_and_code_of_distributor;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.sp_state)
    Spinner sp_state;
    @BindView(R.id.sp_city)
    Spinner sp_city;
    @BindView(R.id.sp_distributor)
    Spinner sp_distributor;
    @BindView(R.id.cb_terms_and_conditions)
    CheckBox cb_terms_and_conditions;
    @BindView(R.id.tv_terms_and_conditions)
    TextView tv_terms_and_conditions;

    private String selectedCityId = "";
    private String selectedStateId = "";
    private String selectedDistributorId = "";
    private String selectedCountryId = "101";
    ArrayList<City> cityArrayList;
    ArrayList<States> stateArrayList;
    ArrayList<States> distributorArrayList;
    String deviceToken = "";

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        mContext = ApplicatorRegisterOldActivity.this;
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();
    }

    private void init() {
        getDeviceToken();
        cityArrayList = new ArrayList<>();
        City city = new City();
        city.setName("Select City");
        cityArrayList.add(city);

        stateArrayList = new ArrayList<>();
        States states = new States();
        states.setName("Select State");
        stateArrayList.add(states);

        distributorArrayList = new ArrayList<>();
        States distributor = new States();
        distributor.setName("Select Distributor");
        distributorArrayList.add(distributor);

        ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_state.setAdapter(dataAdapter);

        ArrayAdapter<City> dataAdapterCity = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
        dataAdapterCity.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_city.setAdapter(dataAdapterCity);

        ArrayAdapter<States> dataAdapterDistributor = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, distributorArrayList);
        dataAdapterDistributor.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_distributor.setAdapter(dataAdapterDistributor);

        sp_distributor.setVisibility(View.GONE);

        if (Function.isNetworkAvailable(mContext)) {
            getStateApiCall();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

    }

    private void getStateApiCall() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<StateListCallBack> stateListCallBackCall = api.getStates(selectedCountryId);
        stateListCallBackCall.enqueue(new Callback<StateListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<StateListCallBack> call, @NonNull Response<StateListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {


                    Log.d("city_data", "onResponse: " + response.body().getData().size());

                   /* if (response.body().getData() != null && response.body().getData().size() > 0) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (response.body().getData().get(i).getId() == Integer.parseInt(selectedCountryId)) {
                                if (response.body().getData().get(i).getStates() != null &&
                                        response.body().getData().get(i).getStates().size() > 0) {
                                    Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().size());
                                    for (int j = 0; j < response.body().getData().get(i).getStates().size(); j++) {
                                        if (response.body().getData().get(i).getStates().get(j).getId() == Integer.parseInt(selectedStateId)) {
                                            if (response.body().getData().get(i).getStates().get(j).getCities() != null &&
                                                    response.body().getData().get(i).getStates().get(j).getCities().size() > 0) {
                                                Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().get(j).getCities().size());
                                                cityArrayList = (ArrayList<City>) response.body().getData().get(i).getStates().get(j).getCities();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        stateArrayList.addAll(response.body().getData());
                        distributorArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_state.setAdapter(dataAdapter);
                    sp_state.setSelection(0);

                    ArrayAdapter<States> dataAdapterDistributor = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, distributorArrayList);
                    dataAdapterDistributor.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_distributor.setAdapter(dataAdapterDistributor);
                    sp_distributor.setSelection(0);

                    sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedStateId = "";
                                cityArrayList = new ArrayList<>();
                                City city = new City();
                                city.setName("Select City");

                                cityArrayList.add(city);
                                ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                                sp_city.setAdapter(dataAdapter);
                                sp_city.setSelection(0);
                            } else {
                                selectedStateId = stateArrayList.get(i).getId().toString();
                                getCityApiCall(selectedStateId);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<StateListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCityApiCall(String stateId) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CityListCallBack> cityListCallBackCall = api.getCities(stateId);
        cityListCallBackCall.enqueue(new Callback<CityListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CityListCallBack> call, @NonNull Response<CityListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());

                   /* if (response.body().getData() != null && response.body().getData().size() > 0) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (response.body().getData().get(i).getId() == Integer.parseInt(selectedCountryId)) {
                                if (response.body().getData().get(i).getStates() != null &&
                                        response.body().getData().get(i).getStates().size() > 0) {
                                    Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().size());
                                    for (int j = 0; j < response.body().getData().get(i).getStates().size(); j++) {
                                        if (response.body().getData().get(i).getStates().get(j).getId() == Integer.parseInt(selectedStateId)) {
                                            if (response.body().getData().get(i).getStates().get(j).getCities() != null &&
                                                    response.body().getData().get(i).getStates().get(j).getCities().size() > 0) {
                                                Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().get(j).getCities().size());
                                                cityArrayList = (ArrayList<City>) response.body().getData().get(i).getStates().get(j).getCities();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }*/
//                    cityArrayList = (ArrayList<City>) response.body().getData();
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        cityArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_city.setAdapter(dataAdapter);
                    sp_city.setSelection(0);

                    sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedCityId = "";
                            } else {
                                selectedCityId = cityArrayList.get(i).getId().toString();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CityListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.tv_terms_and_conditions,R.id.tv_birth_date, R.id.tv_marriage_anniversary_date, R.id.tv_sign_up})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tv_terms_and_conditions:
                startActivity(new Intent(mContext, TermsAndConditionActivity.class));
                break;
            case R.id.tv_birth_date:
                Function.openDatePicker(ApplicatorRegisterOldActivity.this, tv_birth_date);
                break;
            case R.id.tv_marriage_anniversary_date:
                Function.openDatePicker(ApplicatorRegisterOldActivity.this, tv_marriage_anniversary_date);
                break;
            case R.id.tv_sign_up:

                String email = et_email.getText().toString();
                String password = et_password.getText().toString();
                String cPassword = et_confirm_password.getText().toString();
                String name = et_name_as_per_aadhar.getText().toString();
                String deviceToken = this.deviceToken;
//                String anniversaryDate = tv_marriage_anniversary_date.getText().toString();
//                String dateOfBirth = tv_birth_date.getText().toString();

                String birthDate = Function.updateDateFormatWithoutChange(tv_birth_date.getText().toString());
                String marriageDate = Function.updateDateFormatWithoutChange(tv_marriage_anniversary_date.getText().toString());
                String anniversaryDate = "";
                String dateOfBirth = "";

                if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
                    dateOfBirth = birthDate;
                }

                if (marriageDate != null && !marriageDate.equalsIgnoreCase("")) {
                    anniversaryDate = marriageDate;
                }

                String stateId = selectedStateId;
                String cityId = selectedCityId;
                String distributorId = "";
                if (sp_distributor.getSelectedItemPosition() > 0) {
                    distributorId = distributorArrayList.get(sp_distributor.getSelectedItemPosition()).getId().toString();
                }
                String distributorName = et_name_and_code_of_distributor.getText().toString();
                String mobileNo = et_mobile_number.getText().toString();
                String aadharNumber = et_aadhar_number.getText().toString();

                String checkValidation = checkValidation(email, password, cPassword, name, deviceToken, anniversaryDate, dateOfBirth,
                        stateId, cityId, distributorName, mobileNo, aadharNumber);
                if (checkValidation.equals("")) {
                    if (Function.isNetworkAvailable(mContext)) {
                        callApi(email, password, name, deviceToken, anniversaryDate, dateOfBirth,
                                stateId, cityId, distributorName, mobileNo, aadharNumber);
                    } else {
//                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        Function.showDialogue(mContext);
                    }
                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    private void callApi(String email, String password, String name, String deviceToken, String anniversary, String dateOfBirth,
                         String stateId, String cityId, String distributorName, String mobileNo, String aadharNumber) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(ApplicatorRegisterOldActivity.this, "", "please wait...");
        dialog.show();
        HashMap<String, RequestBody> data = new HashMap<>();

        RequestBody req_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody req_password = RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody req_name = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody req_device_token = RequestBody.create(MediaType.parse("text/plain"), deviceToken);
        RequestBody req_aniversary_date = RequestBody.create(MediaType.parse("text/plain"), anniversary);
        RequestBody req_distributor_name = RequestBody.create(MediaType.parse("text/plain"), distributorName);
        RequestBody req_state_id = RequestBody.create(MediaType.parse("text/plain"), stateId);
        RequestBody req_city_id = RequestBody.create(MediaType.parse("text/plain"), cityId);
        RequestBody req_role = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody req_dob = RequestBody.create(MediaType.parse("text/plain"), dateOfBirth);
        RequestBody req_mobile = RequestBody.create(MediaType.parse("text/plain"), mobileNo);
        RequestBody req_adhar_no = RequestBody.create(MediaType.parse("text/plain"), aadharNumber);
        RequestBody req_gst_no_text = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_pan_card_text = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_firm_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_concern_person = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_is_bank_details = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody req_bank_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_account_no = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_ifsc_code = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_branch = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_holder_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_gst_no = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyGst = MultipartBody.Part.createFormData("gst_no", "", req_gst_no);
        RequestBody req_pan_card = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyPanCard = MultipartBody.Part.createFormData("pan_card", "", req_pan_card);
        RequestBody req_image_1 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage1 = MultipartBody.Part.createFormData("image_1", "", req_image_1);
        RequestBody req_image_2 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage2 = MultipartBody.Part.createFormData("image_2", "", req_image_2);

        data.put("email", req_email);
        data.put("password", req_password);
        data.put("name", req_name);
        data.put("device_token", req_device_token);
        data.put("aniversary_date", req_aniversary_date);
        data.put("distributor_code", req_distributor_name);
        data.put("state_id", req_state_id);
        data.put("city_id", req_city_id);
        data.put("role", req_role);
        data.put("dob", req_dob);
        data.put("mobile", req_mobile);
        data.put("adhar_no", req_adhar_no);
        data.put("gst_no_text", req_gst_no_text);
        data.put("pan_card_text", req_pan_card_text);
        data.put("firm_name", req_firm_name);
        data.put("firm_type", req_firm_type);
        data.put("concern_person", req_concern_person);
        data.put("is_bank_details", req_is_bank_details);
        data.put("bank_name", req_bank_name);
        data.put("account_no", req_account_no);
        data.put("ifsc_code", req_ifsc_code);
        data.put("branch", req_branch);
        data.put("holder_name", req_holder_name);

        Call<JsonObject> jsonObjectCall = api.registerApplicator(data, multipartBodyGst, multipartBodyPanCard, multipartBodyImage1, multipartBodyImage2);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(ApplicatorRegisterOldActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ApplicatorRegisterOldActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ApplicatorRegisterOldActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String checkValidation(String email, String password, String cPassword, String name, String deviceToken, String anniversary, String dateOfBirth,
                                  String stateId, String cityId, String distributorName, String mobileNo, String aadharNumber) {
        if (TextUtils.isEmpty(name)) {
            return getString(R.string.enter_name);
        } else if (TextUtils.isEmpty(email)) {
            return getString(R.string.enter_email);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return getString(R.string.valid_email);
        } else if (TextUtils.isEmpty(mobileNo)) {
            return getString(R.string.enter_mobile);
        } else if (!Patterns.PHONE.matcher(mobileNo).matches()) {
            return getString(R.string.valid_mobile);
        }
        else if (mobileNo.length() != 10) {
            return getString(R.string.valid_mobile_exacate);

        }
        else if (TextUtils.isEmpty(anniversary)) {
            return getString(R.string.please_select_anniversary_date);
        } else if (TextUtils.isEmpty(dateOfBirth)) {
            return getString(R.string.please_select_date_of_birth);
        } else if (TextUtils.isEmpty(cityId)) {
            return getString(R.string.please_select_city);
        } else if (TextUtils.isEmpty(distributorName)) {
            return getString(R.string.enter_distributor_or_code);
        } else if (TextUtils.isEmpty(aadharNumber)) {
            return getString(R.string.enter_aadhar_number);
        } else if (TextUtils.isEmpty(password)) {
            return getString(R.string.enter_password);
        } else if (!password.equals(cPassword)) {
            return getString(R.string.password_not_match);
        } else if (!cb_terms_and_conditions.isChecked()) {
            return getString(R.string.please_agree_terms_and_conditions);
        } else {
            return "";
        }
    }

    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        deviceToken = task.getResult().getToken();
                        Log.d("deviceToken", "onComplete: " + deviceToken);

//                        Toast.makeText(mContext, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}