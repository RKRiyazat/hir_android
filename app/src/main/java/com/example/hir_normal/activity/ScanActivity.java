package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.fragments.CameraSelectorDialogFragment;
import com.example.hir_normal.fragments.FormatSelectorDialogFragment;
import com.example.hir_normal.fragments.MessageDialogFragment;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener,
        ZBarScannerView.ResultHandler,
        FormatSelectorDialogFragment.FormatSelectorDialogListener,
        CameraSelectorDialogFragment.CameraSelectorDialogListener {

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private ZBarScannerView mScannerView;
    private boolean mFlash;
    private TextView tvProductName, tvPointValue;
    private Button btnSubmit;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;
    private String productId = "", cityId = "", qrCodeId = "";
    private String hirCode = "";

    private Context mContext;
    JSONObject ob = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mFlash = savedInstanceState.getBoolean(FLASH_STATE, false);
            mAutoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = savedInstanceState.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = savedInstanceState.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }
        mContext = ScanActivity.this;
        setContentView(R.layout.activity_scan);
        init();


    }

    public void init() {
        tvProductName = findViewById(R.id.tvProductName);
        tvPointValue = findViewById(R.id.tvPointValue);
        btnSubmit = findViewById(R.id.btnSubmit);

        hirCode = "";
        qrCodeId = "";
        productId = "";
        cityId = "";
        tvProductName.setText("");
        tvPointValue.setText("");

        if (mScannerView != null) {
            mScannerView.stopCamera();
            mScannerView.setResultHandler(this);
            mScannerView.startCamera(mCameraId);
            mScannerView.setFlash(mFlash);
            mScannerView.setAutoFocus(mAutoFocus);
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hirCode.equalsIgnoreCase("")) {
                    if (!productId.equalsIgnoreCase("") &&
                            !qrCodeId.equalsIgnoreCase("") &&
                            !cityId.equalsIgnoreCase("")) {
                        callSubmitApi();
                    } else {
                        Toast.makeText(mContext, getString(R.string.please_scan_again), Toast.LENGTH_SHORT).show();
                        init();
                    }
                } else {
                    Toast.makeText(mContext, getString(R.string.you_are_not_scanning_hir_product), Toast.LENGTH_SHORT).show();
                    init();
                }
            }
        });
        setupToolbar();
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZBarScannerView(this);
        setupFormats();
        contentFrame.addView(mScannerView);
    }

    private void callSubmitApi() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(ScanActivity.this, "", "please wait...");
        dialog.show();
        Call<JsonObject> jsonObjectCall = api.callEarningApi(Function.getPrefData(Preferences.USER_ID, ScanActivity.this),
                productId, cityId, qrCodeId, hirCode, tvPointValue.getText().toString());

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                    Log.d("checkJson", jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem;

        if (mFlash) {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_off);
        }
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);


        if (mAutoFocus) {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_off);
        }
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        //menuItem = menu.add(Menu.NONE, R.id.menu_formats, 0, R.string.formats);
        //MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        //menuItem = menu.add(Menu.NONE, R.id.menu_camera_selector, 0, R.string.select_camera);
        //MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.menu_flash:
                mFlash = !mFlash;
                if (mFlash) {
                    item.setTitle(R.string.flash_on);
                } else {
                    item.setTitle(R.string.flash_off);
                }
                mScannerView.setFlash(mFlash);
                return true;

            case 16908332:
                startActivity(new Intent(mContext, ApplicatorDashBoardActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                return true;
            case R.id.menu_auto_focus:
                mAutoFocus = !mAutoFocus;
                if (mAutoFocus) {
                    item.setTitle(R.string.auto_focus_on);
                } else {
                    item.setTitle(R.string.auto_focus_off);
                }
                mScannerView.setAutoFocus(mAutoFocus);
                return true;
            case R.id.menu_formats:
                DialogFragment fragment = FormatSelectorDialogFragment.newInstance(this, mSelectedIndices);
                fragment.show(getSupportFragmentManager(), "format_selector");
                return true;
            case R.id.menu_camera_selector:
                mScannerView.stopCamera();
                DialogFragment cFragment = CameraSelectorDialogFragment.newInstance(this, mCameraId);
                cFragment.show(getSupportFragmentManager(), "camera_selector");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if (mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for (int i = 0; i < BarcodeFormat.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for (int index : mSelectedIndices) {
            formats.add(BarcodeFormat.ALL_FORMATS.get(index));
        }
        if (mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }


    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        mScannerView.resumeCameraPreview(this);

    }

    @Override
    public void handleResult(Result rawResult) {

        String data = rawResult.getContents();
        Log.d("scan", "handleResult: " + data);

        String[] dataScan = data.split(",");

        Log.d("scan", "handleResult: " + dataScan.length);
        if (dataScan != null && dataScan.length > 0) {
            for (int i = 0; i < dataScan.length; i++) {
                Log.d("scan", "handleResult: " + dataScan[i]);
                String[] keyValue = dataScan[i].split(":");
                if (keyValue != null && keyValue.length > 0) {
                    if (keyValue[0].trim().equalsIgnoreCase("product")) {
                        tvProductName.setText(keyValue[1].trim());
                    } else if (keyValue[0].trim().equalsIgnoreCase("points")) {
                        tvPointValue.setText(keyValue[1].trim());
                    } else if (keyValue[0].trim().equalsIgnoreCase("product_id")) {
                        productId = keyValue[1].trim();
                    } else if (keyValue[0].trim().equalsIgnoreCase("city_id")) {
                        cityId = keyValue[1].trim();
                    } else if (keyValue[0].trim().equalsIgnoreCase("id")) {
                        qrCodeId = keyValue[1].trim();
                    } else if (keyValue[0].trim().equalsIgnoreCase("hirCode")) {
                        hirCode = keyValue[1].trim();
                    }
                }
            }
        }

       /* if(dataScan.length==0)
        {
            Toast.makeText(ScanActivity.this,"Not Valid Qr Scan Again",Toast.LENGTH_LONG).show();
        }*/

           /* ob = new JSONObject("{" + data + "}");
            Log.d("scan", "handleResult: "+ob.toString());
            if (ob.has("product")) {
                tvProductName.setText(ob.getString("product"));
            } else {
                tvProductName.setText(R.string.no_product_name_found);
            }

            if (ob.has("points")) {
                tvPointValue.setText(String.valueOf(ob.getInt("points")));
            }*/




       /* Intent intent = new Intent();
        intent.putExtra("barcode", rawResult.getContents());
        setResult(1234, intent);
        finish();*/
      /*  if (from.equals("packing")) {
            setBarcode(rawResult.getContents(), position, type, PackingActivity.BARCODE_REQUEST);
//            startActivity(new Intent(FullScannerActivity.this, PackingActivity.class).putExtra("barcode", rawResult.getContents()));
        } else {
            setBarcode(rawResult.getContents(), position, type, UnpackingActivity.BARCODE_REQUEST);
//            startActivity(new Intent(FullScannerActivity.this, UnpackingActivity.class).putExtra("barcode", rawResult.getContents()));
        }*/
      /*  if (from.equals("packing")) {
            startActivity(new Intent(FullScannerActivity.this, PackingActivity.class).putExtra("barcode", rawResult.getContents()));
        } else {
            startActivity(new Intent(FullScannerActivity.this, UnpackingActivity.class).putExtra("barcode", rawResult.getContents()));
        }*/
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        PackingFrgment packingFrgment = new PackingFrgment();
//        Bundle bundle = new Bundle();
//        String myMessage = rawResult.getContents();
//        bundle.putString("barcode", myMessage);
//        packingFrgment.setArguments(bundle);
//        transaction.add(R.id.content_frame, packingFrgment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//        finish();
//        try {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
//            r.play();
//        } catch (Exception e) {
//        }
//        showMessageDialog("Contents = " + rawResult.getContents() + ", Format = " + rawResult.getBarcodeFormat().getName());

    }

    public void showMessageDialog(String message) {
        DialogFragment fragment = MessageDialogFragment.newInstance("Scan Results", message, this);
        fragment.show(getSupportFragmentManager(), "scan_results");
    }

    private void setBarcode(String contents, String position, String type, int barcodeRequest) {
        Intent intent = new Intent();
        intent.putExtra("barcode", contents);
        intent.putExtra("position", position);
        intent.putExtra("type", type);
        setResult(barcodeRequest, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
//        setBarcode("", "", "", 0);
        startActivity(new Intent(mContext, ApplicatorDashBoardActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void closeMessageDialog() {
        closeDialog("scan_results");
    }

    public void closeFormatsDialog() {
        closeDialog("format_selector");
    }

    public void closeDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    @Override
    public void onFormatsSaved(ArrayList<Integer> selectedIndices) {
        mSelectedIndices = selectedIndices;
        setupFormats();
    }

    @Override
    public void onCameraSelected(int cameraId) {
        mCameraId = cameraId;
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        closeMessageDialog();
        closeFormatsDialog();
    }

}
