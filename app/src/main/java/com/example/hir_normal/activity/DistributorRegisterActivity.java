package com.example.hir_normal.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.CityListCallBack;
import com.example.hir_normal.apiCallBack.StateListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.imagecrop.CropHandler;
import com.example.hir_normal.imagecrop.CropHelper;
import com.example.hir_normal.imagecrop.CropParams;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.City;
import com.example.hir_normal.model.States;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistributorRegisterActivity extends AppCompatActivity implements CropHandler {
    ArrayList<City> cityArrayList;
    ArrayList<States> stateArrayList;
    @BindView(R.id.sp_state)
    Spinner sp_state;
    @BindView(R.id.sp_city)
    Spinner sp_city;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.tv_birth_date)
    TextView tv_birth_date;
    @BindView(R.id.tv_marriage_anniversary_date)
    TextView tv_marriage_anniversary_date;
    @BindView(R.id.et_name_and_code_of_distributor)
    EditText et_name_and_code_of_distributor;
    @BindView(R.id.btnGst)
    TextView btnGst;
    @BindView(R.id.ivGSTImage)
    ImageView ivGSTImage;
    @BindView(R.id.et_gst_number)
    EditText et_gst_number;
    @BindView(R.id.et_pancard_number)
    EditText et_pancard_number;
    @BindView(R.id.btnPanCard)
    TextView btnPanCard;
    @BindView(R.id.ivPanCardImage)
    ImageView ivPanCardImage;
    @BindView(R.id.et_firm_name)
    EditText et_firm_name;
    @BindView(R.id.rbPartnership)
    RadioButton rbPartnership;
    @BindView(R.id.rbProperitory)
    RadioButton rbProperitory;
    @BindView(R.id.et_concern_person)
    EditText et_concern_person;
    @BindView(R.id.et_order_person)
    EditText et_order_person;
    @BindView(R.id.et_bank_name)
    EditText et_bank_name;
    @BindView(R.id.et_account_number)
    EditText et_account_number;
    @BindView(R.id.et_ifsc_code)
    EditText et_ifsc_code;
    @BindView(R.id.et_branch)
    EditText et_branch;
    @BindView(R.id.et_holder_name)
    EditText et_holder_name;
    @BindView(R.id.btnPassbook1)
    TextView btnPassbook1;
    @BindView(R.id.ivPassbookImage1)
    ImageView ivPassbookImage1;
    @BindView(R.id.btnPassbook2)
    TextView btnPassbook2;
    @BindView(R.id.ivPassbookImage2)
    ImageView ivPassbookImage2;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;
    @BindView(R.id.cb_terms_and_conditions)
    CheckBox cb_terms_and_conditions;
    @BindView(R.id.tv_terms_and_conditions)
    TextView tv_terms_and_conditions;
    String deviceToken = "";
    MultipartBody.Part gstImage = null;
    MultipartBody.Part panCardImage = null;
    MultipartBody.Part passbookImage1 = null;
    MultipartBody.Part passbookImage2 = null;
    File gstImageFile = null, panCardImageFile = null, passbookFile1 = null, passbookFile2 = null;
    private Context mContext;
    private String selectedCityId = "";
    private String selectedStateId = "";
    private String selectedCountryId = "101";
    private CropParams mCropParams;
    private TextView tvClickByCamera;
    private TextView tvPickFromGallery;
    private TextView tvCancel;
    private Dialog dialogProfilePicture;
    private String imagePath = "";
    private String selectedImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        mContext = DistributorRegisterActivity.this;
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();

    }

    private void init() {
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.img_bg));
        getDeviceToken();

        cityArrayList = new ArrayList<>();
        City city = new City();
        city.setName("Select City");
        cityArrayList.add(city);

        stateArrayList = new ArrayList<>();
        States states = new States();
        states.setName("Select State");
        stateArrayList.add(states);

        ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_state.setAdapter(dataAdapter);

        ArrayAdapter<City> dataAdapterCity = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
        dataAdapterCity.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
        sp_city.setAdapter(dataAdapterCity);

        if (Function.isNetworkAvailable(mContext)) {
            getStateApiCall();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }


   /* private void getStateApiCall() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CountryListCallBack> countryListCallBackCall = api.getCountry();
        countryListCallBackCall.enqueue(new Callback<CountryListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CountryListCallBack> call, @NonNull Response<CountryListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {

                    cityArrayList = new ArrayList<>();

                    Log.d("city_data", "onResponse: " + response.body().getData().size());

                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (response.body().getData().get(i).getId() == Integer.parseInt(selectedCountryId)) {
                                if (response.body().getData().get(i).getStates() != null &&
                                        response.body().getData().get(i).getStates().size() > 0) {
                                    Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().size());
                                    for (int j = 0; j < response.body().getData().get(i).getStates().size(); j++) {
                                        if (response.body().getData().get(i).getStates().get(j).getId() == Integer.parseInt(selectedStateId)) {
                                            if (response.body().getData().get(i).getStates().get(j).getCities() != null &&
                                                    response.body().getData().get(i).getStates().get(j).getCities().size() > 0) {
                                                Log.d("city_data", "onResponse: " + response.body().getData().get(i).getStates().get(j).getCities().size());
                                                cityArrayList = (ArrayList<City>) response.body().getData().get(i).getStates().get(j).getCities();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    City city = new City();
                    city.setName("Select City");

                    cityArrayList.add(city);

                    ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_state.setAdapter(dataAdapter);
                    sp_state.setSelection(cityArrayList.size() - 1);

                    sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == cityArrayList.size() - 1) {
                                selectedCityId = "";
                            } else {
                                selectedCityId = cityArrayList.get(i).getId().toString();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountryListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/

    private void getStateApiCall() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<StateListCallBack> stateListCallBackCall = api.getStates(selectedCountryId);
        stateListCallBackCall.enqueue(new Callback<StateListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<StateListCallBack> call, @NonNull Response<StateListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        stateArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<States> dataAdapter = new ArrayAdapter<States>(mContext, R.layout.spinner_item_with_text, stateArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_state.setAdapter(dataAdapter);
                    sp_state.setSelection(0);

                    sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedStateId = "";
                                cityArrayList = new ArrayList<>();
                                City city = new City();
                                city.setName("Select City");

                                cityArrayList.add(city);

                                ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                                sp_city.setAdapter(dataAdapter);
                                sp_city.setSelection(0);
                            } else {
                                selectedStateId = stateArrayList.get(i).getId().toString();
                                getCityApiCall(selectedStateId);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<StateListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCityApiCall(String stateId) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<CityListCallBack> cityListCallBackCall = api.getCities(stateId);
        cityListCallBackCall.enqueue(new Callback<CityListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<CityListCallBack> call, @NonNull Response<CityListCallBack> response) {
                dialog.dismiss();
                if (response.body().getCode().equals("200")) {
                    Log.d("city_data", "onResponse: " + response.body().getData().size());
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        cityArrayList.addAll(response.body().getData());
                    }

                    ArrayAdapter<City> dataAdapter = new ArrayAdapter<City>(mContext, R.layout.spinner_item_with_text, cityArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_drop_down_item_with_text);
                    sp_city.setAdapter(dataAdapter);
                    sp_city.setSelection(0);

                    sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                selectedCityId = "";
                            } else {
                                selectedCityId = cityArrayList.get(i).getId().toString();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CityListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.tv_terms_and_conditions, R.id.tv_birth_date, R.id.tv_marriage_anniversary_date, R.id.tv_sign_up, R.id.btnGst, R.id.btnPassbook1, R.id.btnPassbook2, R.id.btnPanCard})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tv_terms_and_conditions:
                startActivity(new Intent(mContext, TermsAndConditionActivity.class));
                break;
            case R.id.tv_birth_date:
                Function.openDatePicker(DistributorRegisterActivity.this, tv_birth_date);
                break;
            case R.id.tv_marriage_anniversary_date:
                Function.openDatePicker(DistributorRegisterActivity.this, tv_marriage_anniversary_date);
                break;
            case R.id.tv_sign_up:
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();
                String cPassword = et_confirm_password.getText().toString();
                String name = et_name.getText().toString();
                String deviceToken = this.deviceToken;
//                String anniversaryDate = tv_marriage_anniversary_date.getText().toString();
//                String dateOfBirth = tv_birth_date.getText().toString();

                String birthDate = Function.updateDateFormatWithoutChange(tv_birth_date.getText().toString());
                String marriageDate = Function.updateDateFormatWithoutChange(tv_marriage_anniversary_date.getText().toString());
                String anniversaryDate = "";
                String dateOfBirth = "";

                if (birthDate != null && !birthDate.equalsIgnoreCase("")) {
                    dateOfBirth = birthDate;
                }

                if (marriageDate != null && !marriageDate.equalsIgnoreCase("")) {
                    anniversaryDate = marriageDate;
                }

                String stateId = selectedStateId;
                String cityId = selectedCityId;
                String distributorName = et_name_and_code_of_distributor.getText().toString();
                String mobileNo = et_mobile_number.getText().toString();
                String gstNumber = et_gst_number.getText().toString();
                String panCardNumber = et_pancard_number.getText().toString();
                String firmName = et_firm_name.getText().toString();
                String concernPerson = et_concern_person.getText().toString();
                String orderPerson = et_order_person.getText().toString();
                String bankName = et_bank_name.getText().toString();
                String accountNumber = et_account_number.getText().toString();
                String ifscCode = et_ifsc_code.getText().toString();
                String branchName = et_branch.getText().toString();
                String holderName = et_holder_name.getText().toString();

                String checkValidation = checkValidation(email, password, cPassword, name, deviceToken, anniversaryDate, dateOfBirth,
                        stateId, cityId, mobileNo, gstNumber, gstImageFile, panCardNumber, panCardImageFile,
                        firmName, concernPerson, orderPerson, bankName, accountNumber, ifscCode, branchName, holderName);
                if (checkValidation.equals("")) {
                    if (Function.isNetworkAvailable(mContext)) {
                        callApi(email, password, name, deviceToken, anniversaryDate, dateOfBirth,
                                stateId, cityId, distributorName, mobileNo, gstNumber, panCardNumber,
                                firmName, concernPerson, orderPerson, bankName, accountNumber, ifscCode, branchName, holderName);
                    } else {
//                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        Function.showDialogue(mContext);
                    }


                } else {
                    Toast.makeText(mContext, checkValidation, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnGst:
                if (Build.VERSION.SDK_INT < 23) {
                    selectedImage = "gst";
                    showMenuDialogToSetProfilePic();
                } else {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectedImage = "gst";
                        showMenuDialogToSetProfilePic();
                    } else {
                        ActivityCompat.requestPermissions(DistributorRegisterActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
                    }
                }
                break;
            case R.id.btnPassbook1:
                if (Build.VERSION.SDK_INT < 23) {
                    selectedImage = "passbook1";
                    showMenuDialogToSetProfilePic();
                } else {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectedImage = "passbook1";
                        showMenuDialogToSetProfilePic();
                    } else {
                        ActivityCompat.requestPermissions(DistributorRegisterActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
                    }
                }
                break;
            case R.id.btnPassbook2:
                if (Build.VERSION.SDK_INT < 23) {
                    selectedImage = "passbook2";
                    showMenuDialogToSetProfilePic();
                } else {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectedImage = "passbook2";
                        showMenuDialogToSetProfilePic();
                    } else {
                        ActivityCompat.requestPermissions(DistributorRegisterActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
                    }
                }
                break;
            case R.id.btnPanCard:
                if (Build.VERSION.SDK_INT < 23) {
                    selectedImage = "pancard";
                    showMenuDialogToSetProfilePic();
                } else {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectedImage = "pancard";
                        showMenuDialogToSetProfilePic();
                    } else {
                        ActivityCompat.requestPermissions(DistributorRegisterActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
                    }
                }
                break;
        }
    }


    public void openGallery() {
        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = false;
        Intent intent = CropHelper.buildGalleryIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CROP);

    }


    private void openCamera() {

        mCropParams.refreshUri();
        mCropParams.enable = false;
        mCropParams.compress = false;

        Intent intent = CropHelper.buildCameraIntent(mCropParams, (Activity) mContext);
        startActivityForResult(intent, CropHelper.REQUEST_CAMERA);

    }


    private void callApi(String email, String password, String name, String deviceToken, String anniversary, String dateOfBirth,
                         String stateId, String cityId, String distributorName, String mobileNo,
                         String gstNo, String panCardNo, String firmName, String concernPerson,
                         String orderPerson, String bankName, String accountNumber, String ifscCode,
                         String branchName, String holderName) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(DistributorRegisterActivity.this, "", "please wait...");
        dialog.show();
        HashMap<String, RequestBody> data = new HashMap<>();

        RequestBody req_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody req_password = RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody req_name = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody req_device_token = RequestBody.create(MediaType.parse("text/plain"), deviceToken);
        RequestBody req_aniversary_date = RequestBody.create(MediaType.parse("text/plain"), anniversary);
        RequestBody req_distributor_name = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_state_id = RequestBody.create(MediaType.parse("text/plain"), stateId);
        RequestBody req_city_id = RequestBody.create(MediaType.parse("text/plain"), cityId);
        RequestBody req_role = RequestBody.create(MediaType.parse("text/plain"), "2");
        RequestBody req_dob = RequestBody.create(MediaType.parse("text/plain"), dateOfBirth);
        RequestBody req_mobile = RequestBody.create(MediaType.parse("text/plain"), mobileNo);
        RequestBody req_adhar_no = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody req_gst_no_text = RequestBody.create(MediaType.parse("text/plain"), gstNo);
        RequestBody req_pan_card_text = RequestBody.create(MediaType.parse("text/plain"), panCardNo);
        RequestBody req_firm_name = RequestBody.create(MediaType.parse("text/plain"), firmName);
        RequestBody req_firm_type;
        if (rbPartnership.isChecked()) {
            req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "1");
        } else {
            req_firm_type = RequestBody.create(MediaType.parse("text/plain"), "2");
        }
        RequestBody req_concern_person = RequestBody.create(MediaType.parse("text/plain"), concernPerson);
        RequestBody req_order_person = RequestBody.create(MediaType.parse("text/plain"), orderPerson);
        RequestBody req_is_bank_details = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody req_bank_name = RequestBody.create(MediaType.parse("text/plain"), bankName);
        RequestBody req_account_no = RequestBody.create(MediaType.parse("text/plain"), accountNumber);
        RequestBody req_ifsc_code = RequestBody.create(MediaType.parse("text/plain"), ifscCode);
        RequestBody req_branch = RequestBody.create(MediaType.parse("text/plain"), branchName);
        RequestBody req_holder_name = RequestBody.create(MediaType.parse("text/plain"), holderName);

        RequestBody reqImgFileGst = RequestBody.create(MediaType.parse("multipart/form-data"), gstImageFile);
        gstImage = MultipartBody.Part.createFormData("gst_no", gstImageFile.getName(), reqImgFileGst);
        RequestBody reqImgFilePanCard = RequestBody.create(MediaType.parse("multipart/form-data"), panCardImageFile);
        panCardImage = MultipartBody.Part.createFormData("pan_card", panCardImageFile.getName(), reqImgFilePanCard);

       /* if (gstImage == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            gstImage = MultipartBody.Part.createFormData("gst_no", "", reqimgfile);
        }
        if (panCardImage == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            panCardImage = MultipartBody.Part.createFormData("pan_card", "", reqimgfile);
        }
        */

        if (passbookImage1 == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            passbookImage1 = MultipartBody.Part.createFormData("image_1", "", reqimgfile);
        }
        if (passbookImage2 == null) {
            RequestBody reqimgfile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            passbookImage2 = MultipartBody.Part.createFormData("image_1", "", reqimgfile);
        }


        /*RequestBody req_gst_no = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyGst = MultipartBody.Part.createFormData("gst_no", "", req_gst_no);
        RequestBody req_pan_card = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyPanCard = MultipartBody.Part.createFormData("pan_card", "", req_pan_card);
        RequestBody req_image_1 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage1 = MultipartBody.Part.createFormData("image_1", "", req_image_1);
        RequestBody req_image_2 = RequestBody.create(MediaType.parse("image/*"), "");
        MultipartBody.Part multipartBodyImage2 = MultipartBody.Part.createFormData("image_2", "", req_image_2);*/

        data.put("email", req_email);
        data.put("password", req_password);
        data.put("name", req_name);
        data.put("device_token", req_device_token);
        data.put("aniversary_date", req_aniversary_date);
        data.put("distributor_code", req_distributor_name);
        data.put("state_id", req_state_id);
        data.put("city_id", req_city_id);
        data.put("role", req_role);
        data.put("dob", req_dob);
        data.put("mobile", req_mobile);
        data.put("adhar_no", req_adhar_no);
        data.put("gst_no_text", req_gst_no_text);
        data.put("pan_card_text", req_pan_card_text);
        data.put("firm_name", req_firm_name);
        data.put("firm_type", req_firm_type);
        data.put("concern_person", req_concern_person);
        data.put("order_person", req_order_person);
        data.put("is_bank_details", req_is_bank_details);
        data.put("bank_name", req_bank_name);
        data.put("account_no", req_account_no);
        data.put("ifsc_code", req_ifsc_code);
        data.put("branch", req_branch);
        data.put("holder_name", req_holder_name);

        Call<JsonObject> jsonObjectCall = api.registerApplicator(data, gstImage, panCardImage, passbookImage1, passbookImage2);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(DistributorRegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(DistributorRegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(DistributorRegisterActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }

    @Override
    public void onPhotoCropped(Uri uri) {

        if (!mCropParams.compress) {
//            ivProfile.setImageURI(uri);
            imagePath = uri.getPath();
            if (selectedImage.equals("gst")) {
                ivGSTImage.setVisibility(View.VISIBLE);
                ivGSTImage.setImageURI(uri);
                gstImageFile = new File(Objects.requireNonNull(uri.getPath()));
            } else if (selectedImage.equals("passbook1")) {
                ivPassbookImage1.setVisibility(View.VISIBLE);
                ivPassbookImage1.setImageURI(uri);
                passbookFile1 = new File(Objects.requireNonNull(uri.getPath()));
            } else if (selectedImage.equals("passbook2")) {
                ivPassbookImage2.setVisibility(View.VISIBLE);
                ivPassbookImage2.setImageURI(uri);
                passbookFile2 = new File(Objects.requireNonNull(uri.getPath()));
            } else {
                ivPanCardImage.setVisibility(View.VISIBLE);
                ivPanCardImage.setImageURI(uri);
                panCardImageFile = new File(Objects.requireNonNull(uri.getPath()));
            }
            Log.d("onPhotoCropped", "imagePath==" + imagePath);
            //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
            //                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                ivProfile.setImageBitmap(bitmap);

            /*Image image = new Image();
            image.setImage(imagePath);
            imagesAdapter.imageArrayList.add(image);
            imagesAdapter.notifyDataSetChanged();*/



           /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                    circularBitmapDrawable.setCircular(false);
                    iv_donate_item.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        }
    }

    @Override
    public void onCompressed(Uri uri) {
        imagePath = uri.getPath();
        //ivProfile.setImageBitmap(BitmapUtil.decodeUriAsBitmap(getActivity(), uri));
       /* Glide.with(mContext).asBitmap().load(uri).centerCrop().into(new BitmapImageViewTarget(iv_donate_item) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(false);
                iv_donate_item.setImageDrawable(circularBitmapDrawable);
            }
        });*/
    }


    @Override
    public void onCancel() {
        //Toast.makeText(this, "Crop canceled!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed(String message) {
        //Toast.makeText(this, "Crop failed: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropHelper.REQUEST_CROP) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        } else if (requestCode == CropHelper.REQUEST_CAMERA) {
            CropHelper.handleResult(this, requestCode, resultCode, data);
        }

    }

    private void showMenuDialogToSetProfilePic() {
        mCropParams = new CropParams(mContext);
        dialogProfilePicture = new Dialog(mContext, R.style.picture_dialog_style);
        dialogProfilePicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProfilePicture.setContentView(R.layout.dialog_choose_image);
        final WindowManager.LayoutParams wlmp = dialogProfilePicture.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogProfilePicture.getWindow().setAttributes(wlmp);
        dialogProfilePicture.show();

        tvClickByCamera = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCamera);
        tvPickFromGallery = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvGallery);
        tvCancel = dialogProfilePicture.findViewById(R.id.dialog_choose_image_tvCancel);

        tvPickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
                openGallery();
            }
        });
        tvClickByCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
                openCamera();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProfilePicture.dismiss();
            }
        });
    }

    public String checkValidation(String email, String password, String cPassword, String name, String deviceToken, String anniversary, String dateOfBirth,
                                  String stateId, String cityId, String mobileNo,
                                  String gstNo, File gstImageFile, String panCardNo, File panCardImageFile, String firmName, String concernPerson,
                                  String orderPerson, String bankName, String accountNumber, String ifscCode,
                                  String branchName, String holderName) {
        if (TextUtils.isEmpty(name)) {
            return getString(R.string.enter_name);
        } else if (TextUtils.isEmpty(email)) {
            return getString(R.string.enter_email);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return getString(R.string.valid_email);
        } else if (TextUtils.isEmpty(mobileNo)) {
            return getString(R.string.enter_mobile);
        } else if (!Patterns.PHONE.matcher(mobileNo).matches()) {
            return getString(R.string.valid_mobile);
        } else if (mobileNo.length() != 10) {
            return getString(R.string.valid_mobile_exacate);

        } else if (TextUtils.isEmpty(anniversary)) {
            return getString(R.string.please_select_anniversary_date);
        } else if (TextUtils.isEmpty(dateOfBirth)) {
            return getString(R.string.please_select_date_of_birth);
        } else if (TextUtils.isEmpty(cityId)) {
            return getString(R.string.please_select_city);
        } else if (TextUtils.isEmpty(gstNo)) {
            return getString(R.string.enter_gst_no);
        } else if (gstImageFile == null || gstImageFile.length() == 0) {
            return getString(R.string.please_select_gst_image);
        } else if (TextUtils.isEmpty(panCardNo)) {
            return getString(R.string.enter_pan_card_no);
        } else if (panCardImageFile == null || panCardImageFile.length() == 0) {
            return getString(R.string.please_select_pan_image);
        } else if (TextUtils.isEmpty(firmName)) {
            return getString(R.string.enter_firm_name);
        } else if (TextUtils.isEmpty(concernPerson)) {
            return getString(R.string.enter_concern_person);
        } else if (TextUtils.isEmpty(orderPerson)) {
            return getString(R.string.enter_order_person);
        } else if (TextUtils.isEmpty(bankName)) {
            return getString(R.string.enter_bank_name);
        } else if (TextUtils.isEmpty(accountNumber)) {
            return getString(R.string.enter_account_number);
        } else if (TextUtils.isEmpty(ifscCode)) {
            return getString(R.string.enter_ifsc_code);
        } else if (TextUtils.isEmpty(branchName)) {
            return getString(R.string.enter_branch_name);
        } else if (TextUtils.isEmpty(holderName)) {
            return getString(R.string.enter_holder_name);
        } else if (TextUtils.isEmpty(password)) {
            return getString(R.string.enter_password);
        } else if (!password.equals(cPassword)) {
            return getString(R.string.password_not_match);
        } else if (!cb_terms_and_conditions.isChecked()) {
            return getString(R.string.please_agree_terms_and_conditions);
        } else {
            return "";
        }
    }

    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        deviceToken = task.getResult().getToken();
                        Log.d("deviceToken", "onComplete: " + deviceToken);

//                        Toast.makeText(mContext, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
