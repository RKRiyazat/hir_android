package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.ProductListAdapter;
import com.example.hir_normal.apiCallBack.ProductListCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnProductItemClick;
import com.example.hir_normal.model.ProductData;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends AppCompatActivity implements OnProductItemClick {
    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;

    String categoryId = "";
    String userId = "";
    String classId = "";

    private Dialog qtydialog;

    private Context mContext;
    private ProductListAdapter productListAdapter;
    private ArrayList<ProductData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        mContext = ProductListActivity.this;
        ButterKnife.bind(this);
//        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tvViewCart.setVisibility(View.VISIBLE);
        tv_title.setText(R.string.provider_list);
        categoryId = getIntent().getStringExtra(Constants.CATEGORY_ID);
        userId = Function.getPrefData(Preferences.USER_ID, mContext);
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (Function.isNetworkAvailable(mContext)) {
            getProductData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

    }

    private void getProductData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Log.d("parameter", "getProductData: " + categoryId);
        Log.d("parameter", "getProductData: " + userId);

        Call<ProductListCallBack> productListCallBackCall = api.getProduct(categoryId, userId, classId);
        productListCallBackCall.enqueue(new Callback<ProductListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<ProductListCallBack> call, @NonNull Response<ProductListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<ProductData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvProduct.setVisibility(View.VISIBLE);

                        productListAdapter = new ProductListAdapter(mContext, data);
                        rvProduct.setLayoutManager(new LinearLayoutManager(mContext));
                        rvProduct.setAdapter(productListAdapter);
                    } else {
                        rvProduct.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick(R.id.tvViewCart)
    public void click() {
        startActivity(new Intent(mContext, CartListActivity.class));
    }

    @Override
    public void onProductItemClick(int pos, ProductData productData) {

        quentityUpdateDialog(productData, pos);

    }

    private void quentityUpdateDialog(ProductData productData, int pos) {


        qtydialog = new Dialog(ProductListActivity.this);
        qtydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtydialog.setContentView(R.layout.dialog_quenty_chamge);
        qtydialog.setCancelable(false);
        Objects.requireNonNull(qtydialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        qtydialog.getWindow().setGravity(Gravity.CENTER);
        ImageView ivProduct = qtydialog.findViewById(R.id.ivProduct);
        TextView tvProduuctName = qtydialog.findViewById(R.id.tvProduct);
        TextView tvok = qtydialog.findViewById(R.id.tv_ok);
        TextView tvCancle = qtydialog.findViewById(R.id.tv_cancel);
        EditText edtQty = qtydialog.findViewById(R.id.tv_qty_value);


        tvProduuctName.setText(productData.getTitle());

        edtQty.setText(productData.getCartQuantity() + "");

        if (productData.getImages() != null &&
                productData.getImages().size() > 0 &&
                productData.getImages().get(0).getImage() != null &&
                !productData.getImages().get(0).getImage().equalsIgnoreCase("")) {

            Picasso.get().load(Constants.IMAGE_BASE_URL + productData.getImages().get(0).getImage()).
                    placeholder(R.drawable.ic_image_placeholder).
                    error(R.drawable.ic_image_placeholder).
                    into(ivProduct);
        }


        tvok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = edtQty.getText().toString();

                if (Function.isNetworkAvailable(mContext)) {
                    callAddToCartApi(productData, Integer.parseInt(qty), Constants.UPDATE, pos);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qtydialog.dismiss();
            }
        });


        qtydialog.show();

    }


    private void callAddToCartApi(ProductData productData, int value, String type, int pos) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);
        Double price = Double.parseDouble(productData.getPrice()) * value;
//            Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();

        Log.d("parameter", "callAddToCartApi: " + Function.getPrefData(Preferences.USER_ID, mContext));
        Log.d("parameter", "callAddToCartApi: " + productData.getId());
        Log.d("parameter", "callAddToCartApi: " + price);
        Log.d("parameter", "callAddToCartApi: " + value);
        Call<JsonObject> jsonObjectCall = api.addToCart(Function.getPrefData(Preferences.USER_ID, mContext),
                String.valueOf(productData.getId()), String.valueOf(price), String.valueOf(value), "0");

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        data.get(pos).setCartQuantity(String.valueOf(value));

                        productListAdapter.notifyDataSetChanged();
                        qtydialog.dismiss();
                        //notifyItemChanged(position);
//                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}