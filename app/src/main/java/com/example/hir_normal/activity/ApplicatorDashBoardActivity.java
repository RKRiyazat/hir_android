package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.RequirementsAdapter;
import com.example.hir_normal.apiCallBack.RequirementsCallBack;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.RequirementsData;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicatorDashBoardActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_back;
    private TextView tv_title;
    private ImageView ivImage;
    private TextView tv_email;
    private TextView tv_user_name;
    private TextView tvViewCart;

    private ConstraintLayout cl_gift;
    private ConstraintLayout cl_video;
    private ConstraintLayout cl_point_history;
    private ConstraintLayout cl_scan;
    private ConstraintLayout cl_design;
    private ConstraintLayout cl_notification;

    private LinearLayout ll_profile;
    private LinearLayout ll_add_requirement;
    private LinearLayout ll_requirements;
    private LinearLayout ll_reward_history;
    private LinearLayout ll_catalog;
    private LinearLayout ll_hir_talk;
    private LinearLayout ll_scan;
    private LinearLayout ll_gift;
    private LinearLayout ll_point_history;
    private LinearLayout ll_change_password;
    private LinearLayout ll_logout;

    private NavigationView nav_view;
    private DrawerLayout drawer_layout;

    private final int REQUEST_PERMISSION_STORAGE_AND_CAMERA = 3;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicator_dash_board);
        mContext = ApplicatorDashBoardActivity.this;
        init();
    }

    private void init() {
        drawer_layout = findViewById(R.id.drawer_layout);
        nav_view = findViewById(R.id.nav_view);
        tv_title = findViewById(R.id.tv_title);
        iv_back = findViewById(R.id.iv_back);
        cl_gift = findViewById(R.id.cl_gift);
        cl_video = findViewById(R.id.cl_video);
        cl_point_history = findViewById(R.id.cl_point_history);
        cl_scan = findViewById(R.id.cl_scan);
        cl_design = findViewById(R.id.cl_design);
        cl_notification = findViewById(R.id.cl_notification);
        tvViewCart = findViewById(R.id.tvViewCart);
        View view = nav_view.getHeaderView(0);

        tv_email = view.findViewById(R.id.tv_email);
        tv_user_name = view.findViewById(R.id.tv_user_name);

        ll_profile = view.findViewById(R.id.ll_profile);
        ll_logout = view.findViewById(R.id.ll_logout);
        ll_change_password = view.findViewById(R.id.ll_change_password);
        ll_reward_history = view.findViewById(R.id.ll_reward_history);
        ll_catalog = view.findViewById(R.id.ll_catalog);
        ll_add_requirement = view.findViewById(R.id.ll_add_requirement);
        ll_requirements = view.findViewById(R.id.ll_requirements);
        ll_scan = view.findViewById(R.id.ll_scan);
        ll_gift = view.findViewById(R.id.ll_gift);
        ll_hir_talk = view.findViewById(R.id.ll_hir_talk);
        ll_point_history = view.findViewById(R.id.ll_point_history);

        ivImage = view.findViewById(R.id.ivImage);

//        view_scan.setVisibility(View.GONE);
//        view_gift.setVisibility(View.GONE);
//        view_point_history.setVisibility(View.GONE);
        ll_gift.setVisibility(View.GONE);
        ll_scan.setVisibility(View.GONE);
        ll_point_history.setVisibility(View.GONE);

        iv_back.setImageResource(R.drawable.ic_menu);
        tv_title.setText(getString(R.string.app_name));

        iv_back.setOnClickListener(this);
        cl_gift.setOnClickListener(this);
        cl_video.setOnClickListener(this);
        cl_point_history.setOnClickListener(this);
        cl_scan.setOnClickListener(this);
        cl_design.setOnClickListener(this);
        cl_notification.setOnClickListener(this);
//        tv_profile.setOnClickListener(this);
//        tv_logout.setOnClickListener(this);
//        tvChangePassword.setOnClickListener(this);
//        tv_status_of_gift.setOnClickListener(this);
//        tv_catalog.setOnClickListener(this);
//        tv_hir_talk.setOnClickListener(this);
        tvViewCart.setOnClickListener(this);
//        tv_gift.setOnClickListener(this);
//        tv_scan.setOnClickListener(this);
//        tv_point_history.setOnClickListener(this);

        ll_profile.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        ll_change_password.setOnClickListener(this);
        ll_reward_history.setOnClickListener(this);
        ll_catalog.setOnClickListener(this);
        ll_add_requirement.setOnClickListener(this);
        ll_requirements.setOnClickListener(this);
        ll_scan.setOnClickListener(this);
        ll_gift.setOnClickListener(this);
        ll_hir_talk.setOnClickListener(this);
        ll_point_history.setOnClickListener(this);


//        tv_email.setText(Function.getPrefData(Preferences.USER_EMAIL, mContext));

        ll_reward_history.setVisibility(View.VISIBLE);
//        view_status_of_gift.setVisibility(View.VISIBLE);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_email.setText(Function.getPrefData(Preferences.USER_EMAIL, mContext));
        tv_user_name.setText(Function.getPrefData(Preferences.USER_NAME, mContext));
        if (!Function.getPrefData(Preferences.USER_IMAGE, mContext).equalsIgnoreCase("")) {
            Picasso.get().load(Constants.IMAGE_BASE_URL + Function.getPrefData(Preferences.USER_IMAGE, mContext)).into(ivImage);
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("token", newToken);

                if (!newToken.equalsIgnoreCase(Function.getPrefData(Preferences.DEVICE_TOKEN, mContext))) {
                    updateToken(Function.getPrefData(Preferences.USER_ID, mContext), newToken);
                }
            }
        });
    }

    private void updateToken(String userId, String token) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<LoginResponseModel> requirementsCallBackCall = api.updateToken(userId, token);
        requirementsCallBackCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                        Function.setPrefData(Preferences.DEVICE_TOKEN, token, mContext);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_STORAGE_AND_CAMERA:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(mContext, ScanActivity.class));
                } else {
                    Toast.makeText(mContext, R.string.can_not_run_without_permission, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ll_reward_history:
                selectedBackground(Constants.MENU_REWARD_HISTORY);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, GiftOrderHistoryActivity.class));
                break;
            case R.id.ll_gift:
                selectedBackground(Constants.MENU_GIFT);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, GiftActivity.class));
                break;
            case R.id.ll_scan:
                selectedBackground(Constants.MENU_SCAN);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, ScanActivity.class));
                break;
            case R.id.ll_point_history:
                selectedBackground(Constants.MENU_POINT_HISTORY);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, PointHistoryActivity.class));
                break;
            case R.id.ll_catalog:
                selectedBackground(Constants.MENU_CATALOGUE);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, CatelogActivity.class));
                break;

            case R.id.ll_hir_talk:
                selectedBackground(Constants.MENU_TALK);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, HirTalkActivity.class));
                break;

            case R.id.ll_change_password:
                selectedBackground(Constants.MENU_PASSWORD);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, ChangePasswordActivity.class));
                break;

            case R.id.cl_gift:
                startActivity(new Intent(mContext, GiftActivity.class));
                break;
            case R.id.cl_video:
                startActivity(new Intent(mContext, VideoListActivity.class));
                break;
            case R.id.cl_point_history:
            case R.id.tvViewCart:
                startActivity(new Intent(mContext, PointHistoryActivity.class));
                break;
            case R.id.cl_notification:
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, NotificationActivity.class));
                break;
            case R.id.cl_scan:
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(mContext, ScanActivity.class));
                } else {
                    ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
                }
                break;
            case R.id.cl_design:
                startActivity(new Intent(mContext, CatelogActivity.class));
                break;
            case R.id.iv_back:
                drawer_layout.openDrawer(GravityCompat.START);
                break;
            case R.id.ll_profile:
                selectedBackground(Constants.MENU_PROFILE);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, ApplicatorProfileActivity.class));
                break;
            case R.id.ll_logout:
                selectedBackground(Constants.MENU_LOGOUT);
                Function.setPrefData(Preferences.USER_ID, "", mContext);
                Function.setPrefData(Preferences.USER_ROLE, "", mContext);
                Function.setPrefData(Preferences.USER_EMAIL, "", mContext);
                Function.setPrefData(Preferences.USER_IMAGE, "", mContext);
                Function.setBooleanPrefData(Preferences.IS_LOGIN, false, mContext);
                startActivity(new Intent(mContext, LoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
    }

    public void selectedBackground(String which) {
        ll_profile.setBackground(null);
        ll_catalog.setBackground(null);
        ll_hir_talk.setBackground(null);
        ll_change_password.setBackground(null);
        ll_logout.setBackground(null);
        ll_reward_history.setBackground(null);
        ll_gift.setBackground(null);
        ll_scan.setBackground(null);
        ll_point_history.setBackground(null);

        if (which.equalsIgnoreCase(Constants.MENU_PROFILE)) {
            ll_profile.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_CATALOGUE)) {
            ll_catalog.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_REWARD_HISTORY)) {
            ll_reward_history.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_GIFT)) {
            ll_gift.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_SCAN)) {
            ll_scan.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_TALK)) {
            ll_hir_talk.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_PASSWORD)) {
            ll_change_password.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_LOGOUT)) {
            ll_logout.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_POINT_HISTORY)) {
            ll_point_history.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        }
    }

}