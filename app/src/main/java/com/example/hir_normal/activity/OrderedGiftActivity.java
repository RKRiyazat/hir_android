package com.example.hir_normal.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hir_normal.R;
import com.example.hir_normal.adapter.OrderGiftAdapter;
import com.example.hir_normal.adapter.OrderProductAdapter;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.model.OrderGift;
import com.example.hir_normal.model.OrderHistory.OrderProduct;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderedGiftActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rv_ordered_gift)
    RecyclerView rv_ordered_gift;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String userId = "";
    private OrderGiftAdapter orderGiftAdapter;
    private ArrayList<OrderGift> orderGiftArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordered_gift);
        mContext = OrderedGiftActivity.this;
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tv_title.setText(R.string.str_ordered_gift);

        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        orderGiftArrayList = new ArrayList<>();
        orderGiftArrayList = (ArrayList<OrderGift>) getIntent().getSerializableExtra(Constants.ORDER_GIFT_LIST);

        if (orderGiftArrayList != null && orderGiftArrayList.size() > 0) {
            tv_not_available.setVisibility(View.GONE);
            rv_ordered_gift.setVisibility(View.VISIBLE);
            orderGiftAdapter = new OrderGiftAdapter(mContext, orderGiftArrayList);
            rv_ordered_gift.setLayoutManager(new LinearLayoutManager(mContext));
            rv_ordered_gift.setAdapter(orderGiftAdapter);
        } else {
            rv_ordered_gift.setVisibility(View.GONE);
            tv_not_available.setVisibility(View.VISIBLE);
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });

    }



    @OnClick({R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
