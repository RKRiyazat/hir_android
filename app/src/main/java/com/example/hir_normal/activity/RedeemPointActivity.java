package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.GiftListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.GiftData;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RedeemPointActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.spGiftList)
    Spinner spGiftList;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.et_points)
    EditText et_points;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private ArrayList<GiftData> giftDataArrayList;
    private ArrayList<String> spinnerArrayList;

    private String points, gift_id;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_point);
        mContext = RedeemPointActivity.this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        tv_title.setText(getString(R.string.redeem_point));
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_mobile_number.getText().toString().equals("")) {
                    Toast.makeText(mContext, "please enter mobile number", Toast.LENGTH_SHORT).show();
                } else if (et_points.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(mContext, "Points not available", Toast.LENGTH_SHORT).show();
                } else {
                    if (Function.isNetworkAvailable(mContext)) {
                        callRedeemApi();
                    } else {
//                        Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        Function.showDialogue(mContext);
                    }
                }

            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    private void callRedeemApi() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<JsonObject> jsonObjectCall = api.submitPointData(et_mobile_number.getText().toString(), gift_id, points);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    Log.d("checkJson", jsonObject.toString());
                    if (jsonObject.getString("status").equals("success")) {
                        finish();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getGiftData();
    }

    private void getGiftData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<GiftListCallBack> productListCallBackCall = api.getGift(Function.getPrefData(Preferences.USER_ID, mContext));
        productListCallBackCall.enqueue(new Callback<GiftListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<GiftListCallBack> call, @NonNull Response<GiftListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    giftDataArrayList = new ArrayList<>();
                    giftDataArrayList = (ArrayList<GiftData>) response.body().getData();
                    spinnerArrayList = new ArrayList<>();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        spinnerArrayList.add(response.body().getData().get(i).getName());
                    }


                    spGiftList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            points = giftDataArrayList.get(position).getPoints();
                            gift_id = String.valueOf(giftDataArrayList.get(position).getId());
                            et_points.setText(giftDataArrayList.get(position).getPoints());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
// Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(RedeemPointActivity.this, android.R.layout.simple_spinner_item, spinnerArrayList);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spGiftList.setAdapter(dataAdapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GiftListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick({R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
