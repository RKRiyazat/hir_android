package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivityOld extends AppCompatActivity {

    @BindView(R.id.et_mobile_or_aadhar)
    EditText et_mobile_or_aadhar;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.rbApplicator)
    RadioButton rbApplicator;
    @BindView(R.id.rbDistributor)
    RadioButton rbDistributor;
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.et_gst_or_aadhar)
    EditText et_gst_or_aadhar;

    String deviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {


        myRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbApplicator) {
                et_gst_or_aadhar.setHint(getResources().getString(R.string.aadhar_number));
            } else if (checkedId == R.id.rbDistributor) {
                et_gst_or_aadhar.setHint(getResources().getString(R.string.gst_number));
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("token", newToken);
                deviceToken = newToken;
                Function.setPrefData(Preferences.DEVICE_TOKEN, deviceToken, LoginActivityOld.this);
            }
        });
    }


    @OnClick({R.id.tv_login, R.id.tv_forgot_password, R.id.tv_sign_up})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                if (Function.checkBlankEditText(et_mobile_or_aadhar.getText().toString()) && Function.checkBlankEditText(et_gst_or_aadhar.getText().toString())) {
                    if (rbApplicator.isChecked()) {
                        Toast.makeText(this, getResources().getString(R.string.empty_mobile), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.empty_mobile_distributor), Toast.LENGTH_SHORT).show();
                    }


                }else if (et_mobile_or_aadhar.getText().toString().length() != 10) {
                    Toast.makeText(this, getResources().getString(R.string.valid_mobile_exacate), Toast.LENGTH_SHORT).show();
                } else if (Function.checkBlankEditText(et_password.getText().toString())) {
                    Toast.makeText(this, getResources().getString(R.string.empty_password), Toast.LENGTH_SHORT).show();
                } else {
                    if (Function.isNetworkAvailable(LoginActivityOld.this)) {
                        callLoginApi();
                    } else {
                        Toast.makeText(LoginActivityOld.this, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.tv_forgot_password:
                startActivity(new Intent(LoginActivityOld.this, ForgotPasswordActivity.class));
                break;
            case R.id.tv_sign_up:
                showDialogue();
                break;

        }
    }

    private void callLoginApi() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(LoginActivityOld.this, "", "please wait...");
        dialog.show();
        HashMap<String, String> data = new HashMap<>();

        String token = Function.getPrefData(Preferences.DEVICE_TOKEN, LoginActivityOld.this);

        data.put("mobile", et_mobile_or_aadhar.getText().toString());
        data.put("password", et_password.getText().toString());
        if (token != null && !token.isEmpty()) {
            data.put("device_token", token);
        }
        if (rbApplicator.isChecked()) {
            data.put("adhar_no", et_gst_or_aadhar.getText().toString());
            data.put("role", "1");
        } else if (rbDistributor.isChecked()) {
            data.put("gst_no", et_gst_or_aadhar.getText().toString());
            data.put("role", "2");
        }

        Call<LoginResponseModel> jsonObjectCall = api.loginApi(data);
        jsonObjectCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {
                dialog.dismiss();
                /*JSONObject jsonObject;*/
                try {
                    assert response.body() != null;
                    //jsonObject = new JSONObject(response.body().toString());


                    if (response != null && response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {


                            Function.setBooleanPrefData(Preferences.IS_LOGIN, true, LoginActivityOld.this);

                            Function.setPrefData(Preferences.USER_IMAGE, response.body().getLoginData().getAvatar(), LoginActivityOld.this);

                            Function.setPrefData(Preferences.USER_EMAIL, response.body().getLoginData().getEmail(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_ID, response.body().getLoginData().getId().toString(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_NAME, response.body().getLoginData().getName(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_MOBILE, response.body().getLoginData().getMobile(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_ADHAR, response.body().getLoginData().getAdharNo(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_DOB, response.body().getLoginData().getDob(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.USER_ANNIVERSERY, response.body().getLoginData().getAniversaryDate(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.STATE_ID, response.body().getLoginData().getStateId(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.CITY_ID, response.body().getLoginData().getCityId(), LoginActivityOld.this);
                            Function.setPrefData(Preferences.CLASS_ID, response.body().getLoginData().getClassId(), LoginActivityOld.this);

                            if (response.body().getLoginData().getRole().equalsIgnoreCase("1")) {

                                Function.setPrefData(Preferences.USER_ROLE, "1", LoginActivityOld.this);
                                //Function.setPrefData(Preferences.USER_ID, String.valueOf(jsonObject.getJSONObject("data").getInt("id")), LoginActivity.this);
                                startActivity(new Intent(LoginActivityOld.this, ApplicatorDashBoardActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                            } else {

                                Function.setPrefData(Preferences.USER_BANK_NAME, response.body().getLoginData().getBankDetails().getName(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_ACCOUNT_NUMBER, response.body().getLoginData().getBankDetails().getAccountNo(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_IFSC_CODE, response.body().getLoginData().getBankDetails().getIfscCode(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_BRANCH, response.body().getLoginData().getBankDetails().getBranch(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_HOLDER_NAME, response.body().getLoginData().getBankDetails().getHolderName(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_PASS_BOOK_1, response.body().getLoginData().getBankDetails().getImage1(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_BANK_PASS_BOOK_2, response.body().getLoginData().getBankDetails().getImage2(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.DISTRIBUTOR_CODE, response.body().getLoginData().getDistributorCode(), LoginActivityOld.this);


                                Function.setPrefData(Preferences.USER_FIRM_TYPE, response.body().getLoginData().getFirmType(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_FIRM_NAME, response.body().getLoginData().getFirmName(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_PAN_CARD_IMAGE, response.body().getLoginData().getPanCard(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_PAN_CARD_NUMBER, response.body().getLoginData().getPanCardText(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_GST_IMAGE, response.body().getLoginData().getGstNo(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_GST_NUMBER, response.body().getLoginData().getGstNoText(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.DISTRIBUTORE_NAME, response.body().getLoginData().getDistributorName(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_CONCERN_PERSON, response.body().getLoginData().getConcernPerson(), LoginActivityOld.this);
                                Function.setPrefData(Preferences.USER_ORDER_PERSON, response.body().getLoginData().getOrderPerson(), LoginActivityOld.this);

                                Function.setPrefData(Preferences.USER_ROLE, "2", LoginActivityOld.this);
                                startActivity(new Intent(LoginActivityOld.this, DistributorDashBoardActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            }

                        } else {
                            Toast.makeText(LoginActivityOld.this, response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    }

                    /*if (jsonObject.getString("status").equals("success")) {
                        if (jsonObject.getJSONObject("data").getString("role").equals("1")) {
                            Function.setBooleanPrefData(Preferences.IS_LOGIN, true, LoginActivity.this);
                            Function.setPrefData(Preferences.USER_ROLE, "1", LoginActivity.this);
                            Function.setPrefData(Preferences.USER_IMAGE, "", LoginActivity.this);
                            Function.setPrefData(Preferences.USER_EMAIL, jsonObject.getJSONObject("data").getString("email"), LoginActivity.this);
                            Function.setPrefData(Preferences.USER_ID, String.valueOf(jsonObject.getJSONObject("data").getInt("id")), LoginActivity.this);
                            startActivity(new Intent(LoginActivity.this, ApplicatorDashBoardActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        } else {
                            Function.setBooleanPrefData(Preferences.IS_LOGIN, true, LoginActivity.this);
                            Function.setPrefData(Preferences.USER_ROLE, "2", LoginActivity.this);
                            Function.setPrefData(Preferences.USER_IMAGE, "", LoginActivity.this);
                            Function.setPrefData(Preferences.USER_EMAIL, jsonObject.getJSONObject("data").getString("email"), LoginActivity.this);
                            Function.setPrefData(Preferences.USER_ID, String.valueOf(jsonObject.getJSONObject("data").getInt("id")), LoginActivity.this);

                        }
                    } */
                    else {
                        Toast.makeText(LoginActivityOld.this, response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivityOld.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(LoginActivityOld.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialogue() {
        Dialog dialog = new Dialog(LoginActivityOld.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_select_user);
        Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        TextView tv_provider = dialog.findViewById(R.id.tv_provider);
        TextView tv_distributor = dialog.findViewById(R.id.tv_distributor);

        tv_provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(LoginActivityOld.this, ApplicatorRegisterActivity.class));
            }
        });

        tv_distributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(LoginActivityOld.this, DistributorRegisterActivity.class));
            }
        });
        dialog.show();
    }
}
