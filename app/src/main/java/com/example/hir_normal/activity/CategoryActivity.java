package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.CategoryAdapter;
import com.example.hir_normal.apiCallBack.ProductListCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.ProductData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity {
    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private CategoryAdapter categoryAdapter;
    private ArrayList<ProductData> data;
    private String classId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        mContext = CategoryActivity.this;
        ButterKnife.bind(this);
//        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);
        if (Function.isNetworkAvailable(mContext)) {
            getCategoryData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
        tv_title.setText(getString(R.string.category_list));
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getCategoryData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(CategoryActivity.this, "", "please wait..");
        dialog.show();
        Call<ProductListCallBack> productListCallBackCall = api.getCategory(classId);
        productListCallBackCall.enqueue(new Callback<ProductListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<ProductListCallBack> call, @NonNull Response<ProductListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<ProductData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rvCategory.setVisibility(View.VISIBLE);

                        categoryAdapter = new CategoryAdapter(CategoryActivity.this, data);
                        rvCategory.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
                        rvCategory.setAdapter(categoryAdapter);
                    } else {
                        rvCategory.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(CategoryActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


}
