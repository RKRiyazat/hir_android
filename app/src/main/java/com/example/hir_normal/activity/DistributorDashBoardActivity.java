package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.apiCallBack.loginResponse.LoginResponseModel;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistributorDashBoardActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_back;
    private TextView tv_title;

    private TextView tv_email;
    private TextView tv_user_name;
    private ImageView ivImage;

    private LinearLayout ll_profile;
    private LinearLayout ll_add_requirement;
    private LinearLayout ll_requirements;
    private LinearLayout ll_catalog;
    private LinearLayout ll_hir_talk;
    private LinearLayout ll_change_password;
    private LinearLayout ll_logout;

    private ConstraintLayout cl_purchase;
    private ConstraintLayout cl_video;
    private ConstraintLayout cl_account_ledgers;
    private ConstraintLayout clStatusOfMaterial;
    private ConstraintLayout cl_events;
    private ConstraintLayout cl_contact_us;
    private ConstraintLayout cl_offers;
    private ConstraintLayout cl_notification;
    private NavigationView nav_view;
    private DrawerLayout drawer_layout;


    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_dash_board);
        mContext = DistributorDashBoardActivity.this;
        init();
    }

    private void init() {
        drawer_layout = findViewById(R.id.drawer_layout);
        nav_view = findViewById(R.id.nav_view);
        tv_title = findViewById(R.id.tv_title);
        iv_back = findViewById(R.id.iv_back);
        cl_purchase = findViewById(R.id.cl_purchase);
        cl_video = findViewById(R.id.cl_video);
        cl_account_ledgers = findViewById(R.id.cl_account_ledgers);
        clStatusOfMaterial = findViewById(R.id.cl_status_of_material);
        cl_events = findViewById(R.id.cl_events);
        cl_contact_us = findViewById(R.id.cl_contact_us);
        cl_offers = findViewById(R.id.cl_offers);
        cl_notification = findViewById(R.id.cl_notification);
        View view = nav_view.getHeaderView(0);

        tv_email = view.findViewById(R.id.tv_email);
        tv_user_name = view.findViewById(R.id.tv_user_name);
        ivImage = view.findViewById(R.id.ivImage);

        ll_profile = view.findViewById(R.id.ll_profile);
        ll_logout = view.findViewById(R.id.ll_logout);
        ll_change_password = view.findViewById(R.id.ll_change_password);
        ll_catalog = view.findViewById(R.id.ll_catalog);
        ll_add_requirement = view.findViewById(R.id.ll_add_requirement);
        ll_requirements = view.findViewById(R.id.ll_requirements);
        ll_hir_talk = view.findViewById(R.id.ll_hir_talk);

        ll_add_requirement.setVisibility(View.VISIBLE);
        ll_requirements.setVisibility(View.VISIBLE);

        iv_back.setImageResource(R.drawable.ic_menu);
        tv_title.setText(getString(R.string.app_name));

        iv_back.setOnClickListener(this);
        cl_purchase.setOnClickListener(this);
        cl_video.setOnClickListener(this);
        cl_account_ledgers.setOnClickListener(this);
        cl_events.setOnClickListener(this);
        cl_contact_us.setOnClickListener(this);
        cl_offers.setOnClickListener(this);
        clStatusOfMaterial.setOnClickListener(this);
        cl_notification.setOnClickListener(this);

        ll_profile.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        ll_change_password.setOnClickListener(this);
        ll_catalog.setOnClickListener(this);
        ll_add_requirement.setOnClickListener(this);
        ll_requirements.setOnClickListener(this);
        ll_hir_talk.setOnClickListener(this);


    }

    private void updateToken(String userId, String token) {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<LoginResponseModel> requirementsCallBackCall = api.updateToken(userId, token);
        requirementsCallBackCall.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                        Function.setPrefData(Preferences.DEVICE_TOKEN, token, mContext);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseModel> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_email.setText(Function.getPrefData(Preferences.USER_EMAIL, mContext));
        tv_user_name.setText(Function.getPrefData(Preferences.USER_NAME, mContext));
        if (!Function.getPrefData(Preferences.USER_IMAGE, mContext).equalsIgnoreCase("")) {
            Picasso.get().load(Constants.IMAGE_BASE_URL + Function.getPrefData(Preferences.USER_IMAGE, mContext)).into(ivImage);
        }
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("token", newToken);

                if (!newToken.equalsIgnoreCase(Function.getPrefData(Preferences.DEVICE_TOKEN, mContext))) {
                    updateToken(Function.getPrefData(Preferences.USER_ID, mContext), newToken);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ll_add_requirement:
                selectedBackground(Constants.MENU_ADD_REQUIREMENT);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, AddRequirementActivity.class)
                        .putExtra(Constants.FROM, Constants.ADD));
                break;

            case R.id.ll_requirements:
                selectedBackground(Constants.MENU_REQUIREMENTS);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, RequirementListActivity.class));
                break;

            case R.id.ll_reward_history:
                selectedBackground(Constants.MENU_REWARD_HISTORY);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, GiftOrderHistoryActivity.class));
                break;
            case R.id.ll_catalog:
                selectedBackground(Constants.MENU_CATALOGUE);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, CatelogActivity.class));
                break;

            case R.id.ll_hir_talk:
                selectedBackground(Constants.MENU_TALK);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, HirTalkActivity.class));
                break;

            case R.id.ll_change_password:
                selectedBackground(Constants.MENU_PASSWORD);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, ChangePasswordActivity.class));
                break;

            case R.id.cl_purchase:
                startActivity(new Intent(mContext, CategoryActivity.class));
                break;
            case R.id.cl_video:
                startActivity(new Intent(mContext, VideoListActivity.class));
                break;
            case R.id.cl_account_ledgers:
                startActivity(new Intent(mContext, OrderHistoryActivity.class));
                break;

            case R.id.cl_status_of_material:
                startActivity(new Intent(mContext, StatusMaterialActivity.class));
                break;
            case R.id.cl_notification:
                startActivity(new Intent(mContext, NotificationActivity.class));
                break;
            case R.id.cl_events:
                startActivity(new Intent(mContext, EventActivity.class));
                break;
            case R.id.cl_contact_us:
                startActivity(new Intent(mContext, ContactActivity.class));
                break;
            case R.id.cl_offers:
                startActivity(new Intent(mContext, OfferActivity.class));
                break;
            case R.id.iv_back:
                drawer_layout.openDrawer(GravityCompat.START);
                break;
            case R.id.ll_profile:
                selectedBackground(Constants.MENU_PROFILE);
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
                startActivity(new Intent(mContext, DistributorProfileActivity.class));
                break;
            case R.id.ll_logout:
                selectedBackground(Constants.MENU_LOGOUT);
                Function.setPrefData(Preferences.USER_ID, "", mContext);
                Function.setPrefData(Preferences.USER_ROLE, "", mContext);
                Function.setPrefData(Preferences.USER_EMAIL, "", mContext);
                Function.setPrefData(Preferences.USER_IMAGE, "", mContext);
                Function.setBooleanPrefData(Preferences.IS_LOGIN, false, mContext);
                startActivity(new Intent(mContext, LoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
    }

    public void selectedBackground(String which) {
        ll_profile.setBackground(null);
        ll_add_requirement.setBackground(null);
        ll_requirements.setBackground(null);
        ll_catalog.setBackground(null);
        ll_hir_talk.setBackground(null);
        ll_change_password.setBackground(null);
        ll_logout.setBackground(null);

        if (which.equalsIgnoreCase(Constants.MENU_PROFILE)) {
            ll_profile.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_CATALOGUE)) {
            ll_catalog.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_ADD_REQUIREMENT)) {
            ll_add_requirement.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_REQUIREMENTS)) {
            ll_requirements.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_TALK)) {
            ll_hir_talk.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_PASSWORD)) {
            ll_change_password.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        } else if (which.equalsIgnoreCase(Constants.MENU_LOGOUT)) {
            ll_logout.setBackground(getResources().getDrawable(R.drawable.selected_menu_bg));
        }
    }
}
