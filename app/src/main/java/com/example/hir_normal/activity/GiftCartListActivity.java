package com.example.hir_normal.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.GiftCartListAdapter;
import com.example.hir_normal.apiCallBack.GiftCartListCallBack;
import com.example.hir_normal.apiCallBack.UserTotalPointCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnGiftCartItemClick;
import com.example.hir_normal.model.GiftCartData;
import com.example.hir_normal.model.UserTotalPointsData;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class GiftCartListActivity extends AppCompatActivity implements OnGiftCartItemClick {

    @BindView(R.id.rvGiftCart)
    RecyclerView rvGiftCart;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tvTotalValue)
    TextView tvTotal;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.btnPay)
    Button btnPay;

    String userID = "";

    private Context mContext;
    private GiftCartListAdapter giftCartListAdapter;
    private ArrayList<GiftCartData> data;
    private UserTotalPointsData userTotalPointsData;

    private Dialog qtyDialog;
    private double totalEarning = 0.0;
    private double totalRedeem = 0.0;
    private double totalPoints = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_cart_list);
        mContext = GiftCartListActivity.this;
        ButterKnife.bind(this);
        init();
    }


    private void init() {
        tv_title.setText(R.string.cart_list);
        userID = Function.getPrefData(Preferences.USER_ID, mContext);

        iv_back.setOnClickListener(view -> finish());

        if (Function.isNetworkAvailable(mContext)) {
            getCartData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }

    private void getCartData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<GiftCartListCallBack> giftCartListCallBackCall = api.getGiftCartList(userID);
        giftCartListCallBackCall.enqueue(new Callback<GiftCartListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<GiftCartListCallBack> call, @NonNull Response<GiftCartListCallBack> response) {
                dialog.dismiss();

                try {

                    if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                        data = new ArrayList<>();
                        data = (ArrayList<GiftCartData>) response.body().getData();
                        if (data != null && response.body().getData().size() > 0) {
                            tv_not_available.setVisibility(View.GONE);
                            rvGiftCart.setVisibility(View.VISIBLE);
                            btnPay.setEnabled(true);
                            Double price = 0.0;
                            if (data != null && data.size() > 0) {
                                for (int i = 0; i < data.size(); i++) {
                                    price = price + (Double.parseDouble(data.get(i).getPrice()) * Double.parseDouble(data.get(i).getQuantity()));
                                }
                            }

                            tvTotal.setText(String.valueOf(price));

                            giftCartListAdapter = new GiftCartListAdapter(GiftCartListActivity.this, data, tvTotal);
                            rvGiftCart.setLayoutManager(new LinearLayoutManager(GiftCartListActivity.this));
                            rvGiftCart.setAdapter(giftCartListAdapter);
                            getUserTotalPoints();
                        } else {
//                            Toast.makeText(mContext, R.string.no_cart_item_available, Toast.LENGTH_SHORT).show();
                            tv_not_available.setVisibility(View.VISIBLE);
                            rvGiftCart.setVisibility(View.GONE);
                            btnPay.setEnabled(false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<GiftCartListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getUserTotalPoints() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<UserTotalPointCallBack> getUserTotalPointsCallBack = api.getUserTotalPoints(userID);
        getUserTotalPointsCallBack.enqueue(new Callback<UserTotalPointCallBack>() {
            @Override
            public void onResponse(@NonNull Call<UserTotalPointCallBack> call, @NonNull Response<UserTotalPointCallBack> response) {
                dialog.dismiss();

                try {

                    if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                        userTotalPointsData = new UserTotalPointsData();
                        userTotalPointsData = response.body().getData();
                        if (userTotalPointsData != null) {
                            totalEarning = userTotalPointsData.getTotalEarning();
                            totalRedeem = userTotalPointsData.getTotalRedeemed();
                            if (!userTotalPointsData.getTotalPoints().isEmpty()) {
                                totalPoints = Double.parseDouble(userTotalPointsData.getTotalPoints());
                            }
                        }
                    } else {
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserTotalPointCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick(R.id.btnPay)
    public void click() {
        if (Function.isNetworkAvailable(mContext)) {
            if (!tvTotal.getText().toString().isEmpty()) {
                if (totalEarning >= Double.parseDouble(tvTotal.getText().toString())) {
                    callAddOrder();
                } else {
                    Toast.makeText(mContext, "You have not enough earning.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, "Cart is Empty", Toast.LENGTH_SHORT).show();
            }

        } else {
//                Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }

    private void callAddOrder() {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();

        String total = "";
        String userId = "";

        userId = Function.getPrefData(Preferences.USER_ID, mContext);

        total = tvTotal.getText().toString();

        Log.d("addOrder", "callAddOrder: " + userId);
        Log.d("addOrder", "callAddOrder: " + total);

        API api = MyApp.retrofit.create(API.class);
        Call<JsonObject> jsonObjectCall = api.addGiftCartOrder(userId, total);
        String finalTotal = total;
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getString("code").equalsIgnoreCase("200")) {
//                        finish();
                        if (!jsonObject.isNull("data")) {
                            callApiTotalPoints(finalTotal, String.valueOf(jsonObject.getJSONObject("data").getInt("id")));
                        }
                    }
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callApiTotalPoints(String totalPoint, String orderId) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();

        String mobileNumber = Function.getPrefData(Preferences.USER_MOBILE, mContext);
        Log.d("addOrder", "callAddOrder: " + mobileNumber);
        Log.d("addOrder", "callAddOrder: " + totalPoint);
        Log.d("addOrder", "callAddOrder: " + orderId);

        API api = MyApp.retrofit.create(API.class);
        Call<JsonObject> jsonObjectCall = api.submitGiftPointData(mobileNumber, "", totalPoint, orderId);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                        finish();
                    }
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onGiftCartItemClick(int pos, GiftCartData giftCartData) {

        quentityUpdateDialog(giftCartData, pos);
    }

    private void quentityUpdateDialog(GiftCartData giftCartData, int pos) {


        qtyDialog = new Dialog(GiftCartListActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.dialog_quenty_chamge);
        qtyDialog.setCancelable(false);
        Objects.requireNonNull(qtyDialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        qtyDialog.getWindow().setGravity(Gravity.CENTER);
        ImageView ivProduct = qtyDialog.findViewById(R.id.ivProduct);
        TextView tvProduuctName = qtyDialog.findViewById(R.id.tvProduct);
        TextView tvok = qtyDialog.findViewById(R.id.tv_ok);
        TextView tvCancle = qtyDialog.findViewById(R.id.tv_cancel);
        EditText edtQty = qtyDialog.findViewById(R.id.tv_qty_value);


        tvProduuctName.setText(giftCartData.getTitle());

        edtQty.setText(giftCartData.getQuantity() + "");

        if (giftCartData.getImage() != null &&
                !giftCartData.getImage().equalsIgnoreCase("")) {

            Picasso.get().load(Constants.IMAGE_BASE_URL + giftCartData.getImage()).
                    placeholder(R.drawable.ic_image_placeholder).
                    error(R.drawable.ic_image_placeholder).
                    into(ivProduct);
        }


        tvok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = edtQty.getText().toString();

                if (Function.isNetworkAvailable(mContext)) {
                    callAddToCartApi(giftCartData, Integer.parseInt(qty), Constants.UPDATE, pos);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qtyDialog.dismiss();
            }
        });


        qtyDialog.show();

    }

    private void callAddToCartApi(GiftCartData giftCartData, int value, String type, int pos) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);
        Double price = Double.parseDouble(giftCartData.getPrice()) * value;
//            Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();

        Log.d("parameter", "callAddToCartApi: " + Function.getPrefData(Preferences.USER_ID, mContext));
        Log.d("parameter", "callAddToCartApi: " + giftCartData.getGiftId());
        Log.d("parameter", "callAddToCartApi: " + price);
        Log.d("parameter", "callAddToCartApi: " + value);
        Call<JsonObject> jsonObjectCall = api.addGiftToCart(Function.getPrefData(Preferences.USER_ID, mContext),
                String.valueOf(giftCartData.getGiftId()), String.valueOf(price), String.valueOf(value), "0");

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        giftCartListAdapter.data.get(pos).setQuantity(String.valueOf(value));

                        Double price = 0.0;
                        if (giftCartListAdapter.data != null && giftCartListAdapter.data.size() > 0) {
                            for (int i = 0; i < giftCartListAdapter.data.size(); i++) {
                                price = price + (Double.parseDouble(giftCartListAdapter.data.get(i).getPrice()) * Double.parseDouble(giftCartListAdapter.data.get(i).getQuantity()));
                            }
                        }

                        tvTotal.setText(String.valueOf(price));

                        giftCartListAdapter.notifyDataSetChanged();
                        qtyDialog.dismiss();
                        //notifyItemChanged(position);
//                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Log.d("response", "onResponse: " + jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
