package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.GiftAdapter;
import com.example.hir_normal.adapter.NotificationAdapter;
import com.example.hir_normal.apiCallBack.GiftListCallBack;
import com.example.hir_normal.apiCallBack.NotificationCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.NotificationData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;
    @BindView(R.id.rv_notification)
    RecyclerView rv_notification;

    private Context mContext;
    private NotificationAdapter notificationAdapter;
    private ArrayList<NotificationData> data;
    private String classId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mContext = NotificationActivity.this;
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        if (Function.isNetworkAvailable(mContext)) {
            getNotificationData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
        tv_title.setText(getString(R.string.notification));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }

    private void getNotificationData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<NotificationCallBack> notificationCallBackCall = api.getNotifications(classId);
        notificationCallBackCall.enqueue(new Callback<NotificationCallBack>() {
            @Override
            public void onResponse(@NonNull Call<NotificationCallBack> call, @NonNull Response<NotificationCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<NotificationData>) response.body().getData();
                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_notification.setVisibility(View.VISIBLE);

                        notificationAdapter = new NotificationAdapter(mContext, data);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);

                        //rvGift.setLayoutManager(new GridLayoutManager(mContext, 2));
                        rv_notification.setLayoutManager(linearLayoutManager);
                        rv_notification.setAdapter(notificationAdapter);
                    } else {
                        rv_notification.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
