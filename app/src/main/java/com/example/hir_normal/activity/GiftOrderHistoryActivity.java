package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.OrderGiftStatusAdapter;
import com.example.hir_normal.adapter.OrderStatusAdapter;
import com.example.hir_normal.apiCallBack.GiftCartListCallBack;
import com.example.hir_normal.apiCallBack.GiftOrderHistoryCallBack;
import com.example.hir_normal.apiCallBack.OrderHistoryCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.GiftOrderHistoryData;
import com.example.hir_normal.model.OrderHistory.Order;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftOrderHistoryActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_total_order_point_value)
    TextView tv_total_order_point_value;
    @BindView(R.id.tv_total_pending_point_value)
    TextView tv_total_pending_point_value;
    @BindView(R.id.rv_gift_order_list)
    RecyclerView rv_gift_order_list;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private String userId = "";
    private OrderGiftStatusAdapter orderGiftStatusAdapter;
    private ArrayList<GiftOrderHistoryData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_order_history);
        mContext = GiftOrderHistoryActivity.this;
        ButterKnife.bind(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        tv_title.setText(R.string.reward_history);
        userId = Function.getPrefData(Preferences.USER_ID, mContext);

        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
        Log.d("userId", "init: " + userId);

        if (Function.isNetworkAvailable(mContext)) {
            getGiftOrderHistoryData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }
    }

    private void getGiftOrderHistoryData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();

        Call<GiftOrderHistoryCallBack> giftOrderHistoryCallBackCall = api.getGiftOrderHistory(userId);
        giftOrderHistoryCallBackCall.enqueue(new Callback<GiftOrderHistoryCallBack>() {
            @Override
            public void onResponse(@NonNull Call<GiftOrderHistoryCallBack> call, @NonNull Response<GiftOrderHistoryCallBack> response) {
                dialog.dismiss();
                if (response.code() == 200) {

                   /* tv_total_order_point_value.setText(response.body().getData().getTotalPurchaseAmount().toString());
                    tv_total_pending_point_value.setText(response.body().getData().getTotalPendingAmount().toString());*/

                    data = new ArrayList<>();
                    data = (ArrayList<GiftOrderHistoryData>) response.body().getData();
                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_gift_order_list.setVisibility(View.VISIBLE);
                        orderGiftStatusAdapter = new OrderGiftStatusAdapter(mContext, data);
                        rv_gift_order_list.setLayoutManager(new LinearLayoutManager(mContext));
                        rv_gift_order_list.setAdapter(orderGiftStatusAdapter);
                    } else {
                        rv_gift_order_list.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<GiftOrderHistoryCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick({R.id.iv_back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
