package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.CatalogAdapter;
import com.example.hir_normal.adapter.TalkAdapter;
import com.example.hir_normal.apiCallBack.CatalogListCallBack;
import com.example.hir_normal.apiCallBack.HIRTalkCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.CatalogData;
import com.example.hir_normal.model.TalkData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HirTalkActivity extends AppCompatActivity {

    @BindView(R.id.rv_hir_talk)
    RecyclerView rv_hir_talk;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;
    @BindView(R.id.tvViewCart)
    TextView tvViewCart;

    private Context mContext;
    private TalkAdapter talkAdapter;
    private ArrayList<TalkData> data;
    private String classId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hir_talk);
        mContext = HirTalkActivity.this;
        ButterKnife.bind(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        classId = Function.getPrefData(Preferences.CLASS_ID, mContext);
        tv_title.setText(getString(R.string.talk));
        if (Function.getPrefData(Preferences.USER_ROLE, mContext).equalsIgnoreCase("1")) {
            tvViewCart.setVisibility(View.VISIBLE);
            tvViewCart.setText(getString(R.string.rewards));
        }

        if (Function.isNetworkAvailable(mContext)) {
            getHirTalkData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PointHistoryActivity.class));
            }
        });
    }


    private void getHirTalkData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<HIRTalkCallBack> hirTalkCallBackCall = api.getHirTalk(classId);
        hirTalkCallBackCall.enqueue(new Callback<HIRTalkCallBack>() {
            @Override
            public void onResponse(@NonNull Call<HIRTalkCallBack> call, @NonNull Response<HIRTalkCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<TalkData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_hir_talk.setVisibility(View.VISIBLE);

                        talkAdapter = new TalkAdapter(mContext, data);
                        rv_hir_talk.setLayoutManager(new LinearLayoutManager(mContext));
                        rv_hir_talk.setAdapter(talkAdapter);
                    } else {
                        rv_hir_talk.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<HIRTalkCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
