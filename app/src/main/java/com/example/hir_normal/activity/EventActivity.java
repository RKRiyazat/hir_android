package com.example.hir_normal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.adapter.EventsAdapter;
import com.example.hir_normal.adapter.RequirementsAdapter;
import com.example.hir_normal.apiCallBack.EventListCallBack;
import com.example.hir_normal.apiCallBack.RequirementsCallBack;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.EventData;
import com.example.hir_normal.model.RequirementsData;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity extends AppCompatActivity {

    @BindView(R.id.rv_event)
    RecyclerView rv_event;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_not_available)
    TextView tv_not_available;

    private Context mContext;
    private EventsAdapter eventsAdapter;
    private ArrayList<EventData> data;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        mContext = EventActivity.this;
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        userId = Function.getPrefData(Preferences.USER_ID, mContext);
        tv_title.setText(getString(R.string.event_list));

        if (Function.isNetworkAvailable(mContext)) {
            getEventsData();
        } else {
//            Toast.makeText(mContext, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            Function.showDialogue(mContext);
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getEventsData() {
        API api = MyApp.retrofit.create(API.class);
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait..");
        dialog.show();
        Call<EventListCallBack> eventListCallBackCall = api.getEvent(userId);
        eventListCallBackCall.enqueue(new Callback<EventListCallBack>() {
            @Override
            public void onResponse(@NonNull Call<EventListCallBack> call, @NonNull Response<EventListCallBack> response) {
                dialog.dismiss();
                if (Objects.requireNonNull(response.body()).getCode().equals("200")) {
                    data = new ArrayList<>();
                    data = (ArrayList<EventData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        tv_not_available.setVisibility(View.GONE);
                        rv_event.setVisibility(View.VISIBLE);

                        eventsAdapter = new EventsAdapter(mContext, data);
                        rv_event.setLayoutManager(new LinearLayoutManager(mContext));
                        rv_event.setAdapter(eventsAdapter);
                    } else {
                        rv_event.setVisibility(View.GONE);
                        tv_not_available.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EventListCallBack> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
