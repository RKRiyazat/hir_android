package com.example.hir_normal.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hir_normal.R;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Utility {
    public static final int DELETE = 9;
    public static final String SIMPLE_DATE_FORMAT4 = "dd-MM-yyyy hh:mm:ss";


    public static final int PRINT_MAX_QTY = 4;
    public static final int PRINT_MAX_DESC = 20;
    public static final int PRINT_MAX_PRICE = 7;
    public static final int PRINT_MAX_SPACE = 1;


    public static final String INTENT_DETAILS = "INTENT_DETAILS";
    public static final String INTENT_SUB_DETAILS = "INTENT_SUB_TABLE";

    public static final int CURRENT_CART = 0;
    public static final int KITCHEN_ORDER = 1;


    public static final String CATEGORIES_LIST_URL = "CategoryList";
    public static final String PRODUCT_SYNC_URL = "ProductList";
    public static final String LOGIN_URL = "Login";
    public static final String SELL_REGISTER_URL = "SellOrder";
    public static final String ORDER_HISTORY_URL = "OrderHistory";
    public static final String LOGOUT = "logout";
    public static final String GET_CART_DATA = "get_cart_data";
    public static final String ADDDEVICEID = "addManualDevice";

    // live url
    public static final String MAIN_URL = "http://insttaorder.com/api/";


    // devlopement url
    //public static final String MAIN_URL = "http://cmexpertiseinfotech.com/instta_order/api/";


    public static final String FILE_EXT = ".txt";
    public static boolean musicplayisactive = false;

    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    public static String isStringMaxItemName(String s, int n) {
        String newString = "";
        if (s.length() > n) {

            newString = s.substring(0, n);
            StringBuilder myName = new StringBuilder(newString);
            myName.setCharAt(n - 1, '.');
            newString = myName.toString();
        } else if (s.length() == n) {
            newString = s;
        } else {
            newString = s;
        }
        return newString;
    }

    public static String isStringMax(String s, int n) {
        String newString = "";
        if (s.length() > n) {
            newString = s.replaceAll("(.{1," + n + "})\\s+", "$1\n");
        } else if (s.length() == n) {
            newString = s;
        } else {
            newString = s;
        }
        return newString;
//        return String.format("%1$" + n + "s", s);
    }


    public static void openSetting(Context mContext) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        mContext.startActivity(intent);
    }

    public static String Epoch2DateString(long epochSeconds, String formatString) {
        Date updatedate = new Date(epochSeconds * 1000);
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        return format.format(updatedate);
    }


    public static void stopmusic(Context act) {
        AudioManager mAudioManager = (AudioManager) act.getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager.isMusicActive()) {

            musicplayisactive = true;
            Intent i = new Intent("com.android.music.musicservicecommand");
            i.putExtra("command", "pause");
            act.sendBroadcast(i);
        }
    }

    public static void showAlertDialog(Context mContext, String title, String msg) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    static public void log(String msg) {
        Log.i("GoGolfGPS", msg);
    }

    public static void showAlert(final Activity activity, final String caption,
                                 final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity)
                        // .setIcon(R.drawable.about)
                        .setTitle(caption)
                        .setMessage(msg)
                        .setNeutralButton(
                                activity.getString(android.R.string.ok), null)
                        .show();
            }
        });
    }

    // locks orientation in place until app exits
    public static void lockToOriginalOrientation(Activity activity) {
        // switch (activity.getResources().getConfiguration().orientation)
        // {
        // case Configuration.ORIENTATION_PORTRAIT:
        // activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        // break;
        // case Configuration.ORIENTATION_LANDSCAPE:
        // //
        // activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        // break;
        // }

        activity.setRequestedOrientation(getScreenOrientation(activity));
    }

    // returns value for actual screen orientation
    public static int getScreenOrientation(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        int rotation = display.getRotation();

        Point size = new Point(display.getWidth(), display.getHeight());

        int lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) {
            // if rotation is 0 or 180 and width is greater than height, we have
            // a tablet
            if (size.x > size.y) {
                if (rotation == Surface.ROTATION_0) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                }
            } else {
                // we have a phone
                if (rotation == Surface.ROTATION_0) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                }
            }
        } else {
            // if rotation is 90 or 270 and width is greater than height, we
            // have a phone
            if (size.x > size.y) {
                if (rotation == Surface.ROTATION_90) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                }
            } else {
                // we have a tablet
                if (rotation == Surface.ROTATION_90) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                }
            }
        }
        return lock;
    }

    public static void hidekeyboard(Context context, View edittext) {

        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void resumemusic(Context act) {

        if (musicplayisactive) {
            musicplayisactive = false;
            Intent i = new Intent("com.android.music.musicservicecommand");
            i.putExtra("command", "play");
            act.sendBroadcast(i);
            AudioManager audioManager = (AudioManager) act.getSystemService(Context.AUDIO_SERVICE);
            audioManager.requestAudioFocus(null, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }

    }

    public static void showSettingsAlert(final Context mContext) {
        try {


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

            // Setting Dialog Title
            alertDialog.setTitle("Internet settings");

            // Setting Dialog Message
            alertDialog.setMessage("Internet is not enabled. Do you want to go to settings menu?");

            // On pressing the Settings button.
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    mContext.startActivity(intent);
                }
            });

            // On pressing the cancel button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void showToolBar(Toolbar toolbar,
                                   final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activity.finish();
            }
        });
    }*/

    public static Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }

    public static boolean checkFileExist(String fileName, Context mContext) {
        final File path = Environment.getExternalStoragePublicDirectory(
                //Environment.DIRECTORY_PICTURES
//                                Environment.DIRECTORY_DCIM +
                "/" + mContext.getString(R.string.app_name) + "/"
        );

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, fileName);


        return file.exists();
    }

    public static String replaceWithComa(String data) {

        return data.replace(".", "-");
    }
}
