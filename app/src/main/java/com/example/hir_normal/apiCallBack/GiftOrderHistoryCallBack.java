package com.example.hir_normal.apiCallBack;

import com.example.hir_normal.model.GiftOrderHistoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiftOrderHistoryCallBack {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<GiftOrderHistoryData> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GiftOrderHistoryData> getData() {
        return data;
    }

    public void setData(List<GiftOrderHistoryData> data) {
        this.data = data;
    }

}
