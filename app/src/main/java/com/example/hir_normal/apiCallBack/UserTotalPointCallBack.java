package com.example.hir_normal.apiCallBack;

import com.example.hir_normal.model.UserTotalPointsData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserTotalPointCallBack {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private UserTotalPointsData data;
    @SerializedName("code")
    @Expose
    private String code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserTotalPointsData getData() {
        return data;
    }

    public void setData(UserTotalPointsData data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
