package com.example.hir_normal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.R;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.model.EarningData;
import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.RedeemData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RedeemListAdapter extends RecyclerView.Adapter<RedeemListAdapter.MyViewHolder> {
    private Context mContext;
    private List<RedeemData> data;

    public RedeemListAdapter(Context mContext, List<RedeemData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_available_earning, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvPointValue.setText(data.get(position).getPoints());
//        holder.tvAvailableCreditValue.setText(data.get(position).getCreatedAt());
        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tvAvailableCreditValue.setText(date);
        }
        if (data.get(position).getGift() != null) {
            holder.tvAvailableItemValue.setText(data.get(position).getGift().getName());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAvailablePoint)
        TextView tvAvailablePoint;
        @BindView(R.id.tvPointValue)
        TextView tvPointValue;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tvAvailableCredit)
        TextView tvAvailableCredit;
        @BindView(R.id.tvAvailableCreditValue)
        TextView tvAvailableCreditValue;
        @BindView(R.id.view1)
        View view1;
        @BindView(R.id.tvAvailableItem)
        TextView tvAvailableItem;
        @BindView(R.id.tvAvailableItemValue)
        TextView tvAvailableItemValue;
        @BindView(R.id.view2)
        View view2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvAvailableCredit.setText(mContext.getString(R.string.redeem));
        }
    }
}
