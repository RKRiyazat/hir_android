package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.PdfViewerActivity;
import com.example.hir_normal.activity.VideoPlayerActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.model.DesignData;
import com.example.hir_normal.model.GiftData;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DesignsAdapter extends RecyclerView.Adapter<DesignsAdapter.MyViewHolder> {
    private Context mContext;
    private List<DesignData> data;

    public DesignsAdapter(Context mContext, List<DesignData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_designs_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_design_title.setText(data.get(position).getTitle());
        holder.tv_design_description.setText(data.get(position).getDescription());

        if (data.get(position).getImage() != null && !data.get(position).getImage().equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + data.get(position).getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(holder.iv_design_image);
        } else {
            holder.iv_design_image.setImageResource(R.drawable.ic_image_placeholder);
        }

        holder.iv_design_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, PdfViewerActivity.class)
                        .putExtra(Constants.PDF_URL, data.get(position).getPdf()));
            }
        });

        if (data.get(position).getLink() != null &&
                !data.get(position).getLink().equalsIgnoreCase("")) {
            holder.iv_design_video.setVisibility(View.VISIBLE);
        } else {
            holder.iv_design_video.setVisibility(View.GONE);
        }

        holder.iv_design_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, VideoPlayerActivity.class)
                        .putExtra(Constants.FROM, Constants.VIDEO)
                        .putExtra(Constants.PDF_URL, data.get(position).getPdf())
                        .putExtra(Constants.DESCRIPTION, data.get(position).getDescription())
                        .putExtra(Constants.VIDEO_LINK, data.get(position).getLink())
                        .putExtra(Constants.VIDEO_TITLE, data.get(position).getTitle()));
            }
        });

        holder.iv_design_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(data.get(position).getImage());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_design_title)
        TextView tv_design_title;
        @BindView(R.id.tv_design_description)
        TextView tv_design_description;
        @BindView(R.id.iv_design_pdf)
        ImageView iv_design_pdf;
        @BindView(R.id.iv_design_image)
        ImageView iv_design_image;
        @BindView(R.id.iv_design_video)
        ImageView iv_design_video;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void viewImageDialog(String image) {
        final Dialog popupDialog;
        popupDialog = new Dialog(mContext);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

        popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
        WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
        popupDialog.setCanceledOnTouchOutside(true);
        popupDialog.getWindow().setAttributes(params);

        PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
        ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
        TextView tv_title = popupDialog.findViewById(R.id.tv_title);

        iv_back.setVisibility(View.VISIBLE);
        photo_view.setVisibility(View.VISIBLE);

        tv_title.setText(mContext.getString(R.string.preview));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        Picasso.get()
                .load(Constants.IMAGE_BASE_URL + image)
                .error(R.drawable.ic_image_placeholder)
                .into(photo_view);

        popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupDialog.getWindow().setGravity(Gravity.CENTER);
        popupDialog.show();
        popupDialog.setCancelable(true);
    }
}
