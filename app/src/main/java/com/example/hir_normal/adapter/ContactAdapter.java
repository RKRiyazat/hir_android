package com.example.hir_normal.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.ProductListActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.model.ContactData;
import com.example.hir_normal.model.ProductData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    private Context mContext;
    private List<ContactData> data;
    public final int REQUEST_PHONE_CALL = 1234;

    public ContactAdapter(Context mContext, List<ContactData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_contact, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_name_value.setText(data.get(position).getName());
        holder.tv_email_value.setText(data.get(position).getEmail());
        holder.tv_mobile_value.setText(data.get(position).getPhone());
        holder.tv_post_value.setText(data.get(position).getPost());

        holder.tv_mobile_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    Intent intent1 = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", data.get(position).getPhone(), null));
                    mContext.startActivity(intent1);
                }
            }
        });

        holder.tv_email_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{data.get(position).getEmail()});
                email.putExtra(Intent.EXTRA_SUBJECT, "info");
                email.putExtra(Intent.EXTRA_TEXT, "hello");

                //need this to prompts email client only
                email.setType("message/rfc822");

                mContext.startActivity(Intent.createChooser(email, mContext.getString(R.string.choose_an_email_client)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name_value)
        TextView tv_name_value;
        @BindView(R.id.tv_email_value)
        TextView tv_email_value;
        @BindView(R.id.tv_mobile_value)
        TextView tv_mobile_value;
        @BindView(R.id.tv_post_value)
        TextView tv_post_value;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
