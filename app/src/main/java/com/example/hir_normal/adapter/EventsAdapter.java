package com.example.hir_normal.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.AddRequirementActivity;
import com.example.hir_normal.activity.VideoExoPlayerActivity;
import com.example.hir_normal.apiCallBack.RequirementsDeleteCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.EventData;
import com.example.hir_normal.model.RequirementsData;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {
    private Context mContext;
    private List<EventData> data;

    public EventsAdapter(Context mContext, List<EventData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_events, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_title_re_value.setText(data.get(position).getData().getTitle());
        holder.tv_remark_value.setText(data.get(position).getData().getRemark());
        String receiveDateTextView = Function.dateFormatWithoutChangeShort(data.get(position).getData().getEventdate());

        if (receiveDateTextView != null && !receiveDateTextView.equalsIgnoreCase("")) {
            holder.tv_event_date_value.setText(receiveDateTextView);
        }

        if (data.get(position).getData().getImage() != null && !data.get(position).getData().getImage().equalsIgnoreCase("")) {
            holder.iv_image.setVisibility(View.VISIBLE);
        } else {
            holder.iv_image.setVisibility(View.GONE);
        }

        if (data.get(position).getData().getVideo() != null && !data.get(position).getData().getVideo().equalsIgnoreCase("")) {
            holder.iv_video.setVisibility(View.VISIBLE);
        } else {
            holder.iv_video.setVisibility(View.GONE);
        }

        holder.iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.viewImageDialog(data.get(position).getData().getImage());
            }
        });

        holder.iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, VideoExoPlayerActivity.class)
                        .putExtra(Constants.VIDEO_URL, Constants.IMAGE_BASE_URL + data.get(position).getData().getVideo())
                        .putExtra(Constants.VIDEO_TYPE, "live"));
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title_re_value)
        TextView tv_title_re_value;
        @BindView(R.id.tv_remark_value)
        TextView tv_remark_value;
        @BindView(R.id.tv_event_date_value)
        TextView tv_event_date_value;
        @BindView(R.id.iv_image)
        ImageView iv_image;
        @BindView(R.id.iv_video)
        ImageView iv_video;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void viewImageDialog(String image) {
            final Dialog popupDialog;
            popupDialog = new Dialog(mContext);
            popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

            popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
            WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
            popupDialog.setCanceledOnTouchOutside(true);
            popupDialog.getWindow().setAttributes(params);

            PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
            ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
            TextView tv_title = popupDialog.findViewById(R.id.tv_title);

            iv_back.setVisibility(View.VISIBLE);
            photo_view.setVisibility(View.VISIBLE);

            tv_title.setText(mContext.getString(R.string.preview));
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupDialog.dismiss();
                }
            });
            Picasso.get()
                    .load(Constants.IMAGE_BASE_URL + image)
                    .error(R.drawable.ic_image_placeholder)
                    .into(photo_view);

            popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            popupDialog.getWindow().setGravity(Gravity.CENTER);
            popupDialog.show();
            popupDialog.setCancelable(true);
        }
    }
}
