package com.example.hir_normal.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.R;
import com.example.hir_normal.activity.PaymentHistoryActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.model.OrderHistory.Order;
import com.example.hir_normal.model.PaymentHistory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    private Context mContext;
    private List<Order> data;
    private List<PaymentHistory> paymentHistoryList;

    public OrderHistoryAdapter(Context mContext, List<Order> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_order_history, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_order_id_value.setText(data.get(position).getId().toString());

        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tv_order_date_value.setText(date);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentHistoryList = new ArrayList<>();
                paymentHistoryList = data.get(position).getPaymentHistory();

                if (paymentHistoryList != null && paymentHistoryList.size() > 0) {
                    mContext.startActivity(new Intent(mContext, PaymentHistoryActivity.class)
                            .putExtra(Constants.PAYMENT_HISTORY_LIST, (Serializable) paymentHistoryList));
                } else {
                    Toast.makeText(mContext, R.string.no_payment_history, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_id_value)
        TextView tv_order_id_value;
        @BindView(R.id.tv_order_date_value)
        TextView tv_order_date_value;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
