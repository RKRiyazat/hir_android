package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.EarningData;
import com.example.hir_normal.model.ProductData;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarningListAdapter extends RecyclerView.Adapter<EarningListAdapter.MyViewHolder> {
    private Context mContext;
    private List<EarningData> data;

    public EarningListAdapter(Context mContext, List<EarningData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_available_earning, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvPointValue.setText(data.get(position).getPoints());
        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tvAvailableCreditValue.setText(date);
        }
        if (data.get(position).getProduct() != null) {
            holder.tvAvailableItemValue.setText(data.get(position).getProduct().getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAvailablePoint)
        TextView tvAvailablePoint;
        @BindView(R.id.tvPointValue)
        TextView tvPointValue;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tvAvailableCredit)
        TextView tvAvailableCredit;
        @BindView(R.id.tvAvailableCreditValue)
        TextView tvAvailableCreditValue;
        @BindView(R.id.view1)
        View view1;
        @BindView(R.id.tvAvailableItem)
        TextView tvAvailableItem;
        @BindView(R.id.tvAvailableItemValue)
        TextView tvAvailableItemValue;
        @BindView(R.id.view2)
        View view2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
