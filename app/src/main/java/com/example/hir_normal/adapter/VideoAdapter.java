package com.example.hir_normal.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.ProductListActivity;
import com.example.hir_normal.activity.VideoPlayerActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.model.ProductData;
import com.example.hir_normal.model.VideoData;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<VideoData> data;

    public VideoAdapter(Context mContext, ArrayList<VideoData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_video_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        try
        {

            holder.tvProduct.setText(data.get(position).getTitle());
            if (data.get(position).getImage() != null &&
                    !data.get(position).getImage().equalsIgnoreCase("")) {
                Glide.with(mContext)
                        .load(Constants.IMAGE_BASE_URL + data.get(position).getImage())
                        .placeholder(R.drawable.ic_image_placeholder)
                        .error(R.drawable.ic_image_placeholder)
                        .into(holder.ivProduct);
            } else {
                holder.ivProduct.setImageResource(R.drawable.ic_image_placeholder);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, VideoPlayerActivity.class)
                            .putExtra(Constants.FROM, Constants.VIDEO)
                            .putExtra(Constants.VIDEO_LINK, data.get(position).getLink())
                            .putExtra(Constants.VIDEO_TITLE, data.get(position).getTitle()));
                }
            });
            String url = data.get(position).getLink();
            String id = url.split("v=")[1];


            String img_url = "https://img.youtube.com/vi/" + id + "/0.jpg"; // this is link which will give u thumnail image of that video


            Log.d("youtube_thumb", "onBindViewHolder: " + img_url);

            // picasso jar file download image for u and set image in imagview

            Picasso.get()
                    .load(img_url)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .into(holder.ivProduct);

            holder.ivShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String shareBody1 = data.get(position).getLink();
                    Intent sharingIntent1 = new Intent(Intent.ACTION_SEND);
                    sharingIntent1.setType("text/plain");
                    sharingIntent1.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.share));
                    sharingIntent1.putExtra(Intent.EXTRA_TEXT, shareBody1);
                    mContext.startActivity(Intent.createChooser(sharingIntent1, mContext.getResources().getString(R.string.share_using)));
                }
            });

        }catch (Exception e)
        {e.printStackTrace();}



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivProduct)
        ImageView ivProduct;
        @BindView(R.id.tvProduct)
        TextView tvProduct;
        @BindView(R.id.clMain)
        ConstraintLayout clMain;
        @BindView(R.id.ivShare)
        ImageView ivShare;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
