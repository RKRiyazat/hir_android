package com.example.hir_normal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.R;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.model.PaymentHistory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<PaymentHistory> data;

    public PaymentHistoryAdapter(Context mContext, ArrayList<PaymentHistory> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_payment_history, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_order_id_value.setText(data.get(position).getOrderId());
//        holder.tv_order_date_value.setText(data.get(position).getCreatedAt());
        holder.tv_amount_value.setText(data.get(position).getRs());
        holder.tv_pending_amount_value.setText(data.get(position).getPendingBalance());

        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tv_order_date_value.setText(date);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_id_value)
        TextView tv_order_id_value;
        @BindView(R.id.tv_order_date_value)
        TextView tv_order_date_value;
        @BindView(R.id.tv_amount_value)
        TextView tv_amount_value;
        @BindView(R.id.tv_pending_amount_value)
        TextView tv_pending_amount_value;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
