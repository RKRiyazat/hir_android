package com.example.hir_normal.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.AddRequirementActivity;
import com.example.hir_normal.activity.ProductListActivity;
import com.example.hir_normal.activity.VideoExoPlayerActivity;
import com.example.hir_normal.apiCallBack.RequirementsCallBack;
import com.example.hir_normal.apiCallBack.RequirementsDeleteCallBack;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.ProductData;
import com.example.hir_normal.model.RequirementsData;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequirementsAdapter extends RecyclerView.Adapter<RequirementsAdapter.MyViewHolder> {
    private Context mContext;
    private List<RequirementsData> data;

    public RequirementsAdapter(Context mContext, List<RequirementsData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_requirement, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_title_re_value.setText(data.get(position).getTitle());
        holder.tv_description_value.setText(data.get(position).getDescription());
        holder.tv_address_value.setText(data.get(position).getAddress());
        holder.tv_qty_value.setText(data.get(position).getQty());

        if (data.get(position).getImage() != null && !data.get(position).getImage().equalsIgnoreCase("")) {
            holder.iv_image.setVisibility(View.VISIBLE);
        } else {
            holder.iv_image.setVisibility(View.INVISIBLE);
        }

        if (data.get(position).getVideo() != null && !data.get(position).getVideo().equalsIgnoreCase("")) {
            holder.iv_video.setVisibility(View.VISIBLE);
        } else {
            holder.iv_video.setVisibility(View.INVISIBLE);
        }

        holder.iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.viewImageDialog(data.get(position).getImage());
            }
        });

        holder.iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, VideoExoPlayerActivity.class)
                        .putExtra(Constants.VIDEO_URL, Constants.IMAGE_BASE_URL + data.get(position).getVideo())
                        .putExtra(Constants.VIDEO_TYPE, "live"));
            }
        });

        String receiveDateTextView = Function.dateFormatWithoutChangeShort(data.get(position).getReceiveDate());

        if (receiveDateTextView != null && !receiveDateTextView.equalsIgnoreCase("")) {
            holder.tv_receive_date_value.setText(receiveDateTextView);
        }

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getString(R.string.want_to_delete));
                builder.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Function.isNetworkAvailable(mContext)) {
                            holder.deleteRequirement(position);
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, AddRequirementActivity.class);
                intent.putExtra(Constants.FROM, Constants.UPDATE)
                        .putExtra(Constants.REQUIREMENT_ID, data.get(position).getId())
                        .putExtra(Constants.REQUIREMENT_TITLE, data.get(position).getTitle())
                        .putExtra(Constants.REQUIREMENT_DESCRIPTION, data.get(position).getDescription())
                        .putExtra(Constants.REQUIREMENT_ADDRESS, data.get(position).getAddress())
                        .putExtra(Constants.REQUIREMENT_QTY, data.get(position).getQty())
                        .putExtra(Constants.REQUIREMENT_RECEIVE_DATE, data.get(position).getReceiveDate())
                        .putExtra(Constants.REQUIREMENT_IMAGE, data.get(position).getImage())
                        .putExtra(Constants.REQUIREMENT_VIDEO, data.get(position).getVideo());
                ((Activity) mContext).startActivityForResult(intent, Constants.REQUIREMENT_UPDATE_CODE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title_re_value)
        TextView tv_title_re_value;
        @BindView(R.id.tv_description_value)
        TextView tv_description_value;
        @BindView(R.id.tv_address_value)
        TextView tv_address_value;
        @BindView(R.id.tv_qty_value)
        TextView tv_qty_value;
        @BindView(R.id.tv_receive_date_value)
        TextView tv_receive_date_value;
        @BindView(R.id.iv_delete)
        ImageView iv_delete;
        @BindView(R.id.iv_edit)
        ImageView iv_edit;
        @BindView(R.id.iv_image)
        ImageView iv_image;
        @BindView(R.id.iv_video)
        ImageView iv_video;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void deleteRequirement(int position) {
            Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
            dialog.show();
            API api = MyApp.retrofit.create(API.class);


            Call<RequirementsDeleteCallBack> deleteRequirementCallBack = null;
       /* if (value == 0) {
            jsonObjectCall = api.addToCart(data.get(0).getUserId(),
                    data.get(0).getProductId(), String.valueOf(price), String.valueOf(value), "0");
        } else {*/
            deleteRequirementCallBack = api.deleteRequirements(data.get(position).getId().toString());
//        }
            deleteRequirementCallBack.enqueue(new Callback<RequirementsDeleteCallBack>() {
                @Override
                public void onResponse(@NonNull Call<RequirementsDeleteCallBack> call, @NonNull Response<RequirementsDeleteCallBack> response) {
                    dialog.dismiss();

                    String code = response.body().getCode();
                    if (code.equalsIgnoreCase("500")) {
                        Toast.makeText(mContext, mContext.getString(R.string.inter_server_error), Toast.LENGTH_SHORT).show();
                    }
                    if (code.equals("200")) {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        data.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, data.size());
                    } else {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RequirementsDeleteCallBack> call, @NonNull Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void viewImageDialog(String image) {
            final Dialog popupDialog;
            popupDialog = new Dialog(mContext);
            popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

            popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
            WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
            popupDialog.setCanceledOnTouchOutside(true);
            popupDialog.getWindow().setAttributes(params);

            PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
            ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
            TextView tv_title = popupDialog.findViewById(R.id.tv_title);

            iv_back.setVisibility(View.VISIBLE);
            photo_view.setVisibility(View.VISIBLE);

            tv_title.setText(mContext.getString(R.string.preview));
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupDialog.dismiss();
                }
            });
            Picasso.get()
                    .load(Constants.IMAGE_BASE_URL + image)
                    .error(R.drawable.ic_image_placeholder)
                    .into(photo_view);

            popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            popupDialog.getWindow().setGravity(Gravity.CENTER);
            popupDialog.show();
            popupDialog.setCancelable(true);
        }
    }
}
