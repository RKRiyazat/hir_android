package com.example.hir_normal.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.R;
import com.example.hir_normal.activity.PdfViewerActivity;
import com.example.hir_normal.activity.VideoPlayerActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.model.CatalogLink;
import com.example.hir_normal.model.CatalogPDF;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatalogVideoAdapter extends RecyclerView.Adapter<CatalogVideoAdapter.MyViewHolder> {
    private Context mContext;
    private List<CatalogLink> data;

    public CatalogVideoAdapter(Context mContext, List<CatalogLink> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_pdf_video_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.iv_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mContext.startActivity(new Intent(mContext, VideoPlayerActivity.class)
                                .putExtra(Constants.FROM, Constants.CATALOG)
                                .putExtra(Constants.VIDEO_LINK, data.get(position).getLink())
                                .putExtra(Constants.VIDEO_TITLE, ""));
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image)
        ImageView iv_image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            iv_image.setImageResource(R.drawable.ic_youtube);
        }
    }
}
