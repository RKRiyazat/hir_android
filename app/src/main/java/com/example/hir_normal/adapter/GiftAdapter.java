package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnGiftItemClick;
import com.example.hir_normal.interfaces.OnProductItemClick;
import com.example.hir_normal.model.GiftData;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.MyViewHolder> {
    private Context mContext;
    private List<GiftData> data;
    private int value = 1;
    private OnGiftItemClick onGiftItemClick;

    public GiftAdapter(Context mContext, List<GiftData> data) {
        this.mContext = mContext;
        this.data = data;
        this.onGiftItemClick = (OnGiftItemClick) mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_gift_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_gift_title.setText(data.get(position).getName());
        holder.tv_gift_description.setText(data.get(position).getDescription());
        holder.tv_available_point.setText("Points: " + data.get(position).getPoints());
        holder.tv_qty_value.setText(String.valueOf(data.get(position).getCartQuantity()));

        if (data.get(position).getCartQuantity() > 0) {
            holder.cl_quantity.setVisibility(View.VISIBLE);
            holder.tv_add_to_cart.setVisibility(View.GONE);
        } else {
            holder.cl_quantity.setVisibility(View.GONE);
            holder.tv_add_to_cart.setVisibility(View.VISIBLE);
        }

        if (data.get(position).getCartQuantity() <= 1) {
            holder.iv_minus.setImageResource(R.drawable.ic_minus_disable);
            holder.iv_minus.setEnabled(false);
        } else {
            holder.iv_minus.setEnabled(true);
            holder.iv_minus.setImageResource(R.drawable.ic_minus);
        }

        if (data.get(position).getImage() != null && !data.get(position).getImage().equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + data.get(position).getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(holder.iv_gift_image);
        } else {
            holder.iv_gift_image.setImageResource(R.drawable.ic_image_placeholder);
        }

        holder.iv_gift_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(data.get(position).getImage());
            }
        });

        holder.tv_qty_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onGiftItemClick.onGiftItemClick(position, data.get(position));
            }
        });

        holder.iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Function.isNetworkAvailable(mContext)) {
                    value = Integer.parseInt(holder.tv_qty_value.getText().toString());
                    value = value + 1;
                    data.get(position).setCartQuantity(value);
                    holder.callAddToCartApi(position, value, Constants.UPDATE);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value = Integer.parseInt(holder.tv_qty_value.getText().toString());
                if (value != 1) {
                    if (Function.isNetworkAvailable(mContext))
                        value = value - 1;
                    data.get(position).setCartQuantity(value);
                    holder.callAddToCartApi(position, value, Constants.UPDATE);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.tv_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Function.isNetworkAvailable(mContext)) {
                    holder.callAddToCartApi(position, 1, Constants.ADD);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_gift_title)
        TextView tv_gift_title;
        @BindView(R.id.tv_gift_description)
        TextView tv_gift_description;
        @BindView(R.id.iv_gift_image)
        ImageView iv_gift_image;
        @BindView(R.id.iv_plus)
        ImageView iv_plus;
        @BindView(R.id.iv_minus)
        ImageView iv_minus;
        @BindView(R.id.tv_qty_value)
        TextView tv_qty_value;
        @BindView(R.id.cl_quantity)
        ConstraintLayout cl_quantity;
        @BindView(R.id.tv_available_point)
        TextView tv_available_point;
        @BindView(R.id.tv_add_to_cart)
        TextView tv_add_to_cart;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void callAddToCartApi(int position, int value, String type) {
            Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
            dialog.show();
            API api = MyApp.retrofit.create(API.class);
            Double price = Double.parseDouble(data.get(position).getPoints()) * value;
//            Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();

            Log.d("parameter", "callAddToCartApi: " + Function.getPrefData(Preferences.USER_ID, mContext));
            Log.d("parameter", "callAddToCartApi: " + data.get(position).getId());
            Log.d("parameter", "callAddToCartApi: " + price);
            Log.d("parameter", "callAddToCartApi: " + value);
            Call<JsonObject> jsonObjectCall = api.addGiftToCart(Function.getPrefData(Preferences.USER_ID, mContext),
                    String.valueOf(data.get(position).getId()), String.valueOf(price), String.valueOf(value), "0");

            jsonObjectCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    dialog.dismiss();
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                        String code = jsonObject.getString("code");
                        if (code.equals("200")) {
                            if (type.equalsIgnoreCase(Constants.ADD)) {
                                data.get(position).setCartQuantity(1);
                            }
                            notifyItemChanged(position);
//                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void viewImageDialog(String image) {
        final Dialog popupDialog;
        popupDialog = new Dialog(mContext);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

        popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
        WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
        popupDialog.setCanceledOnTouchOutside(true);
        popupDialog.getWindow().setAttributes(params);

        PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
        ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
        TextView tv_title = popupDialog.findViewById(R.id.tv_title);

        iv_back.setVisibility(View.VISIBLE);
        photo_view.setVisibility(View.VISIBLE);

        tv_title.setText(mContext.getString(R.string.preview));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        Picasso.get()
                .load(Constants.IMAGE_BASE_URL + image)
                .error(R.drawable.ic_image_placeholder)
                .into(photo_view);

        popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupDialog.getWindow().setGravity(Gravity.CENTER);
        popupDialog.show();
        popupDialog.setCancelable(true);
    }
}
