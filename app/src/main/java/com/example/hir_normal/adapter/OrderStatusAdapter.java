package com.example.hir_normal.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.OrderProductActivity;
import com.example.hir_normal.activity.PaymentHistoryActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.OrderHistory.Order;
import com.example.hir_normal.model.OrderHistory.OrderProduct;
import com.example.hir_normal.model.PaymentHistory;
import com.example.hir_normal.utility.Utility;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusAdapter extends RecyclerView.Adapter<OrderStatusAdapter.MyViewHolder> {
    private Context mContext;
    private List<Order> data;
    private List<OrderProduct> orderProductList;

    public File file;
    public String fileName;
    public Format dfOne;
    public Dialog pDialog;

    public OrderStatusAdapter(Context mContext, List<Order> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_order_status, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_order_id_value.setText(data.get(position).getId().toString());
//        holder.tv_order_date_value.setText(data.get(position).getCreatedAt());

        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tv_order_date_value.setText(date);
        }
        double totalAmount = 0.0;

        orderProductList = new ArrayList<>();
        orderProductList = data.get(position).getOrderProduct();

        if (orderProductList != null && orderProductList.size() > 0) {
            for (int i = 0; i < orderProductList.size(); i++) {

                double orderTotal = Double.parseDouble(orderProductList.get(i).getPrice()) * Double.parseDouble(orderProductList.get(i).getQuantity());
                totalAmount = totalAmount + orderTotal;

            }
        }

        holder.tv_order_total_value.setText(String.valueOf(totalAmount));

        /*1 = inprogress, 2 = waiting, 3 = on the way , 4 = delivered*/

        if (data.get(position).getStatus().equalsIgnoreCase("1")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_inprogress);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("2")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_waiting);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("3")) {
            holder.btn_cancel_order.setVisibility(View.VISIBLE);
            holder.btn_cancel_order.setText(mContext.getString(R.string.accept_order));
            holder.tv_order_status_value.setText(R.string.str_on_the_way);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("4")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_deliverd);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("5")) {
            holder.tv_order_status_value.setText(R.string.str_canceled);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorOrange));
            holder.btn_cancel_order.setVisibility(View.GONE);
        }

        holder.btn_cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Function.isNetworkAvailable(mContext)) {
                    if (holder.btn_cancel_order.getText().toString().equalsIgnoreCase(mContext.getString(R.string.accept_order))) {
                        apiCallStatusChange(position, data.get(position).getId().toString());
                    } else {
                        apiCallCancelOrder(position, data.get(position).getId().toString());
                    }
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderProductList = new ArrayList<>();
                orderProductList = data.get(position).getOrderProduct();

                if (orderProductList != null && orderProductList.size() > 0) {
                    mContext.startActivity(new Intent(mContext, OrderProductActivity.class)
                            .putExtra(Constants.ORDER_PRODUCT_LIST, (Serializable) orderProductList));
                } else {
                    Toast.makeText(mContext, R.string.no_payment_history, Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeToFilePaymentReceipt(data, "HIR_PROJECT", position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_id_value)
        TextView tv_order_id_value;
        @BindView(R.id.tv_order_date_value)
        TextView tv_order_date_value;
        @BindView(R.id.tv_order_status_value)
        TextView tv_order_status_value;
        @BindView(R.id.tv_order_total_value)
        TextView tv_order_total_value;
        @BindView(R.id.btn_cancel_order)
        Button btn_cancel_order;
        @BindView(R.id.iv_share)
        ImageView iv_share;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void apiCallCancelOrder(int position, String orderId) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);

        Call<JsonObject> jsonObjectCall = null;
        jsonObjectCall = api.cancelOrder(orderId);
//        }
        Objects.requireNonNull(jsonObjectCall).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        data.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, data.size());
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void apiCallStatusChange(int position, String orderId) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);

        Call<JsonObject> jsonObjectCall = null;
        jsonObjectCall = api.changeOrderStatus(orderId, "4");
//        }
        Objects.requireNonNull(jsonObjectCall).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        data.get(position).setStatus("4");
                        notifyItemChanged(position);
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void writeToFilePaymentReceipt(List<Order> orderList, String strFileName, int position) {
        orderProductList = new ArrayList<>();
        orderProductList = data.get(position).getOrderProduct();

        pDialog = ProgressDialog.show(mContext, "", "Please wait...");
        fileName = strFileName;
        File uriFile = null;
        final File path = new File(Environment.getExternalStorageDirectory() + "/HIR");
        File tempDir = path;
        // File sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        // File tempDir = new File(sdCard.getAbsolutePath() + "/"+CROP_CACHE_FOLDER);

        if (!tempDir.exists()) {
            try {
                boolean result = tempDir.mkdir();
            } catch (Exception e) {
                Log.e("TAG", "generateUri failed: " + tempDir, e);
            }
        }
        String name = "app" + System.currentTimeMillis();

        try {
            uriFile = File.createTempFile(fileName, Utility.FILE_EXT, tempDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (uriFile!=null && !uriFile.exists()) {
            uriFile.mkdirs();
        }

        StringBuilder data = new StringBuilder();
        int maxLine = Utility.PRINT_MAX_QTY + Utility.PRINT_MAX_DESC + Utility.PRINT_MAX_PRICE + Utility.PRINT_MAX_SPACE + Utility.PRINT_MAX_SPACE;
        String str = "";
        for (int i = 0; i < maxLine; i++) {
            str += "-";
        }
        data.append(Utility.padRight("HIR", maxLine) + "\n");
        data.append("Order No : " + orderList.get(position).getId() + "\n");
        Calendar c = Calendar.getInstance();
        data.append(Utility.padRight("Order Date : " + orderList.get(position).getCreatedAt() + "", maxLine) + "\n");

        String strSpace = "";
        for (int i = 0; i < Utility.PRINT_MAX_SPACE; i++) {
            strSpace += " ";
        }
        data.append(str + "\n");

        data.append(Utility.padRight("QTY", Utility.PRINT_MAX_QTY));
        data.append(strSpace);
        data.append(Utility.padRight("Item Name", Utility.PRINT_MAX_DESC));
        data.append(strSpace);
        data.append(Utility.padLeft("Amount", Utility.PRINT_MAX_PRICE) + "\n");
        data.append(str + "\n");

        for (OrderProduct cartView : orderProductList) {

            data.append(Utility.padRight(cartView.getQuantity() + "", 3));
            data.append(strSpace);
            data.append(Utility.padRight(cartView.getName() + "", 8));
            data.append(strSpace);
            data.append(Utility.padRight(cartView.getPrice() + "", 8) + "\n");

        }
        data.append(str + "\n\n");
        double totalPrice = 0;
        double discount = 0;
        double discounted = 0;
        double tax = 0;
        double totalToPay = 0;
        for (Order mOrderDetails : orderList) {
            totalPrice += Double.parseDouble(mOrderDetails.getOrderProduct().get(0).getPrice());
        }

        discount = 10;
        discounted = totalPrice - discount;
        tax = 18 * (discounted) / 100;
        totalToPay = (totalPrice + tax) - discount;


        data.append("Total = " + orderList.get(position).getTotal() + "\n");
        Toast.makeText(mContext, data.toString(), Toast.LENGTH_SHORT).show();
        file = uriFile;
        if (file.exists()) {
            try {
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(data);
                myOutWriter.close();
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }

                fOut.flush();
                fOut.close();
                if (file != null && file.length() > 0) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    sharingIntent.setType("text/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse( file.getAbsolutePath()));
                    mContext.startActivity(Intent.createChooser(sharingIntent, "share file with"));
                }
                return;
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        } else {
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(data);
                myOutWriter.close();
                fOut.flush();
                fOut.close();

                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (file != null && file.length() > 0) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
                    mContext.startActivity(Intent.createChooser(sharingIntent, "share file with"));
                }
                return;
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }



    }

   /* public void getMailAndSend() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_email, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        txtViewTotalDiscountAmount = (EditText) dialogView.findViewById(R.id.edt_mail);

        final AlertDialog b = dialogBuilder.create();

        TextView txtSend = (TextView) dialogView.findViewById(R.id.txt_send);
        TextView txtCancel = (TextView) dialogView.findViewById(R.id.txt_cancel);

        txtSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                userMail = txtViewTotalDiscountAmount.getText().toString().trim();


                if (!userMail.equals("")) {
                    if (Validation.isValidEmailAddress(userMail)) {


                        try {

                            File filepath = Environment.getExternalStoragePublicDirectory(
                                    "/" + getString(R.string.app_name) + "/" + getString(R.string.pay) + "/" + fileName);

                            *//*File filelocation = new File(filepath, fileName);*//*
                            Uri path = Uri.fromFile(filepath);


                            //AppUtility.showLog("---- file path" + file.getAbsolutePath());

                            AppUtility.showLog("---- file  name " + path);


                            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                            emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            emailIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            emailIntent.setType("plain/text");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{userMail});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Order Receipt");
                            if (URI != null) {
                                emailIntent.putExtra(Intent.EXTRA_STREAM, path);
                            }
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Message");
                            startActivity(Intent.createChooser(emailIntent, "Sending email..."));
                        } catch (Throwable t) {
                            Toast.makeText(MainActivity.this, "Request failed try again: " + t.toString(), Toast.LENGTH_LONG).show();
                            t.printStackTrace();
                        }
                        b.dismiss();


                    } else {
                        txtViewTotalDiscountAmount.setText("");

                        txtViewTotalDiscountAmount.setError("Please enter valid email address.");
                    }
                } else {

                    Toast.makeText(MainActivity.this, "Mail is empty", Toast.LENGTH_LONG).show();
                }


            }
        });


        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b.dismiss();

            }
        });


        b.show();
    }*/
}
