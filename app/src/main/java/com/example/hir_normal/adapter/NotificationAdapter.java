package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.function.Preferences;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnGiftItemClick;
import com.example.hir_normal.model.GiftData;
import com.example.hir_normal.model.NotificationData;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context mContext;
    private List<NotificationData> data;

    public NotificationAdapter(Context mContext, List<NotificationData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_notification_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_notification_title.setText(data.get(position).getTitle());
        holder.tv_notification_description.setText(data.get(position).getDescription());

        if (data.get(position).getImage() != null && !data.get(position).getImage().equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + data.get(position).getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(holder.iv_notification_image);
        } else {
            holder.iv_notification_image.setImageResource(R.drawable.ic_image_placeholder);
        }

        holder.iv_notification_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.viewImageDialog(data.get(position).getImage());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_notification_title)
        TextView tv_notification_title;
        @BindView(R.id.tv_notification_description)
        TextView tv_notification_description;
        @BindView(R.id.iv_notification_image)
        ImageView iv_notification_image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void viewImageDialog(String image) {
            final Dialog popupDialog;
            popupDialog = new Dialog(mContext);
            popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

            popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
            WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
            popupDialog.setCanceledOnTouchOutside(true);
            popupDialog.getWindow().setAttributes(params);

            PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
            ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
            TextView tv_title = popupDialog.findViewById(R.id.tv_title);

            iv_back.setVisibility(View.VISIBLE);
            photo_view.setVisibility(View.VISIBLE);

            tv_title.setText(mContext.getString(R.string.preview));
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupDialog.dismiss();
                }
            });
            Picasso.get()
                    .load(Constants.IMAGE_BASE_URL + image)
                    .error(R.drawable.ic_image_placeholder)
                    .into(photo_view);

            popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            popupDialog.getWindow().setGravity(Gravity.CENTER);
            popupDialog.show();
            popupDialog.setCancelable(true);
        }
    }
}