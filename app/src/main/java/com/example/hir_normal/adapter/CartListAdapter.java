package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.interfaces.OnCartItemClick;
import com.example.hir_normal.model.CartData;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.MyViewHolder> {
    private Context mContext;
    List<CartData> data;
    private int value = 1;
    private TextView tvTotal;
    private OnCartItemClick onCartItemClick;

    public CartListAdapter(Context mContext, List<CartData> data, TextView tvTotal) {
        this.mContext = mContext;
        this.data = data;
        this.tvTotal = tvTotal;
        this.onCartItemClick = (OnCartItemClick) mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_cart_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvProduct.setText(data.get(position).getTitle());
        if (Integer.parseInt(data.get(position).getCartQuantity()) > 0) {
            holder.tv_qty_value.setText(data.get(position).getCartQuantity());
        }
        value = Integer.parseInt(data.get(position).getCartQuantity());
        if (value == 1) {
            holder.iv_minus.setImageResource(R.drawable.ic_minus_disable);
        } else {
            holder.iv_minus.setImageResource(R.drawable.ic_minus);
        }

        holder.tv_qty_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartItemClick.onCartItemClick(position, data.get(position));
            }
        });

        if (data.get(position).getProductImages() != null &&
                data.get(position).getProductImages().size() > 0 &&
                data.get(position).getProductImages().get(0).getImage() != null &&
                !data.get(position).getProductImages().get(0).getImage().equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + data.get(position).getProductImages().get(0).getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(holder.ivProduct);
        } else {
            holder.ivProduct.setImageResource(R.drawable.ic_image_placeholder);
        }

        holder.tvAddToCart.setOnClickListener(v -> {
            if (Function.isNetworkAvailable(mContext)) {
                addOrRemoveQuantity(position, Integer.parseInt(holder.tv_qty_value.getText().toString()), 1);
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            }
        });

        holder.iv_plus.setOnClickListener(v -> {
            value = Integer.parseInt(holder.tv_qty_value.getText().toString());
            value = value + 1;
            if (Function.isNetworkAvailable(mContext)) {
                addOrRemoveQuantity(position, value, 0);
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            }
        });

        holder.iv_minus.setOnClickListener(v -> {
            if (value != 1) {
                value = Integer.parseInt(holder.tv_qty_value.getText().toString());
                value = value - 1;
                if (Function.isNetworkAvailable(mContext)) {
                    addOrRemoveQuantity(position, value, 0);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(data.get(position).getProductImages().get(0).getImage());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data != null && data.size() > 0) {
            return data.size();
        } else {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivProduct)
        ImageView ivProduct;
        @BindView(R.id.tvProduct)
        TextView tvProduct;
        @BindView(R.id.clMain)
        ConstraintLayout clMain;
        @BindView(R.id.cl_quantity)
        ConstraintLayout cl_quantity;
        @BindView(R.id.iv_plus)
        ImageView iv_plus;
        @BindView(R.id.iv_minus)
        ImageView iv_minus;
        @BindView(R.id.tv_qty_value)
        TextView tv_qty_value;
        @BindView(R.id.tvAddToCart)
        ImageView tvAddToCart;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cl_quantity.setVisibility(View.VISIBLE);
            //tvAddToCart.setText(mContext.getResources().getString(R.string.remove_cart));

        }
    }

    private void addOrRemoveQuantity(int position, int value, int isRemovable) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);
        Double price = Double.parseDouble(data.get(position).getPrice()) * value;
//        Toast.makeText(mContext, "price " + price, Toast.LENGTH_SHORT).show();
        Call<JsonObject> jsonObjectCall = null;
       /* if (value == 0) {
            jsonObjectCall = api.addToCart(data.get(0).getUserId(),
                    data.get(0).getProductId(), String.valueOf(price), String.valueOf(value), "0");
        } else {*/
        jsonObjectCall = api.addToCart(data.get(position).getUserId(),
                data.get(position).getProductId(), String.valueOf(price), String.valueOf(value), String.valueOf(isRemovable));
//        }
        Objects.requireNonNull(jsonObjectCall).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (isRemovable == 0) {
                            data.get(position).setCartQuantity(String.valueOf(value));
                            Double price = 0.0;
                            if (data != null && data.size() > 0) {
                                for (int i = 0; i < data.size(); i++) {
                                    price = price + (Double.parseDouble(data.get(i).getPrice()) * Double.parseDouble(data.get(i).getCartQuantity()));
                                }
                            }

                            tvTotal.setText(String.valueOf(price));
                            notifyItemChanged(position);
                        } else {
                            data.remove(position);
                            Double price = 0.0;
                            if (data != null && data.size() > 0) {
                                for (int i = 0; i < data.size(); i++) {
                                    price = price + (Double.parseDouble(data.get(i).getPrice()) * Double.parseDouble(data.get(i).getCartQuantity()));
                                }
                            }

                            tvTotal.setText(String.valueOf(price));
                            notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void viewImageDialog(String image) {
        final Dialog popupDialog;
        popupDialog = new Dialog(mContext);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

        popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
        WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
        popupDialog.setCanceledOnTouchOutside(true);
        popupDialog.getWindow().setAttributes(params);

        PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
        ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
        TextView tv_title = popupDialog.findViewById(R.id.tv_title);

        iv_back.setVisibility(View.VISIBLE);
        photo_view.setVisibility(View.VISIBLE);

        tv_title.setText(mContext.getString(R.string.preview));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        Picasso.get()
                .load(Constants.IMAGE_BASE_URL + image)
                .error(R.drawable.ic_image_placeholder)
                .into(photo_view);

        popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupDialog.getWindow().setGravity(Gravity.CENTER);
        popupDialog.show();
        popupDialog.setCancelable(true);
    }
}