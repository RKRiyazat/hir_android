package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.PaymentHistoryActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.model.OrderHistory.Order;
import com.example.hir_normal.model.OrderHistory.OrderProduct;
import com.github.chrisbanes.photoview.PhotoView;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderProductAdapter extends RecyclerView.Adapter<OrderProductAdapter.MyViewHolder> {
    private Context mContext;
    private List<OrderProduct> data;

    public OrderProductAdapter(Context mContext, List<OrderProduct> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_order_product, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_product_name.setText(data.get(position).getName());
        holder.expTv1.setText(data.get(position).getDescription());
        holder.tv_price_value.setText(data.get(position).getPrice());
        holder.tv_quantity_value.setText(data.get(position).getQuantity());

        holder.tv_total_value.setText(String.valueOf(Double.parseDouble(data.get(position).getPrice()) * Double.parseDouble(data.get(position).getQuantity())));

        if (data.get(position).getImages() != null &&
                data.get(position).getImages().size() > 0 &&
                data.get(position).getImages().get(0).getImage() != null &&
                !data.get(position).getImages().get(0).getImage().equalsIgnoreCase("")) {
            Glide.with(mContext)
                    .load(Constants.IMAGE_BASE_URL + data.get(position).getImages().get(0).getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .into(holder.iv_product);
        } else {
            holder.iv_product.setImageResource(R.drawable.ic_image_placeholder);
        }

        holder.iv_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(data.get(position).getImages().get(0).getImage());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product_name)
        TextView tv_product_name;
        @BindView(R.id.tv_description)
        ExpandableTextView tv_description;
        @BindView(R.id.tv_price_value)
        TextView tv_price_value;
        @BindView(R.id.expandable_text)
        TextView expandable_text;
        @BindView(R.id.tv_quantity_value)
        TextView tv_quantity_value;
        @BindView(R.id.tv_total_value)
        TextView tv_total_value;
        @BindView(R.id.iv_product)
        ImageView iv_product;
        ExpandableTextView expTv1;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            expTv1 = itemView.findViewById(R.id.tv_description);
        }
    }

    public void viewImageDialog(String image) {
        final Dialog popupDialog;
        popupDialog = new Dialog(mContext);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.image_preview);

//              popupDialog.getWindow().setLayout(((getWidth(mContext) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);

        popupDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupDialog.getWindow().setWindowAnimations(R.style.PauseDialogAnimationBottomToUp);
//              ((getWidth(mContext) / 100) * 98)
        WindowManager.LayoutParams params = popupDialog.getWindow().getAttributes();
        popupDialog.setCanceledOnTouchOutside(true);
        popupDialog.getWindow().setAttributes(params);

        PhotoView photo_view = popupDialog.findViewById(R.id.photo_view);
        ImageView iv_back = popupDialog.findViewById(R.id.iv_back);
        TextView tv_title = popupDialog.findViewById(R.id.tv_title);

        iv_back.setVisibility(View.VISIBLE);
        photo_view.setVisibility(View.VISIBLE);

        tv_title.setText(mContext.getString(R.string.preview));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        Picasso.get()
                .load(Constants.IMAGE_BASE_URL + image)
                .error(R.drawable.ic_image_placeholder)
                .into(photo_view);

        popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupDialog.getWindow().setGravity(Gravity.CENTER);
        popupDialog.show();
        popupDialog.setCancelable(true);
    }
}
