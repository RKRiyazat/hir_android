package com.example.hir_normal.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hir_normal.MyApp;
import com.example.hir_normal.R;
import com.example.hir_normal.activity.OrderProductActivity;
import com.example.hir_normal.activity.OrderedGiftActivity;
import com.example.hir_normal.function.Constants;
import com.example.hir_normal.function.Function;
import com.example.hir_normal.interfaces.API;
import com.example.hir_normal.model.GiftOrderHistoryData;
import com.example.hir_normal.model.OrderGift;
import com.example.hir_normal.model.OrderHistory.Order;
import com.example.hir_normal.model.OrderHistory.OrderProduct;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderGiftStatusAdapter extends RecyclerView.Adapter<OrderGiftStatusAdapter.MyViewHolder> {
    private Context mContext;
    private List<GiftOrderHistoryData> data;
    private List<OrderGift> orderGiftList;

    public OrderGiftStatusAdapter(Context mContext, List<GiftOrderHistoryData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_order_gift_status, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_order_id_value.setText(data.get(position).getId().toString());

        String date = Function.dateFormatChange(data.get(position).getCreatedAt());
        if (date != null && !date.equalsIgnoreCase("")) {
            holder.tv_order_date_value.setText(date);
        }


        /*1 = inprogress, 2 = waiting, 3 = on the way , 4 = delivered*/

        if (data.get(position).getStatus().equalsIgnoreCase("1")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_inprogress);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("2")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_waiting);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("3")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_on_the_way);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("4")) {
            holder.btn_cancel_order.setVisibility(View.GONE);
            holder.tv_order_status_value.setText(R.string.str_deliverd);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        } else if (data.get(position).getStatus().equalsIgnoreCase("5")) {
            holder.tv_order_status_value.setText(R.string.str_canceled);
            holder.tv_order_status_value.setTextColor(mContext.getResources().getColor(R.color.colorOrange));
            holder.btn_cancel_order.setVisibility(View.GONE);
        }

        holder.btn_cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Function.isNetworkAvailable(mContext)) {
                    apiCallCancelOrder(position, data.get(position).getId().toString());
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderGiftList = new ArrayList<>();
                orderGiftList = data.get(position).getItems();

                if (orderGiftList != null && orderGiftList.size() > 0) {
                    mContext.startActivity(new Intent(mContext, OrderedGiftActivity.class)
                            .putExtra(Constants.ORDER_GIFT_LIST, (Serializable) orderGiftList));
                } else {
                    Toast.makeText(mContext, R.string.no_gift_history, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_id_value)
        TextView tv_order_id_value;
        @BindView(R.id.tv_order_date_value)
        TextView tv_order_date_value;
        @BindView(R.id.tv_order_status_value)
        TextView tv_order_status_value;
        @BindView(R.id.btn_cancel_order)
        Button btn_cancel_order;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void apiCallCancelOrder(int position, String orderId) {
        Dialog dialog = ProgressDialog.show(mContext, "", "please wait...");
        dialog.show();
        API api = MyApp.retrofit.create(API.class);

        Call<JsonObject> jsonObjectCall = null;
        jsonObjectCall = api.cancelOrder(orderId);
//        }
        Objects.requireNonNull(jsonObjectCall).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                dialog.dismiss();
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(Objects.requireNonNull(response.body()).toString());
                    String code = jsonObject.getString("code");
                    if (code.equals("200")) {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        data.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, data.size());
                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
